#Standard makefile for LMI labs
include $(LMIBASE)/bbb/make/bbb_tools.mk

ifneq ($(LIBDIRS),)
LIBALL=lib_all
LIBALL_CLEAN=lib_all_clean
endif

.PHONY: all clean lib_all lib_all_clean

all: $(OBJDIR) $(EXTRA_ALL) $(LIBALL) $(EXE)

clean: $(EXTRA_CLEAN) $(LIBALL_CLEAN)
	-$(DEL) $(EXE) $(EXEC)_[ahp] *.map *~ $(EXTRAS)
	-$(RMDIR) .obj	
	
lib_all_clean:
	$(foreach dir,$(LIBDIRS), "$(MAKE)" --no-print-directory -C $(dir) clean $(SEP))
	
lib_all:
	$(foreach dir,$(LIBDIRS), "$(MAKE)" --no-print-directory -C $(dir) all $(SEP))

$(EXE): $(OBJS) $(LINKER_SCRIPT) $(EXTRA_LIBS)
	$(LD) $(OBJS) $(LDFLAGS) -o $@ 
			
$(OBJDIR):
	echo $(OBJDIR)
	$(MKDIR) $(OBJDIR)

-include $(OBJS:.o=.d)


