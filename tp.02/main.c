/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Counter and snake on Beagle Bone
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <am335x_gpio.h>
#include "seg7.h"
#include "wheel.h"
#include "buttons.h"
#include "leds.h"
// -- constants & variable declaration ---------------------------------------

// delays
#define DELAY_ON	0x000000


// pin definition for 7-segment access
#define GPIO2		AM335X_GPIO2
#define DIG1		(1<<2)
#define GPIO0		AM335X_GPIO0
#define SEGA		(1<<4)
#define SEGB		(1<<5)
#define SEGC		(1<<14)
#define SEGD		(1<<22)
#define SEGE		(1<<23)
#define SEGF		(1<<26)
#define SEGG		(1<<27)
// macro to compute number of elements of an array
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static int displayUnit = 0;
static int displayDecade = 0;
static int serpentPosition = 0;
enum modePlayed{
	compteur, serpent, reset
};
static enum modePlayed mode = reset;
/* structure to initialize gpio pins used by 7-segment
   segment definition

           +-- SEG_A --+
           |           |
         SEG_F       SEG_B
           |           |
           +-- SEG_G --+
           |           |
         SEG_E       SEG_C
           |           |
           +-- SEG_D --+
*/


static void mode_compteur(enum wheel_direction direction){
	if(direction == WHEEL_LEFT){		//on diminue la valeur
		//-----------pos
		if(displayUnit== 0 && displayDecade > 0){
			displayDecade --;
			displayUnit = 9;
		}else if (displayUnit>0 && displayDecade>=0){
			displayUnit--;
		}
		//----------------negate
		else if(displayDecade<= 0 && displayUnit == -9 && displayDecade > -9){
			displayUnit =0;
			displayDecade--;
		}
		else if(displayDecade<=0 && displayUnit<=0 && displayDecade >=-9 && displayUnit>-9){
			displayUnit--;
		}
	}else if(direction == WHEEL_RIGHT){
	//-----------------pos
		if (displayUnit == 9 && displayDecade<9){
			displayUnit=0;
			displayDecade++;
		}else if( displayUnit <9 && displayUnit>0){
			displayUnit++;
		}
		//-----------------negate
		else if ( displayUnit==0 && displayDecade<0){
			displayDecade ++;
			displayUnit = -9;
		}else if ( displayDecade <= 0 || displayUnit<=0){
			displayUnit++;
		}
	}
	return;
}

static void mode_serpentin(enum wheel_direction direction){
	if ( direction == WHEEL_LEFT){
		if(serpentPosition>0){
			serpentPosition--;
		}else{
			serpentPosition=15;
		}
	}else if(direction == WHEEL_RIGHT){
		if(serpentPosition<15){
			serpentPosition++;
		}else{
			serpentPosition=0;
		}
	}
}

static void mode_reset(){
	displayUnit = 0;
	displayDecade = 0;
	serpentPosition = 0;
}

static enum modePlayed getMode(){
	if(mode == reset && !buttons_get_states(BUTTONS_3)){
		mode = compteur;
	}
	else if(buttons_get_states(BUTTONS_3)){
			mode = reset;
		}
	else if (buttons_get_states(BUTTONS_1)){
		mode = compteur;
	}else if(buttons_get_states(BUTTONS_2)){
		mode = serpent;
	}
	return mode;
}

static void refresh_seg7(int valDecade, int valUnit, int serpentPosition){
	static enum wheel_direction direction = WHEEL_STILL;
	enum modePlayed mode = getMode();
	direction = wheel_get_direction();
	if (mode == compteur){
		mode_compteur(direction);
		if (valUnit >=0 && valDecade >= 0){
				seg7_display_segments(abs(valDecade),abs(valUnit));
		}else{
				seg7_display_dots(abs(valDecade),abs(valUnit));
		}
		leds_set_states(LED_S1, true);
		leds_set_states(LED_S2, false);
		leds_set_states(LED_S3, false);
	}else if(mode == serpent){
		mode_serpentin(direction);
		seg7_serpentin(serpentPosition);
		leds_set_states(LED_S2, true);
		leds_set_states(LED_S1, false);
		leds_set_states(LED_S3, false);

	}else if (mode == reset){
		mode_reset();
		leds_set_states(LED_S3, true);
				leds_set_states(LED_S2, false);
				leds_set_states(LED_S1, false);
				seg7_reset();
	}


	else if(mode == reset){		//mode selected is reset

	}



}

//-- implementation of public methods ---------------------------------------

int main(){
	// main loop
	seg7_init();
	wheel_init();
	buttons_init();
	leds_init();
	while(true) {
		refresh_seg7(displayDecade,displayUnit, serpentPosition);
	}
	return 0;
}
