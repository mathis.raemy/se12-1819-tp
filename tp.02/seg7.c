/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	This file manage the 7 segments display
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <am335x_gpio.h>
#include "seg7.h"
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#define GPIO0		AM335X_GPIO0
#define GPIO1		AM335X_GPIO1
#define GPIO2		AM335X_GPIO2
#define DIG1		(1<<2)
#define SEGA		(1<<4)
#define SEGB		(1<<5)
#define SEGC		(1<<14)
#define SEGD		(1<<22)
#define SEGE		(1<<23)
#define SEGF		(1<<26)
#define SEGG		(1<<27)

/* structure to initialize gpio pins used by 7-segment
   segment definition

           +-- SEG_A --+
           |           |
         SEG_F       SEG_B
           |           |
           +-- SEG_G --+
           |           |
         SEG_E       SEG_C
           |           |
           +-- SEG_D --+
*/
static const struct gpio_init {
	uint32_t pin_nr;
} gpio_init[] = {
	{ 4},	// SEGA
	{ 5},	// SEGB
	{14},	// SEGC
	{22},	// SEGD
	{23},	// SEGE
	{26},	// SEGF
	{27},	// SEGG
};
static const uint32_t valSerpent[]	= {
		SEGF, SEGA, SEGB, SEGC, SEGD, SEGE, SEGG,			//one digit complete
		SEGG, SEGC, SEGD, SEGE, SEGF, SEGA, SEGB, SEGG, 	//second digit
		SEGG,												//back to the first
															//from 0 to 15
	0
};
static const uint32_t valCompteur[]	= {
		SEGA | SEGB | SEGC | SEGD | SEGE | SEGF 		, //0
			   SEGB | SEGC 								, //1
		SEGA | SEGB |		 SEGD | SEGE | 		  SEGG	, //2
		SEGA | SEGB | SEGC | SEGD | 		 	  SEGG	, //3
			   SEGB | SEGC | 		 	   SEGF | SEGG	, //4
		SEGA | 		  SEGC | SEGD |		   SEGF | SEGG	, //5
		SEGA | 		  SEGC | SEGD | SEGE | SEGF | SEGG	, //6
		SEGA | SEGB | SEGC 								, //7
		SEGA | SEGB | SEGC | SEGD | SEGE | SEGF | SEGG	, //8
		SEGA | SEGB | SEGC | SEGD | 	   SEGF | SEGG	, //9
	0
};



void seg7_init(){
	am335x_gpio_init(GPIO0);					//init of GPIO usefull
	am335x_gpio_init(GPIO1);					//init of GPIO usefull
	am335x_gpio_init(GPIO2);					//init of GPIO usefull
	am335x_gpio_setup_pin_out(GPIO2, 4, false); // init of the dots output
	am335x_gpio_setup_pin_out(GPIO2, 2, false);	//set gpio2 of 2 as an output (to pilot the first display)
	am335x_gpio_setup_pin_out(GPIO2, 3, false);	//set gpio2 of 2 as an output (to pilot the first display)
	for (int i = ARRAY_SIZE(gpio_init)-1; i>=0; i--) {
		am335x_gpio_setup_pin_out(GPIO0, gpio_init[i].pin_nr, false);
		printf("i pointer ----""%p",(void*)&i);
	}
}

void seg7_display_segments (uint32_t Decade_Value, uint32_t Unit_value){
	//digit indicate on which digit we want to display
	//value indicate the value that we want to display
	static bool b = false;
	am335x_gpio_change_states(GPIO0, valCompteur[8], false);
	am335x_gpio_change_state(GPIO2,4,false);
	am335x_gpio_change_state(GPIO2,2,false);
	am335x_gpio_change_state(GPIO2,3,false);

	if (b) {
		am335x_gpio_change_state(GPIO2,3,true);
		am335x_gpio_change_states(GPIO0, valCompteur[Unit_value], true);

	}else {
		am335x_gpio_change_state(GPIO2,2,true);
		am335x_gpio_change_states(GPIO0, valCompteur[Decade_Value], true);

	}

	b = b ? false : true;
}

void seg7_display_dots (uint32_t Decade_Value, uint32_t Unit_Value){
	//digit indicate on which digit we want to display
	//value indicate the value that we want to display
	static bool b = false;
	am335x_gpio_change_states(GPIO0, valCompteur[8], false);
	am335x_gpio_change_state(GPIO2,4,false);
	am335x_gpio_change_state(GPIO2,2,false);
	am335x_gpio_change_state(GPIO2,3,false);

	if (b) {
		am335x_gpio_change_state(GPIO2,3,true);
		am335x_gpio_change_states(GPIO0, valCompteur[Unit_Value], true);
	}else{
		am335x_gpio_change_state(GPIO2,4,true);
		am335x_gpio_change_state(GPIO2,2,true);
		am335x_gpio_change_states(GPIO0, valCompteur[Decade_Value], true);
	}
	b = b ? false : true;

}
void seg7_serpentin (uint32_t value){
	//digit indicate on which digit we want to display
	//value indicate the value that we want to display
	am335x_gpio_change_state(GPIO2,4,false);
		if (value <=6 || value == 15) {
			am335x_gpio_change_state(GPIO2,3,false);
			am335x_gpio_change_state(GPIO2,2,true);

		}
		else if (value >5 && value <15) {
			am335x_gpio_change_state(GPIO2,2,false);
			am335x_gpio_change_state(GPIO2,3,true);
		}
		am335x_gpio_change_states(GPIO0, valCompteur[8], false);
		am335x_gpio_change_states(GPIO0, valSerpent[value], true);
}

void seg7_reset(){
	am335x_gpio_change_state(GPIO2,4, false);
	am335x_gpio_change_states(GPIO0, valCompteur[8], false);
}





