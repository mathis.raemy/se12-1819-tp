/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Manage the Wheel input
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <am335x_gpio.h>
#include "wheel.h"
#define CHA_GPIO	AM335X_GPIO2
#define CHA_PIN 	1
#define CHB_GPIO	AM335X_GPIO1
#define CHB_PIN		29



enum wheel_direction transitions[4][4] = {
		{WHEEL_STILL, WHEEL_STILL, WHEEL_STILL, WHEEL_STILL	},
		{WHEEL_LEFT	, WHEEL_STILL, WHEEL_STILL, WHEEL_RIGHT	},
		{WHEEL_RIGHT, WHEEL_STILL, WHEEL_STILL, WHEEL_LEFT	},
		{WHEEL_STILL, WHEEL_STILL, WHEEL_STILL, WHEEL_STILL	},
	};

static int former_state = 0;

static int get_encoder_state(){
	int state  = 0;
	if(am335x_gpio_get_state(CHA_GPIO,CHA_PIN)){
		state += 1;
	}
	if(am335x_gpio_get_state(CHB_GPIO,CHB_PIN)){
		state += 2;
	}
	return state;
}

void wheel_init(){
	am335x_gpio_init(CHA_GPIO);
	am335x_gpio_init(CHB_GPIO);
	am335x_gpio_setup_pin_in(CHA_GPIO,CHA_PIN,AM335X_GPIO_PULL_NONE,true);
	am335x_gpio_setup_pin_in(CHB_GPIO,CHB_PIN,AM335X_GPIO_PULL_NONE,true);

	former_state = get_encoder_state();
}

enum wheel_direction wheel_get_direction(){
	int state = get_encoder_state();
	enum wheel_direction dir = transitions[former_state][state];
	former_state = state;
	return dir;

}

















