/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of the Wheel.c file
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */

#pragma once
#ifndef WHEEL_H_
#define WHEEL_H_
enum wheel_direction {
	WHEEL_STILL, WHEEL_RIGHT, WHEEL_LEFT
};

//init of the wheel input
extern void wheel_init();

//Used to get the direction where the wheel turned
extern enum wheel_direction wheel_get_direction();

#endif
