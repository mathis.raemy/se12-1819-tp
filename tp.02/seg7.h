#pragma once
#ifndef SEG7_H_
#define SEG7_H_

/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of seg7.c
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */


enum seg7_display{
	unit, decade
};
//initialisation of the GPIO pind of the segments
extern void seg7_init();

//Used to display the counter with as input the digit, and the value

void seg7_display_segments (uint32_t Decabe_Value, uint32_t Unit_value);

//same as seg7_display_segments but for a nagate value
void seg7_display_dots (uint32_t Decade_Value, uint32_t Unit_Value);

//Used to display the snake with the actual position in Integer
extern void seg7_serpentin (uint32_t value);

//Used to reset the segments
extern void seg7_reset();

#endif
