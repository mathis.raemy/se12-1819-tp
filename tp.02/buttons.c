/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	This file is used to read the buttons input
 *
 * Purpose:	This module implements basic service of the buttons S1,S2,S3
 *
 * Author: 	Tobias Moullet
 * Date: 	03.08.2016
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "buttons.h"
#include <am335x_gpio.h>



#define BTN_GPIO		AM335X_GPIO1
#define Btn1		(1<<15)
#define Btn2		(1<<16)
#define Btn3		(1<<17)

// macro to compute number of elements of an array
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static const struct gpio_init {
	uint32_t pin_nr;
} gpio_init[] = {
	{15},	// Btn1
	{16},	// Btn2
	{17},	// Btn3
};


static int buttons_return_pn(enum buttons_names name)
{
	if (name == BUTTONS_1)return 15;
	else if( name == BUTTONS_2)return 16;
	else if ( name == BUTTONS_3)return 17;
	else return 0;
}


// For each header, we need a method to initialize the functio
void buttons_init(){
	// initialize GPIO
	am335x_gpio_init (BTN_GPIO);

	for (int i=ARRAY_SIZE(gpio_init)-1; i>=0; i--) {
		am335x_gpio_setup_pin_in(BTN_GPIO, gpio_init[i].pin_nr, AM335X_GPIO_PULL_NONE, false);
		printf("i pointer ----""%p",(void*)&i);
	}
}



// this method return the states of the button passed in the parameter
bool buttons_get_states(enum buttons_names button){
	return !(am335x_gpio_get_state(BTN_GPIO,buttons_return_pn(button)));
}
