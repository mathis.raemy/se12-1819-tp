#pragma once
#ifndef BUTTONS_H
#define BUTTONS_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of the button.c
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author: 	Tobias Moullet
 * Date: 	01.10.18
 */
#include <stdint.h>
#include <stdbool.h>


// This enum is used to recognize witch button has been pressed or used 
enum buttons_names {BUTTONS_1, BUTTONS_2, BUTTONS_3};

// For each header, we need a method to initialize the function
extern void buttons_init();

// this method return the states of the button passed in the parameter
extern bool buttons_get_states(enum buttons_names button);

#endif





