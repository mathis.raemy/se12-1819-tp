/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	This file manage the output led
 *
 * Purpose:	This module implements basic service of the buttons S1,S2,S3
 *
 * Author: 	Tobias Moullet
 * Date: 	03.08.2016
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "leds.h"
#include <am335x_gpio.h>


#define LEDS_GPIO	AM335X_GPIO1
#define LED1		(1<<12)
#define LED2		(1<<13)
#define LED3		(1<<14)


// macro to compute number of elements of an array
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))


static const struct gpio_init {
	uint32_t pin_nr;
} gpio_init[] = {
	{12},	// Btn1
	{13},	// Btn2
	{14},	// Btn3
};

static int leds_return_pn(enum leds_name name)
{
	if (name == LED_S1)return 12;
	else if( name == LED_S2)return 13;
	else return 14;
}

extern void leds_init(){
	// initialize GPIO
	am335x_gpio_init (LEDS_GPIO);

	for (int i=ARRAY_SIZE(gpio_init)-1; i>=0; i--){
		am335x_gpio_setup_pin_out(LEDS_GPIO, gpio_init[i].pin_nr, false);
		printf("i pointer ----""%p",(void*)&i);
	}
}


extern void leds_set_states(enum leds_name name, bool state){
	am335x_gpio_change_state(LEDS_GPIO,leds_return_pn(name),state);
}
