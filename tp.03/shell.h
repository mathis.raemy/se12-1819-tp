#pragma once
#ifndef SHELL_H_
#define SHELL_H_
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:	This module read the string in input and call the linked function
 *
 * Author: 	Raemy Mathis
 * Date: 	26.11.2018
 */
#include <stdint.h>
#include <stdbool.h>

//init the shell and construct the chained list of pointer on function
extern void init_shell();

//read the string and call the linked function
extern int process (const char* command_line);

#endif /* SHELL_H_ */
