/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:	This module read the string in input and call the linked function
 *
 * Author: 	Raemy Mathis
 * Date: 	26.11.2018
 */

#include "cli.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "display.h"
#include "shell.h"
#define ARRAY_SIZE(x)(sizeof(x)/sizeof(x[0]))

struct command {
	const char* name;
	const char* brief;
	const char* help;
	int (*cmd) (int argc, char* argv[]) ;
	struct command* next;
};

static struct command* cmd_list = 0;

// ---------------------------------------------------------------------------------------




static void tokenize (char* str, int* argc, char** argv, int sz)
{
	*argc = 0;
	while (1) {
		while ((*str <= ' ') && (*str != '\0')) str++;
		if (*str == '\0') break;

		argv[(*argc)++] = str++;
		if (*argc >= sz) break;

		while (*str > ' ') str++;
		if (*str == '\0') break;

		*str++ = '\0';
	}
}

// ---------------------------------------------------------------------------------------

int attach (struct command* cmd)
{
	int status = 0;
	struct command* e = cmd_list;
	struct command* p = 0;
	while ((e != 0) && (strcmp(e->name, cmd->name) < 0)) {
		p = e;
		e = e->next;
	}
	if (p == 0) {
		cmd->next = cmd_list;
		cmd_list = cmd;
	} else {
		cmd->next = e;
		p->next = cmd;
	}
	return status;
}

// ---------------------------------------------------------------------------------------

int process (const char* command_line)
{

	int status = -1;
	char str[strlen(command_line)+1];
	char* argv[100] = {""};
	int   argc = 0;

	strcpy (str, command_line);
	tokenize (str, &argc, &argv[0], ARRAY_SIZE(argv));

	const struct command *cmd = cmd_list;
	while ((cmd != 0) && (strcmp (argv[0], cmd->name) != 0)) {
		cmd = cmd->next;
	}
	if (cmd != 0) {
		status = cmd->cmd (argc, argv);
		if (status != 0) {
			printf ("error on processing: %d\n", status);
		}
	} else {
		printf ("error: command not found: \"%s\"\nType \"help\" to get the command list \n", argv[0]);
	}
	return status;
}


// ---------------------------------------------------------------------------------------

static int help (int argc, char* argv[])
{
	struct command* e = cmd_list;
	while (e != 0) {
		printf("-------------------------------------------------------------------------------\n");
		printf ("%s %s %s\n", e->name, e->brief, e->help);
		e = e->next;
	}
	printf("-------------------------------------------------------------------------------\n");
	printf("\n");
	return 0;
}
static struct command hlp = {
	.name = "help",
	.brief ="",
	.help="",
	.cmd = help,
};

// ---------------------------------------------------------------------------------------

extern int color (int argc, char* argv[])
{
	int parameter = 0;
	int status = -1;
	for (int i = 1; i < argc; i=i+2) {
		if(argv[i][0] == '-' && argv[i][1] != '\0' && argv[i][2] == '\0'){
				if(argv[i][1] == 'd'){		//-b background 	-m min color -M max color -c current color
					status = 0;
					change_color(BACKGRUND, 0xffff);	//default colors
					change_color(LOW_COLOR, 0x001f);	//default colors
					change_color(ACT_COLOR, 0xffff);	//default colors
					change_color(HIGH_COLOR, 0xf800);	//default colors
				}else if(argv[i][1] == 'm' && argc >2){
					parameter= strtol(argv[i+1], NULL, 16);
					if(parameter >= 0x0000 && parameter <= 0xffff){
						status = 0;
						change_color(LOW_COLOR, parameter);
					}
				}else if(argv[i][1] == 'M' && argc >2){
					parameter= strtol(argv[i+1], NULL, 16);
					if(parameter >= 0x0000 && parameter<= 0xffff){
						status = 0;
						change_color(HIGH_COLOR, parameter);
					}
				}else if(argv[i][1] == 'b' && argc >2){
					parameter= strtol(argv[i+1], NULL, 16);
					if(parameter >= 0x0000 && parameter <= 0xffff){
						status = 0;
						change_color(BACKGRUND, parameter);
					}
				}else if(argv[i][1] == 'c' && argc >2){
					parameter= strtol(argv[i+1], NULL, 16);
					if(parameter >= 0x0000 && parameter <= 0xffff){
						status = 0;
						change_color(ACT_COLOR, parameter);
					}
				}
		}
	}
	return status;
}
static struct command clr = {
	.name = "color",
	.brief ="",
	.help="   -d \t  \t to set the default color \n \t  -m [hexaValue] to set the min value color\n \t  -M [hexaValue] to set the max value color\n \t  -b [hexaValue] to set the background color\n \t  -c [hexaValue] to set the current value color",
	.cmd = color,
};

// ---------------------------------------------------------------------------------------
int temperature (int argc, char* argv[])
{
	int status = -1;
	for (int i = 1; i < argc; i=i+1) {
		if(argv[i][0] == '-' && argv[i][1] != '\0' && argv[i][2] == '\0'){
			if(argv[i][1] == 'm'){
				status = 0;
				reinit_termo_Min();
			}else if(argv[i][1] == 'M'){
				status = 0;
				reinit_termo_Max();

			}
		}
	}

	return status;
}
static struct command tmp = {
	.name = "temperature",
	.brief ="",
	.help=" -m   \t Reset the min temperatur mesured\n \t      -M \t Reset the max temperatur mesured\n",
	.cmd = temperature,
};

// ---------------------------------------------------------------------------------------

//static int command4 (int argc, char* argv[])
//{
//	printf("command4");
//	for (int i = 1; i < argc; i++) {
//		printf(" %s", argv[i]);
//	}
//	printf("\n");
//	return 0;
//}
//static struct command cmd4 = {
//	.name = "command4",
//	.brief ="b4",
//	.help="c4",
//	.cmd = command4,
//};

// ---------------------------------------------------------------------------------------

// ---------------------------------------------------------------------------------------
void init_shell(){
		attach (&tmp);
		attach (&hlp);
		attach (&clr);
//		attach (&cmd4);
}


//int main()
//{
//	process (str1);
//	process (str2);
//	process (str3);
//	process (str4);
//	process (str5);
//	process (str6);
//
//	return 0;
//}
