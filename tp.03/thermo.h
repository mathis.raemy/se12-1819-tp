
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	AM335x Pin Multiplexer Driver
 *
 * Purpose:	This module implements basic service of the buttons S1,S2,S3
 *
 * Author: 	Raemy Mathis
 * Date: 	03.08.2016
 */
#include <stdint.h>
#include <stdbool.h>

#ifndef THERMO_H_
#define THERMO_H_

extern void init_thermo();

extern int read_thermo();

#endif /* THERMO_H_ */
