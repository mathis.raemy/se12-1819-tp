/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:	This module implement the Command Line Interface of the Beagle Bones. It read the user's input, and send
 * 			the prompted string to the shell module.
 *
 * Author: 	Mathis Raemy
 * Date: 	26.11.2018
 */
#include <stdint.h>
#include <stdbool.h>

#ifndef CLI_H_
#define CLI_H_

//initialosation of the CLI
extern void init_cli(void);

//this methode write on the terminal a text
extern void write_text(char* c);

//this methode check if the user has pressed a key and construct a string to give to the shell
void prompt_text();

#endif /* CLI_H_ */
