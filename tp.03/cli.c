/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This module is based on the software library developped by Texas Instruments
 * Incorporated - http://www.ti.com/ for its AM335x starter kit.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:	This module implement the Command Line Interface of the Beagle Bones. It read the user's input, and send
 * 			the prompted string to the shell module.
 *
 * Author: 	Mathis Raemy
 * Date: 	26.11.2018
 */

#include "shell.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <am335x_console.h>
#include <string.h>
#include "cli.h"


#define baudRate	115200
#define nmbOfSavedCMD	20
#define maxCommandSize	300
#define header	"BeagleBones_GroupeX~: "

static char prompted[maxCommandSize];
static char MemCommand[nmbOfSavedCMD][sizeof(prompted)];
const char str1[100] = "Welcome to the BEST CLI you have ever seen !!\nPleas enter a command bellow:\n";
char str2[100] = header;


void init_cli(){
	init_shell();
	am335x_console_init() ;
	am335x_console_set_baudrate(baudRate);
	am335x_console_puts(str1);
	am335x_console_puts(str2);

}

void write_text(char* str){
	am335x_console_puts(str);
}

void printEnteredChar(char* prompted, char enteredChar, int *t, int* i){

	if(*i == *t){
		prompted[*t] = enteredChar;
		am335x_console_putc(prompted[*t]);
		(*i)++;
		(*t)++;

	}else if(*t < *i){
		for(int index = *i-1; index >= *t; index--){
			prompted[index+1] = prompted[index]; //move all the prompted table of 1 case of the right


		}
		prompted[*t] = enteredChar;
		(*i)++;

		for(int index = *t; index <*i ; index++){	//
			am335x_console_putc(prompted[index]);
		}
		for(int index = *i-1; index >*t ; index--){
			am335x_console_putc((char)8);
		}
		(*t)++;

	}


}

void deleteChar(char* prompted, int *t, int *i){
	if(*i == *t){
		am335x_console_putc((char) 8);
		am335x_console_putc(' ');
		am335x_console_putc((char) 8);
		prompted[--(*t)]='\0';
		(*i)--;
	}else if (*t < *i){
		for(int index = *t-1; index < *i; index++){
			prompted[index] = prompted[index+1];//move all the prompted table of 1 case of the left
		}
		prompted[*i] = '\0';
		am335x_console_putc((char) 8);
		for(int index = *t-1; index <*i; index++){
			am335x_console_putc(prompted[index]);
		}
		am335x_console_putc(' ');
		for(int index = (*i)+1; index >*t; index--){
			am335x_console_putc((char) 8);
		}
		(*t)--;
		(*i)--;
	}

}

int printCommand(char *prompted, int i){		//return the size of the prompted command
													//from t to i

	for(int index = 0; index < i; index ++){
		am335x_console_putc((char) 8);
		am335x_console_putc(' ');
		am335x_console_putc((char) 8);

	}
	int index =0;
	while (prompted[index] != '\0'){
		am335x_console_putc(prompted[index]);
		index++;
	}
	return index;
}



void prompt_text(){
	static int i = 0;			//number of char entered
	static int t = 0;			//t is the index to know where is the insert pointer (white square in terminal)
	static int k = 0;			//index for the MemCommand navigation
	static int l = 0;			//size of the MemCommand Table
	static int m = 0;			//Number of time that arrow Up is pressed (to display the last command)
	if(am335x_console_tstc()){
	char enteredChar = am335x_console_getc();
		if(enteredChar == 13){		// if the char is a "return"

			write_text("\n");
			if(prompted[0] != '\0'){
				process(prompted);
				if( l > 0 && strcmp(prompted, MemCommand[l%nmbOfSavedCMD-1]) != 0){	// compare to see if the prompted															// cmd is diffrent then the last cmd
					strcpy(MemCommand[l%nmbOfSavedCMD], prompted);
					l++;
				}
				else if (l == 0){									//if it is the first cmd we add in evry case
					strcpy(MemCommand[l%nmbOfSavedCMD], prompted);
					l++;
				}
				k = l;
			}
			write_text(str2);
			for(uint16_t j = 0; j<sizeof(prompted)-1;j++){
				prompted[j] = '\0';
			}
			i = 0;
			t = 0;

		}else if(enteredChar == '\e'){
			am335x_console_getc();
			char pressedArrow = 0;
			pressedArrow = am335x_console_getc();
			if (pressedArrow == 'D'){		// D mean that we pressed arrow Left
				if (t > 0){
					am335x_console_putc((char) 8);
					t--;
				}

			}else if (pressedArrow == 'A'){		// A mean that we pressed arrow Up
				if(k >0 && m<nmbOfSavedCMD-1){
					strcpy(MemCommand[k%nmbOfSavedCMD], prompted);
					strcpy(prompted,MemCommand[--k%nmbOfSavedCMD]);
					i = printCommand(prompted, i);
					t = i;
					m++;
				}

			}else if (pressedArrow == 'C'){		// C mean that we pressed arrow Right
				if (t < i){
					am335x_console_putc(prompted[t]);
					t++;
				}
			}else if (pressedArrow == 'B'){		// B mean that we pressed arrow Down
				if( k< l && m >0){
					strncpy(prompted,MemCommand[++k%nmbOfSavedCMD], maxCommandSize);
					i = printCommand(prompted, i);
					t = i;
					m--;
				}
			}
		}
		else if(enteredChar == 8){			// si on veut supprimer la dernière lettre
			if (t>0){
				deleteChar(prompted, &t, &i);
			}
		}else {
			printEnteredChar(prompted, enteredChar, &t, &i);

		}
	}

}
