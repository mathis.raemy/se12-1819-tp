/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of display.c
 *
 * Purpose:	This module implements basic services to draw forms, caract and thermo on the screen.
 *
 * Author:	Tobias Moullet
 * Date: 	5.11.18
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "oled.h"
#include "font_8x8.h"
#include "display.h"
#define SCREEN_WIDTH	96
#define SCREEN_HEIGHT	96
#define BLACK			0
#define BLUE			0x001f
#define RED				0xf800
#define WHITE			0xffff


//************Init  Statics variables ****************

static int low_value =-2000;
static int high_value = 2000;
static int act_value;
static char max_value_to_display[5];
static char actuel_value_to_display[5];
static char low_value_to_display[5];

static bool isInit = false;
static bool isHorizontal= false;
static bool hasChangetemp = false;
static bool hasChangedColor= true;

static struct display_point ptn_btm;
static struct display_point ptn_tp;
static struct display_point bgn_ptn;
static struct display_point bgn_ptn_actu;
static struct display_point bgn_ptn_high;
static struct display_point stp_ptn_low;
static struct display_point stp_ptn_actu;
static struct display_point stp_ptn_high;

static int ray_in;

static int echelle;

static struct display_point plus40degree;
static struct display_point plus20degree;
static struct display_point zerodegree;
static struct display_point minus20degree;
static struct display_point high_value_array_pos;
static struct display_point act_value_array_pos;
static struct display_point low_value_array_pos;
static struct display_point start_point;
static struct display_point end_point;

static uint16_t color_thermo[4] ={WHITE,BLUE,WHITE,RED}; // case 0 = base, case  = min, case 3 = act, case 3 = max

//************* Privates functions********************

//************* External funcitons********************

void display_init(struct display_point start, struct display_point end){
	start_point = start;
	end_point = end;
	oled_init();
	display_clear();

}

void display_clear()
{
	oled_init();
	oled_memory_size(0, SCREEN_WIDTH - 1, 0, SCREEN_WIDTH - 1);
	for(unsigned int y = 0; y< SCREEN_HEIGHT; y++){
		for(unsigned int x=0; x< SCREEN_WIDTH; x++){
				oled_color(BLACK);
		}
	}
}

void change_color(enum PLACE_COLOR place, uint16_t newColor){
	if (place == BACKGRUND){
		color_thermo[0] = newColor;
		hasChangedColor = true;
	}
	else if(place == LOW_COLOR){
		color_thermo[1] = newColor;
	}
	else if(place == ACT_COLOR){
		color_thermo[2] = newColor;
	}
	else{
		color_thermo[3] = newColor;
	}
}

void reinit_termo_Max(){
	high_value = act_value;
}
void reinit_termo_Min(){
	low_value = act_value;
}

void display_rect(struct display_point bottom_left, struct display_point top_right, uint32_t color)
{
	oled_memory_size(bottom_left.x_coor, top_right.x_coor, bottom_left.y_coor, top_right.y_coor);
	for(unsigned int y = bottom_left.y_coor; y <= top_right.y_coor; y++){
		for(unsigned int x=bottom_left.x_coor; x <= top_right.x_coor; x++){
			oled_color(color);
		}
	}
}

void display_circle(struct display_point center,int32_t ray, uint32_t color)
{
	int size =0;
	for (int i =-(ray); i <= ray; i++)
	{
		size = sqrt((ray*ray)-(i*i));
		//oled_init();
		oled_memory_size(center.x_coor-size, center.x_coor+size, center.y_coor+i, center.y_coor+i);
		for (int r=0;r<=(2*size);r++){
			oled_color(color);
		}
	}
}

void display_line(struct display_point start, int size, bool isHorizontal, uint32_t color)
{
	if (isHorizontal) // if we want to display in horizontal or not
	{
		oled_memory_size(start.x_coor, start.x_coor, start.y_coor, start.y_coor+size);
	}
	else
	{
		oled_memory_size(start.x_coor-size, start.x_coor, start.y_coor, start.y_coor);
	}

	for(int i = 0;i<=size;i++)
	{
		oled_color(color);
	}
}

void display_thermo_basic_form(struct display_point center_bottom, struct display_point center_top,int32_t ray, uint32_t color)
{
	int space = 0;
	struct display_point begin_ptn;
	struct display_point stop_ptn;
	struct display_point begin_ptn_in;
	struct display_point stop_ptn_in;
	struct display_point minus201;
	struct display_point minus202;
	struct display_point minus203;
	struct display_point zero1;
	struct display_point plus201;
	struct display_point plus202;
	struct display_point plus401;
	struct display_point plus402;

//************** init all the parameters needed to implemente a thermo form******

	if(!isHorizontal && !isInit){
		high_value_array_pos = center_top;
		isInit = true;
		high_value_array_pos.y_coor -= ray+7;
		act_value_array_pos = high_value_array_pos;
		act_value_array_pos.x_coor -= 10;
		low_value_array_pos = act_value_array_pos;
		low_value_array_pos.x_coor -= 10;
	}
	else if(!isInit){
		high_value_array_pos = center_top;
		isInit = true;
		high_value_array_pos.x_coor += ray+7;
		act_value_array_pos = high_value_array_pos;
		act_value_array_pos.y_coor -= 10;
		low_value_array_pos = act_value_array_pos;
		low_value_array_pos.y_coor -= 10;
	}

	if (center_bottom.x_coor==center_top.x_coor)
	{
		isHorizontal = false;
		space = (center_top.y_coor-center_bottom.y_coor)/5;
		begin_ptn.x_coor = center_bottom.x_coor-ray;
		begin_ptn.y_coor = center_bottom.y_coor;
		begin_ptn_in.x_coor = center_bottom.x_coor-(ray-1);
		begin_ptn_in.y_coor = center_bottom.y_coor;

		stop_ptn.x_coor = center_top.x_coor+ray;
		stop_ptn.y_coor = center_top.y_coor;
		stop_ptn_in.x_coor = center_top.x_coor+(ray-1);
		stop_ptn_in.y_coor = center_top.y_coor;

		minus20degree.x_coor = center_bottom.x_coor;
		zerodegree.x_coor = center_bottom.x_coor;
		plus20degree.x_coor = center_bottom.x_coor;
		plus40degree.x_coor = center_bottom.x_coor;

		minus20degree.y_coor = center_top.y_coor-(space/2)-(3*space);
		zerodegree.y_coor = center_top.y_coor-(space/2)-(2*space);
		plus20degree.y_coor = center_top.y_coor-(space/2)-(1*space);
		plus40degree.y_coor = center_top.y_coor-(space/2);

	}
	if (center_bottom.y_coor==center_top.y_coor)
	{
		isHorizontal = true;
		space = (center_top.x_coor-center_bottom.x_coor)/5;
		begin_ptn.x_coor = center_bottom.x_coor;
		begin_ptn.y_coor = center_bottom.y_coor-ray;
		begin_ptn_in.x_coor = center_bottom.x_coor;
		begin_ptn_in.y_coor = center_bottom.y_coor-(ray-1);

		stop_ptn.x_coor = center_top.x_coor;
		stop_ptn.y_coor = center_top.y_coor+ray;
		stop_ptn_in.x_coor = center_top.x_coor;
		stop_ptn_in.y_coor = center_top.y_coor+(ray-1);

		minus20degree.y_coor = center_bottom.y_coor;
		zerodegree.y_coor = center_bottom.y_coor;
		plus20degree.y_coor = center_bottom.y_coor;
		plus40degree.y_coor = center_bottom.y_coor;

		minus20degree.x_coor = center_top.x_coor-(space/2)-(3*space);
		zerodegree.x_coor = center_top.x_coor-(space/2)-(2*space);
		plus20degree.x_coor = center_top.x_coor-(space/2)-(1*space);
		plus40degree.x_coor = center_top.x_coor-(space/2);


	}

	if (isHorizontal){
		minus203.x_coor = minus20degree.x_coor + 4;
		minus203.y_coor = minus20degree.y_coor + 20;

		minus202 = minus203;
		minus202.y_coor += 8;

		minus201 = minus202;
		minus201.y_coor += 8;

		zero1.x_coor = zerodegree.x_coor + 4;
		zero1.y_coor = zerodegree.y_coor + 20;

		plus202.x_coor = plus20degree.x_coor + 4;
		plus202.y_coor = plus20degree.y_coor + 20;

		plus201 = plus202;
		plus201.y_coor += 8;

		plus402.x_coor = plus40degree.x_coor + 4;
		plus402.y_coor = plus40degree.y_coor + 20;

		plus401 = plus402;
		plus401.y_coor += 8;

	}
	else
	{
		minus203.x_coor = minus20degree.x_coor -20;
		minus203.y_coor = minus20degree.y_coor + 4;

		minus202 = minus203;
		minus202.x_coor -= 8;

		minus201 = minus202;
		minus201.x_coor -= 8;

		zero1.x_coor = zerodegree.x_coor -20;
		zero1.y_coor = zerodegree.y_coor + 4;

		plus202.x_coor = plus20degree.x_coor - 20;
		plus202.y_coor = plus20degree.y_coor + 4;

		plus201 = plus202;
		plus201.x_coor -= 8;

		plus402.x_coor = plus40degree.x_coor - 20;
		plus402.y_coor = plus40degree.y_coor + 4;

		plus401 = plus402;
		plus401.x_coor -= 8;
	}



//************ drow the thermo form ************
	display_circle(center_bottom, ray*2, color);
	display_circle(center_top, ray, color);
	display_rect(begin_ptn, stop_ptn, color);
	display_line(minus20degree, 10, isHorizontal, color);
	display_caract(minus201, '-', color, isHorizontal);
	display_caract(minus202, '2', color, isHorizontal);
	display_caract(minus203, '0', color, isHorizontal);
	display_line(zerodegree, 10, isHorizontal, color);
	display_caract(zero1, '0', color, isHorizontal);
	display_line(plus20degree, 10, isHorizontal, color);
	display_caract(plus201, '2', color, isHorizontal);
	display_caract(plus202, '0', color, isHorizontal);
	display_line(plus40degree, 10, isHorizontal, color);
	display_caract(plus401, '4', color, isHorizontal);
	display_caract(plus402, '0', color, isHorizontal);

	ptn_btm = center_bottom;
	ptn_tp = center_top;
	bgn_ptn = begin_ptn_in;
	stp_ptn_high = stop_ptn_in;
	ray_in = ray-1;

	// calculate the value between the low value and the high value to be almost right
	// with the graduation in the term form.
	if (isHorizontal){
		echelle = plus40degree.x_coor - minus20degree.x_coor;
	}
	else {
		echelle = plus40degree.y_coor - minus20degree.y_coor;
	}

}

void display_update_termo(int value_actuel){
	if (hasChangedColor){
		hasChangedColor = false;
		display_thermo_basic_form(start_point, end_point, 5, color_thermo[0]);
		display_circle(ptn_btm, ray_in*2, BLACK);
		display_rect(bgn_ptn, stp_ptn_high, BLACK);
		display_circle(ptn_tp, ray_in, BLACK);
	}
	// when the termo is used for the frst time only. init all values.
	if (low_value == -2000 && high_value == 2000){
		low_value = value_actuel;
		high_value = value_actuel;
		act_value = value_actuel;
		stp_ptn_low = minus20degree;
		stp_ptn_actu = minus20degree;
		stp_ptn_high = minus20degree;
		hasChangetemp = true;


		if (isHorizontal){
			stp_ptn_low.y_coor +=ray_in;
			stp_ptn_actu.y_coor +=ray_in;
			stp_ptn_high.y_coor +=ray_in;
		}
		else{
			stp_ptn_low.x_coor +=ray_in;
			stp_ptn_actu.x_coor +=ray_in;
			stp_ptn_high.x_coor +=ray_in;
		}
	}

	//"""**** look if one of the parameter has changed.**********
	if (act_value != value_actuel){
		act_value = value_actuel;
		hasChangetemp = true;
	}

	if (low_value > value_actuel){
		if(act_value < -20){
			low_value = -20;
		}
		else{
			low_value = act_value;
		}
		hasChangetemp = true;
	}

	if (high_value < value_actuel){
		if(act_value> 40){
			high_value = 40;
		}
		else{
			high_value = act_value;
		}
		hasChangetemp = true;
	}


	if (isHorizontal){
		stp_ptn_low.x_coor = minus20degree.x_coor + (uint32_t)(((float)echelle/60)*(low_value+20));
		bgn_ptn_actu = stp_ptn_low;
		bgn_ptn_actu.x_coor +=1;
		bgn_ptn_actu.y_coor -= 2*ray_in;
		stp_ptn_actu.x_coor = minus20degree.x_coor + (uint32_t)(((float)echelle/60)*(act_value+20));
		bgn_ptn_high = stp_ptn_actu;
		bgn_ptn_high.x_coor +=1;
		bgn_ptn_high.y_coor -= 2*ray_in;
		stp_ptn_high.x_coor = minus20degree.x_coor + (uint32_t)(((float)echelle/60)*(high_value+20));
	}
	else {
		stp_ptn_low.y_coor = minus20degree.x_coor + (uint32_t)(((float)echelle/60)*(low_value+20));
		bgn_ptn_actu = stp_ptn_low;
		bgn_ptn_actu.x_coor +=1;
		bgn_ptn_actu.y_coor -= 2*ray_in;
		stp_ptn_actu.y_coor = minus20degree.x_coor + (uint32_t)(((float)echelle/60)*(act_value+20));
		bgn_ptn_high = stp_ptn_actu;
		bgn_ptn_high.x_coor +=1;
		bgn_ptn_high.y_coor -= 2*ray_in;
		stp_ptn_high.y_coor = minus20degree.x_coor + (uint32_t)(((float)echelle/60)*(high_value+20));
	}
	sprintf(max_value_to_display,"M:%-3d",high_value);
	sprintf(actuel_value_to_display,"C:%-3d",act_value);
	sprintf(low_value_to_display,"m:%-3d",low_value);

	display_rect(bgn_ptn_high, stp_ptn_high, color_thermo[3]);
	display_rect(bgn_ptn_actu, stp_ptn_actu, color_thermo[2]);
	display_rect(bgn_ptn, stp_ptn_low, color_thermo[1]);
	display_circle(ptn_btm, ray_in*2, color_thermo[1]);
	display_array_caract(max_value_to_display,high_value_array_pos, color_thermo[3], isHorizontal);
	display_array_caract(actuel_value_to_display, act_value_array_pos,  color_thermo[2], isHorizontal);
	display_array_caract(low_value_to_display, low_value_array_pos,  color_thermo[1], isHorizontal);
	hasChangetemp = false;

}

void display_array_caract(char array[], struct display_point position, uint32_t color, bool isHorizontal){
	for (unsigned int i=0; array[i] != '\0';i++){
		display_caract(position, array[i], color, isHorizontal);
		if (isHorizontal){
			position.y_coor -= 8;
		}
		else{
			position.x_coor+= i*8;
		}
	}
}

void display_caract(struct display_point position, uint8_t caract, uint32_t color, bool isHorizontal)
{
	char test = 0;
	if (isHorizontal)
	{
		for (int i=0;i<8;i++)
		{
			oled_memory_size(position.x_coor-i, position.x_coor -i, position.y_coor-8, position.y_coor);
			test = fontdata_8x8[caract][i];


			for (int j=0;j<8;j++)
			{
				if (test>= 128)
				{
					oled_color(color);
				}
				else
				{
					oled_color(0);
				}
				test= test<<1;

			}
		}
	}
	else
	{
		for (int i=0;i<8;i++)
		{
			oled_memory_size(position.x_coor, position.x_coor +8, position.y_coor-i, position.y_coor-i);
			test = fontdata_8x8[caract][i];


			for (int j=0;j<8;j++)
			{
				if (test>= 128)
				{
					oled_color(color);
				}
				else
				{
					oled_color(0);
				}
				test= test<<1;
			}
		}
	}
}
