#pragma once
#ifndef DISPLAY_H
#define DISPLAY_H

/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of dislpay.c
 *
 * Purpose:  This module provides basics functions for making forms on the oled screen.
 *
 * Author:	Tobias Moullet & Mathis Raemy
 * Date: 	5.11.18
 */
#include <stdint.h>
#include <stdbool.h>

struct display_point {
   uint32_t  x_coor;
   uint32_t y_coor;
};

/**
 * This enum is used to choice witch part of the thermo you want to change the color:
 * 			BACKGRUND --> this the base of the thermo
 * 			LOW_COLOR --> this the low temperature color
 * 			ACT_COLOR --> this the actual temperature color
 * 			HIGH_COLOR--> this the high value temperature
 */
enum PLACE_COLOR {BACKGRUND, LOW_COLOR, ACT_COLOR, HIGH_COLOR};

/**
 * This method is used to change color of the part selected by the enum PLACE_COLOR
 */
extern void change_color(enum PLACE_COLOR place, uint16_t newColor);

/**
 * This method is used to initialize the display
 */
extern void display_init();

/**
 * With this method, you can clear all the pixels of the screen. You put all of them with the color black
 */
extern void display_clear();

/**
 * With this method, you can draw a cercle where you want on the screen and with the color you want
 *
 *                 ---
 *              -       -
 *              -   *   -
 *              -       -
 *                 ---
 *
 *  A display_point (*) is needed for the center.
 *  A ray and a color.
 */
extern void display_circle(struct display_point center,int32_t ray, uint32_t color);

/**
 *This method draws a caract. You need to have a display_point for then position.
 *
 * position
 *    *---------
 *    | caract |
 *    ----------
 */
extern void display_caract(struct display_point position, uint8_t caract, uint32_t color, bool isHorizontal);

/**
 * This method could be use to display a chain of caracts where you want on the screen.
 *
 * position
 *    *---------------------------------
 *    |                                |
 *    |    chain of caract             |
 *    ----------------------------------
 */
extern void display_array_caract(char array[], struct display_point position, uint32_t color, bool isHorizontal);

//tell to the display to display the video
extern void display_video();

#endif
