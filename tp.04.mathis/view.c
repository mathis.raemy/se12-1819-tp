/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of display.c
 *
 * Purpose:	This module implements basic services to draw forms, caract and thermo on the screen.
 *
 * Author:	Raemy Mathis
 * Date: 	03.12.2018
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "view.h"
#include "display.h"

#define sizeOfText	8
#define SCREEN_WIDTH	96
#define SCREEN_HEIGHT	96
#define GREEN			0x0ff0
#define RED				0xf800
#define WHITE			0xffff

static struct display_point StrReadyPosition;
static struct display_point StrPressPosition;
static struct display_point StrReactionPosition;
static struct display_point StrTimeMadePosition;
static struct display_point CircleReadyPosition;
static struct display_point CirclePressPosition;
static struct display_point StrReadyPosition;
static struct display_point StrMsPosition;

static char StrReady[] = "Ready";
static char StrPress[] = "Press";
static char StrReaction[] = "Reaction:";
static char StrMs[] = "ms";


void view_init(){
	display_init();
	StrReadyPosition.x_coor = SCREEN_HEIGHT-3;
	StrReadyPosition.y_coor = SCREEN_WIDTH-3;
	StrPressPosition.x_coor = StrReadyPosition.x_coor-10;
	StrPressPosition.y_coor = StrReadyPosition.y_coor;
	StrReactionPosition.x_coor = SCREEN_HEIGHT / 2;
	StrReactionPosition.y_coor = StrReadyPosition.y_coor;
	CircleReadyPosition.x_coor = StrReadyPosition.x_coor-4;
	CircleReadyPosition.y_coor = StrReadyPosition.y_coor-7*8;		//place a circle 8 character space after string
	CirclePressPosition.x_coor = StrPressPosition.x_coor-4;
	CirclePressPosition.y_coor = CircleReadyPosition.y_coor;
	StrMsPosition.x_coor = StrReactionPosition.x_coor -10;
	StrMsPosition.y_coor = SCREEN_WIDTH/2;
	StrTimeMadePosition.x_coor = StrMsPosition.x_coor;
}
void view_restartGame(){
	display_clear();
}

void view_loadIdleState(){
	display_array_caract(StrReady, StrReadyPosition, WHITE, true);
	display_array_caract(StrPress, StrPressPosition, WHITE, true);
	display_circle(CirclePressPosition, 4, WHITE);
	display_circle(CircleReadyPosition, 4, WHITE);
}

void view_ChangeRdyColor(uint16_t color){
	display_array_caract(StrReady, StrReadyPosition, color, true);
	display_circle(CircleReadyPosition, 4, color);
}

void view_ChangePressColor(uint16_t color){
	display_array_caract(StrPress, StrPressPosition, color, true);
	display_circle(CirclePressPosition, 4, color);
}

void view_GameWin(uint32_t time){
	display_array_caract(StrReaction, StrReactionPosition, RED, true);
	char StrTime[10];
	snprintf(StrTime, 10, "%d", (int)time);
	StrTimeMadePosition.y_coor = StrMsPosition.y_coor + strlen(StrTime)*8;
	display_array_caract(StrTime, StrTimeMadePosition, RED, true);
	display_array_caract(StrMs, StrMsPosition, RED, true);

}

void view_GameLoose(){
	display_video();
}


