#pragma once
#ifndef FRAME_H_
#define FRAME_H_
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract: OLED-C Click Board (SPEPS114A - LCD)
 *
 * Purpose:	this module give the interface for the 5 frame for the game over's video
 *
 * Author: 	Mathis Raemy
 * Date: 	3.12.2018
 */
struct frame {
  unsigned int 	 width;
  unsigned int 	 height;
  unsigned int 	 bytes_per_pixel; /* 2:RGB16, 3:RGB, 4:RGBA */
  unsigned char	 pixel_data[96 * 96 * 2 + 1];
  struct frame*  next;
};

//return a chain of picture;
extern struct frame* getFrame();

#endif /* FRAME_H_ */
