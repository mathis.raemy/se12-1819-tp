/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of display.c
 *
 * Purpose:	This module implements basic services to draw forms, caract and thermo on the screen.
 *
 * Author:	Tobias Moullet & Mathis Raemy
 * Date: 	5.11.18
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include "oled.h"
#include "font_8x8.h"
#include "display.h"
#include "frame.h"
#define SCREEN_WIDTH	96
#define SCREEN_HEIGHT	96
#define nbPixel			SCREEN_HEIGHT*SCREEN_WIDTH
#define nbFrame			5
#define BLACK			0
#define BLUE			0x001f
#define RED				0xf800
#define WHITE			0xffff


//************Init  Statics variables ****************
static struct frame* frameChain;
static struct pixel video[nbFrame][nbPixel] ;



void display_video(){
	oled_memory_size (0,95,0,95);
	for(int i = 0; i < nbFrame; i++){
		oled_send_image(video[i], nbPixel);
	}
}

void convertToPixelArray(){
	for(int f = 0; f<nbFrame; f++){
	int j=0;
	    for (int y=0;y<96;y++) {
	        for (int x=96;x>0;x--) {
	            video[f][x+96*y-1].lword = frameChain->pixel_data[j++];
	            video[f][x+96*y-1].hword = frameChain->pixel_data[j++];
	        }
	    }
	    frameChain = frameChain->next;
	}
}

void display_init(){
	oled_init();
	display_clear();
	frameChain = getFrame();
	convertToPixelArray();
}
void display_clear()
{
	oled_init();
	oled_memory_size(0, SCREEN_WIDTH - 1, 0, SCREEN_WIDTH - 1);
	for(unsigned int y = 0; y< SCREEN_HEIGHT; y++){
		for(unsigned int x=0; x< SCREEN_WIDTH; x++){
				oled_color(BLACK);
		}
	}
}
void display_circle(struct display_point center,int32_t ray, uint32_t color)
{
	int size =0;
	for (int i =-(ray); i <= ray; i++)
	{
		size = sqrt((ray*ray)-(i*i));
		oled_memory_size(center.x_coor-size, center.x_coor+size, center.y_coor+i, center.y_coor+i);
		for (int r=0;r<=(2*size);r++){
			oled_color(color);
		}
	}
}

void display_array_caract(char array[], struct display_point position, uint32_t color, bool isHorizontal){
	for (unsigned int i=0; array[i] != '\0';i++){
		display_caract(position, array[i], color, isHorizontal);
		if (isHorizontal){
			position.y_coor -= 8;
		}
		else{
			position.x_coor+= i*8;
		}
	}
}

void display_caract(struct display_point position, uint8_t caract, uint32_t color, bool isHorizontal)
{
	char test = 0;
	if (isHorizontal)
	{
		for (int i=0;i<8;i++)
		{
			oled_memory_size(position.x_coor-i, position.x_coor -i, position.y_coor-8, position.y_coor);
			test = fontdata_8x8[caract][i];
			for (int j=0;j<8;j++)
			{
				if (test>= 128)
				{
					oled_color(color);
				}
				else
				{
					oled_color(0);
				}
				test= test<<1;

			}
		}
	}
	else
	{
		for (int i=0;i<8;i++)
		{
			oled_memory_size(position.x_coor, position.x_coor +8, position.y_coor-i, position.y_coor-i);
			test = fontdata_8x8[caract][i];
			for (int j=0;j<8;j++)
			{
				if (test>= 128)
				{
					oled_color(color);
				}
				else
				{
					oled_color(0);
				}
				test= test<<1;
			}
		}
	}
}
