/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:  This module is the game master of this program, it manage all the game.
 *
 * Author:	Mathis Raemy
 * Date: 	03.12.2018
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "game_master.h"
#include "view.h"
#include "timer.h"
#include "buttons.h"

#define MaxTime		2500
#define MinTime		500
#define WHITE		0xffff
#define ORANGE		0xf480
#define GREEN		0x0ff0
#define timeCounter	DMTimer_4
#define MASK		(BUTTONS_2)

uint32_t totalTime;
static uint32_t rdmTime;

enum etat {
		Idle, Loose, Win, Waiting_time, Waiting_reseal,
};

static enum etat etat_present;

void init_game(){
	view_init();
	dmtimer_init(timeCounter);
	buttons_init();
	etat_present = Idle;
}


void gameProcess(){
	if(etat_present==Idle){
		view_loadIdleState();
		if(buttons_get_state(MASK)){
			rdmTime = rand()%(MaxTime-MinTime)+MinTime;			//modulo used to be sure to never be bigger then MaxTime
			dmtimer_reset_counter(timeCounter);
			etat_present = Waiting_time;
		}
	}else if(etat_present == Waiting_time){
		totalTime = dmtimer_get_counter(timeCounter)/(dmtimer_get_frequency()/1000);
		view_ChangePressColor(ORANGE);
		view_ChangeRdyColor(WHITE);
		if( totalTime< rdmTime){
			if(!buttons_get_state(MASK)){
				etat_present = Loose;
			}
		}else if(totalTime> rdmTime && buttons_get_state(MASK)){
			dmtimer_reset_counter(timeCounter);
			etat_present = Waiting_reseal;
		}

	}else if(etat_present == Waiting_reseal){
		view_ChangeRdyColor(GREEN);
		if(!buttons_get_state(MASK)){
			totalTime = dmtimer_get_counter(timeCounter)/(dmtimer_get_frequency()/1000);
			etat_present = Win;
		}

	}else if(etat_present == Win){
		view_ChangePressColor(WHITE);
		view_GameWin(totalTime);
		if(buttons_get_state(MASK)){
			view_restartGame();
			etat_present = Idle;
		}
	}else if(etat_present == Loose){
		view_GameLoose();
		if(buttons_get_state(MASK)){
			view_restartGame();
			etat_present = Idle;
		}
	}
}

