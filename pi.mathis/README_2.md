# PI.2 : Traitement des interruptions et exceptions au niveau du µP

## Objectifs

A la fin du laboratoire, les étudiant-e-s seront capables de

* Décrire le traitement des interruptions et exceptions des µP ARM
* Concevoir et développer une application mixte C - assembleur permettant de traiter les interruptions et exceptions de bas niveau d'un µP ARM Cortex-A8
* Débogguer une application mixte C - assembleur traitant des interruptions
* Etudier le datasheet du µP ARM Cortex-A8 et du µP TI AM335x

Durée du travail pratique

* 1 séance de laboratoire + travail personnel

Livrables

* Le code et le journal de travail seront rendus au travers du dépôt Git centralisé
  * sources: _.../tp/pi_
  * rapport: _.../tp/pi/doc/report\_2.pdf_

* Le journal et le code doivent être rendus au plus tard dans les 7 jours après la fin du TP.

## Travail à réaliser

L'objectif de ce travail est la réalisation d'une infrastructure pour le traitement des interruptions pouvant être levées au niveau du processeur. Cela consiste :

* Au développement d'une bibliothèque pour le traitement des interruptions et des exceptions d'un µP ARM Cortex-A8. Cette bibliothèque se compose des 3 modules suivants:
   * Le module _`exception`_ pour un traitement basique des exceptions
   * Le module _`interrupt`_ pour le traitement de bas niveau (niveau du µP) des interruptions et exceptions
   * Le module _`interrupt_asm`_ pour la réalisation en assembleur des méthodes de traitement des interruptions nécessitant l'utilisation d'instructions spécifiques au µP
* A l'intégration de la bibliothèque dans un programme principale _`main.c`_ pour sa validation

![Software Structure](img/2_sw_struct.png)

La spécification détaillée de chaque module et de leur interface se trouve plus bas dans ce document.

## Définitions

Quelques définitions utiles :

* Interrupt Sources : signal ou événement capable de lever une interruption ou de générer une exception
* Interrupt Vector Table : table contenant l'instruction permettant de traiter l'événement
* Interrupt Vector : contenu de la table des vecteurs, dans le cas de notre µP une instruction
* Interrupt Vector Number : index dans la table des vecteurs d'interruptions 

## Traitement des interruptions par le µP TI AM335x

Lors de la levée d'une interruption (hardware, software ou exception), les µP ARM évaluent les 7 sources possibles d'interruptions, soit reset (`reset`), undefined instruction (`undef`), data abort(`data`), prefetch abort (`inst`), software interrupt (`svc`), interrut request (`irq`) et fast interrupt resquest (`fiq`), pour déterminer le vecteur d'interruption. Sur le µP TI AM335x, le vecteur `reset` est traité directement par le µP et le vecteur `fiq` n'est pas disponible.

![µP Interrupt Handling](img/2_hw_handling.png)

Une fois la source d'interruption identifiée, le µP suspend l'exécution du programme en court. Il bascule son mode de fonctionnement pour le mode associé à la source d'interruption (`irq`, `svc`, `abt`, ou `und`). Il sauve ensuite l'adresse de la prochaine instruction à exécuter dans le registre `LR` ainsi que le contenu du registre `CPSR` dans le registre `SPSR` du nouveau mode de fonctionnement. Il met ensuite à jour le mode de fonctionnement du µP dans le registre `CPSR` et bloque la levée d'autres interruptions matérielles. Finalement, il exécutera l'instruction stockée dans la table des vecteurs correspondante à la source d'interruption.

En résumé, une fois qu'une interruption est levée, le µP effectue les opérations suivantes:

1. Identification de la source d'interruption et ainsi du nouveau mode de fonctionnement du µP (`mode`)
3. Sauvegarde de l'adresse de retour et du `CPSR` (`LR_mode='Return Address'` et `SPSR_mode=CPSR`)
2. Basculement du mode fonctionnement du µP (`CPSR.M[4:0]=mode`)
4. Désactivation des interruptions matérielles (`CPSR.I=1`)
5. Appel de la routine de traitement d'interruption (`vector_table[vector_number]()`)

Il est important de rappeler ici que la table des vecteurs sur les processeurs ARM contient une et une seule instruction par source d'interruption. Le µP exécutera cette instruction lorsqu'il identifiera une interruption pour cette source.

Sur les processeurs ARM Cortex-A8 la table des vecteurs peut être placée librement en mémoire. La seule contrainte est que celle-ci doit être alignée sur 32 bytes (2**5). Le co-processeur P15 offre un service pour spécifier l'adresse de base de la table des vecteurs.

```
   ldr   r0, =vector_table
   mcr   p15, #0, r0, c12, c0, #0
```

## Traitement des interruptions par le logiciel

Une fois la source d'interruption identifiée et que le µP a lancé l'exécution de l'instruction stockée dans la table des vecteurs d'interruption (code correspondant à la source d'interruption), le traitement de l'interruption se poursuit dans la routine de bas niveau (Low Level Interrupt Service Routine). Cette routine est chargée de sauvegarder le contexte du programme en cours d'exécution (Main Program), puis d'appeler la routine de traitement au niveau applicatif (Application Interrupt Service Routine), avant de restaurer le contexte pour finalement revenir à l'exécution du programme principal.

![Software Interrupt Handling](img/2_sw_handling.png)

Hormis pour la source `reset`, traitée directement un micro-code stocké dans la boot-rom du µP, et le source `reserved`, réservé à un futur usage, les 6 autres sources d'interruptions exigent un traitement similaire au niveau du µP. Le code ci-dessous indique les opérations à effectuer.

```
    sub    lr, #OFFSET          // only if necessary
    stmfd  sp!, {r0-r12,lr}     // save full context and return address
    mov    r0, #VECTOR_NR       // indicate vector number (source)
    bl     interrupt_handler    // process interrupt
    ldmfd  sp!, {r0-r12,pc}^    // restore the context (pc & cpsr)
```

Dans le code ci-dessus, l'appel de la routine `interrupt_handler` sert d'intermédiaire à l'appel de la routine de traitement de l'interruption au niveau applicatif. Cette technique offre un plus grand confort pour la réalisation de _listener_ utilisés pour accrocher et traiter des interruptions au niveau applicatif. La méthode `interrupt_handler` sera réalisé en langage C dans le module _`interrupt.c`_. Les constantes _`OFFSET`_ et _`VECTOR_NR`_ sont spécifiques à chaque source d'interruptions et doivent impérativement être adaptées.

### Table des vecteurs

L'implémentation de la table des vecteurs est assez simple si l'on utilise le langage assembleur. Il suffit de définir une table avec les instructions pour appeler les routines de traitement des interruptions de bas niveau (les _handlers_). On prendra soin à n'utiliser que l'instruction `b` et non pas `bl`.  Il est en effet essentiel de ne pas écraser le contenu du registre `LR` contenant l'adresse de retour vers le programme principal.

```
   .text
	.align 5
vector_table:
1:	b	1b		// reset
	b	undef_handler
	b	svc_handler
	b	prefetch_handler
	b	data_handler
1:	b	1b		// reserved
	b	irq_handler
	b	fiq_handler
```

Les sources `reset` et `reserved` n'étant pas levés par le processeur, il n'est pas nécessaire d'appeler une routine de traitement. Cependant, pour des raisons de fiabilité et de robustesse du code, il faut éviter d'exécuter une autre routine si le µP appelle quand même l'une de ces sources. Pour cela l'instruction `"1: b 1b"`, permet de forcer le µP à boucler sur elle-même.

### Routines de traitement des interruptions de bas niveau

Comme vu précédemment, toutes les routines de traitement des interruptions de bas niveau sont similaires. Seuls l'offset et le numéro du vecteur varient d'une routine à l'autre. En assembleur, il existe le concept de _macro_. La macro permet de définir une suite d'instructions que l'on peut paramétriser à l'aide d'arguments. Ici, celle-ci pourrait prendre la forme suivante:

```
.macro ll_interrupt_handler offset, vector_nr
   nop
   .if \offset != 0            // adjust return address
   sub    lr, #\offset         // only if necessary
   .endif
   stmfd  sp!, {r0-r12,lr}     // save full context and return address
   mov    r0, #\vector_nr      // indicate vector number (source)
   bl     interrupt_handler    // process interrupt
   ldmfd  sp!, {r0-r12,pc}^    // restore the context (pc & cpsr)
.endm
```

On constate qu'il est possible de tester si l'offset est nulle. Dans ce cas, la correction du registre `LR` n'est pas utile.

Avec cette macro, les routines de traitement pourront être définies comme suit:

```
   .text
undef_handler:		ll_interrupt_handler 0, INT_UNDEF
svc_handler:		ll_interrupt_handler 0, INT_SWI
prefetch_handler:	ll_interrupt_handler 4, INT_PREFETCH
data_handler:		ll_interrupt_handler 4, INT_DATA
irq_handler:		ll_interrupt_handler 4, INT_IRQ
fiq_handler:		ll_interrupt_handler 4, INT_FIQ
```

### Initialisation du µP

Deux opérations sont nécessaires à l'initialisation du µP pour le traitement des interruptions. La première opération consiste à initialiser le registre `SP` des différents modes du processeur, afin que lorsqu'une interruption ou une exception est levée le µP puisse la traiter correctement. Cette opération passe d'abord par la création d'une pile en réservant une zone mémoire dans la RAM. Puis, on initialisera le registre `SP` avec l'adresse la plus de cette pile. Le code ci-dessous donne un exemple d'implémentation pour l'un des modes du µP.

```
   .bss
   .align 4
und_s: .space 0x2000           // reserve stack of the undef mode
UNDEF_STACK_TOP:               // points to the top of the stack

   .text
   msr   cpsr_c, #0xdb         // switch to undef mode
   ldr   sp, =UNDEF_STACK_TOP  // init SP with top stack address
```

La deuxième opération consite à indiquer au processeur l'adresse de base de la table de vecteur. Pour cela, on utilisera les instructions indiquées dans le chapitre _Traitement des interruptions par le µP TI AM335x_.

### Interrupt Handler

Pour simplifier le traitement d'interruptions et d'exceptions, la bibliothèque de traitement des interruptions implémente le mécanisme de _'event listener'_. Ce mécanisme permet au logiciel applicatif d'accrocher une fonction à l'infrastructure de traitement des interruptions pour un événement donné (interruption ou exception du µP). Lors de la levée de l'interruption, l'infrastructure de traitement appellera cette fonction afin d'effectuer les traitements souhaités et nécessaires.

Pour réaliser une infrastructure générique, tous les _listener_ (routines de traitement des interruptions) auront la même signature, soit

```c
void interrupt_service_routine (enum interrupt_vectors vector_nr, void* param);
```
Le paramètre _`vector_nr`_ identifie la source ayant levé l'interruption. Le paramètre _`param`_ est un paramètre générique passé à la bibliothèque de traitement des interruptions lors de l'accrochage de l'`Interrupt Service Routine`. Il peut ainsi servir de référence sur des données applicatives et éviter ainsi l'obligation d'utiliser des données globales pour le traitment de l'événement.

Avec ce mécanisme, la réalisation de la routine _`interrupt_handler`_, appelée depuis la routine de traitement des interruptions de bas niveau, est considérablement simplifiée. Celle-ci peut prendre la forme suivante :

```c
static struct listener {
    interrupt_service_routine_t routine;
    void* param;
} listeners[INT_NB_VECTORS];

void interrupt_handler (enum interrupt_vectors vector_nr)
{
    struct listener* listener = &listeners[vector];
    if (listener->routine != 0) {
        listener->routine (vector_nr, listener->param);
    } else {
        pr_debug("No interrupt service routine hooked to the vector# %d. Freezing!\n", vector_nr);
        while(1);
    }
}
```

Afin que cette fonction _`interrupt_handler`_ puisse être directement appelée depuis un code en assembleur, il est impératif que celle-ci soit public. Il **ne** faudra donc **pas** utiliser le mot clef `static` en préfixe.

## Simulation des exceptions et interruptions

A des fins de validation, il est possible de simuler les exceptions capturées par le µP, ainsi que les interruptions logicielles. Bien que l'infrastructure le permette, les interruptions matérielles seront traitées lors d'un prochain travail pratique. Les codes ci-dessous donnent quelques exemples de simulation.

### Software Interrupt

La génération d'une interruption logicielle s'effectue avec l'aide de l'instruction assembleur `svc`. Le code ci-dessous donne un exemple comment utiliser cette instruction dans un code C.

```c
	__asm__ ("svc #1;");
```

### Undefined Instruction Interrupt

Comme indiqué dans le code ci-dessous, il est possible de générer une exception pour une instruction non définie par le µP.
```c
	__asm__ (".word 0xffffffff;");
```

### Data Abort Interrupt

Il est possible de générer une exception `Data Abort` en effectuant un accès à une donnée désalignée (_misaligned_). Le code ci-dessous donne un exemple.

```
	__asm__("ldr r0,=0x8000001;\n ldr r0,[r0];\n");
```

### Prefetch Abort Interrupt

Il est également possible de générer une exception `Prefetch Abort`, par contre, la génération de cette exception est fatale, car le µP ne sauve pas dans le registre `LR_abt` l'adresse suivant l'instruction fautive. Il n'est par conséquent pas possible de poursuivre le déroulement du programme. Voici un exemple de code :

```c
	__asm__ ("mov pc,#0x00000000;");
```

## Modules et méthodes à réaliser

La spécification des méthodes publiques de chaque module à implémenter pour la réalisation de ce travail pratique est décrite ci-dessous.

### Module _`interrupt_asm`_

Le module _`interrupt_asm.S`_ servira à l'implémentation en assembleur des méthodes pour le traitement de bas niveau des interruptions. Aucun fichier d'interface ne sera créé, car il ne sert qu'au module _`interrupt`_. La seule méthode publique de ce fichier est la routine d'initialisation de bas niveau de la logique d'interruption du µP, soit:

```c
/**
  Interrupt logic low level initialization routine.
  This method is implemented in assembler file interrupt_asm.S
 */
extern void interrupt_asm_init();
```

Cette méthode sera simplement à déclarer dans le module _`interrupt.c`_.

### Module _`interrupt`_

Le module _`interrupt.c`_ servira à l'implémentation en C des routines pour le traitement de bas niveau des interruptions, mais plus spécifiquement l'infrastructure pour les `event listener` applicatifs et la méthode _`interrupt_handler`_. Il offrira via son fichier d'interface _`interrupt.h`_ les méthodes suivantes:

```c
/**
 * ARM interrupt vector numbers 
 */
enum interrupt_vectors {
  INT_UNDEF,      ///< undefined instruction
  INT_SVC,        ///< supervisor call (software interrupt)
  INT_PREFETCH,   ///< prefetch abort (instruction prefetch)
  INT_DATA,       ///< data abort (data access)
  INT_IRQ,        ///< hardware interrupt request
  INT_FIQ,        ///< hardware fast interrupt request
  INT_NB_VECTORS
  };
```

```c
/**
 * Prototype of the interrupt service routine (listener)
 *
 * @param vector interrupt vector number
 * @param param parameter specified while attaching the isr
 */
typedef void (*interrupt_service_routine_t) (
	enum interrupt_vectors vector_nr,
	void* param);
```

```c
/**
 * Method to initialize low level resources of the microprocessor.
 * At least a 8KiB of memory will be allocated for each interrupt vector
 */
extern void interrupt_init();
```

```c
/**
 * Method to hook and unhook an interrupt service routine to a specified interrupt source
 *
 * @param vector interrupt vector number
 * @param routine interrupt service routine to hook to the specified interrupt source
 *                use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the the
 *              specified interrupt service routine
 * @return execution status, 0 if sussess, -1 if already attached
 */
extern int interrupt_on_event (enum interrupt_vectors vector_nr,
			     interrupt_service_routine_t routine,
			     void* param);
```

```c
/**
 * Method to enable interrupt requests
 */
extern void interrupt_enable();
```

```c
/**
 * Method to disable interrupt requests
 */
extern void interrupt_disable();
```

### Module _`exception`_

Le module _`exception.c`_ servira au traitement des exceptions. Seul un traitement basic devra être réalisé. Le module offrira via son fichier d'interface _`exception.h`_ une seule méthode pour son initialisation, soit :

```c
/**
 * initialization method
 */
extern void exception_init();
```

### Module _`main`_

Le module _`main.c`_ permettra d'initialiser l'infrastructure de traitement des interruptions et servira à simuler des exceptions et interruptions pouvant survenir au niveau du µP. Les méthodes des autres modules sont définies ci-dessous.
