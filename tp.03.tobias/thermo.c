#include <am335x_i2c.h>

#define  I2C2  AM335X_I2C2
#define  CLK   400000
#define  ID    0x48


void init_thermo(){
  am335x_i2c_init(I2C2,CLK);
}


int read_thermo(uint8_t data[2]){
	int status = am335x_i2c_read(AM335X_I2C2, ID, 0, data, 2);
	int temp = (status == 0) ? (int8_t)data[0] : -128;
  return temp;

}
