/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:	Introduction the ARM's assembler language
 *
 * Purpose:		First step in C and remote target debugging...
 * 				Program to show a short message on the 7-segment display
 *
 * Author: 	Tobias Moullet
 *
 * Date:    le 19.11.18
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <am335x_gpio.h>
#include "display.h"
#include "thermo.h"

// -- constants & variable declaration ---------------------------------------

// delays
#define DELAY_ON	0x09ffff
#define DELAY_OFF	0x01ffff

// macro to compute number of elements of an array
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))


static void delay(int value)
{
	while (value > 0) value--;
}

int main()
{
	struct display_point start_point;
	struct display_point end_point;
	uint8_t data[2] = {0x80,0};


	start_point.x_coor = 20;
	start_point.y_coor = 55;
	end_point.x_coor = 80;
	end_point.y_coor = 55;

	display_init(start_point,end_point);

	init_thermo();

	display_update_termo(read_thermo(data));
	display_update_termo(read_thermo(data));
	delay(1000000);
	change_color(BACKGRUND, 0x01f0);
	display_update_termo(read_thermo(data));
	display_update_termo(read_thermo(data));
	delay(2000);

	while(1){
		display_update_termo(read_thermo(data));
	}

}
