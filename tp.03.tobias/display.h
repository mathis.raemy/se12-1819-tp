#pragma once
#ifndef DISPLAY_H
#define DISPLAY_H

/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of dislpay.c
 *
 * Purpose:  This module provides basics functions for making forms on the oled screen.
 *
 * Author:	Tobias Moullet
 * Date: 	5.11.18
 */

extern struct display_point {
   uint32_t  x_coor;
   uint32_t y_coor;
};

extern enum PLACE_COLOR {BACKGRUND, LOW_COLOR, ACT_COLOR, HIGH_COLOR};

extern void change_color(enum PLACE_COLOR place, uint16_t newColor);

extern void reinit_termo();

/**
 * This method is used to initialize the display
 */
extern void display_init(struct display_point start, struct display_point end);

/**
 * With this method, you can clear all the pixels of the screen. You put all of them with the color black
 */
extern void display_clear();

/**
 * With this method, you can draw a rectangle where you want on the screen and with color you want
 *
 *     ---------------* top_rignt point
 *     -              -
 *     -              -
 *     *---------------
 *Bottom_left point
 *The color is for all pixel in the rectangle
 */
extern void display_rect(struct display_point bottom_left, struct display_point top_right, uint32_t color);

/**
 * With this method, you can draw a cercle where you want on the screen and with the color you want
 *
 *                 ---
 *              -       -
 *              -   *   -
 *              -       -
 *                 ---
 *
 *  A display_point (*) is needed for the center.
 *  A ray and a color.
 */
extern void display_circle(struct display_point center,int32_t ray, uint32_t color);

/**
 * This method draws a thermo form with several parameter
 *
 *                     ---
 *                   -     -
 *                   -  *  -
 *                   -     -
 *                   -     -
 *                   -     -
 *                   -     -
 *                   -     -
 *                 -         -
 *                 -    *    -
 *                  -       -
 *                    -----
 *
 *  For this method, it needed 2 point for each cercle
 *  A ray for the top_cercle is needed too
 *  And a color for the whole form.
 *  The bottom_cercle's ray is automaticly set 2 time bigger than the top_cercle's ray
 */
extern void display_thermo_basic_form(struct display_point center_bottom, struct display_point center_top,int32_t ray, uint32_t color);

/**
 *This method draws a caract. You need to have a display_point for then position.
 *
 * position
 *    *---------
 *    | caract |
 *    ----------
 */
extern void display_caract(struct display_point position, uint8_t caract, uint32_t color, bool isHorizontal);

/**
 * This method could be use to display a chain of caracts where you want on the screen.
 *
 * position
 *    *---------------------------------
 *    |                                |
 *    |    chain of caract             |
 *    ----------------------------------
 */
extern void display_array_caract(char array[], struct display_point position, uint32_t color, bool isHorizontal);

/**
 * This method draws a line
 *
 * start *--------------------------------------
 *
 * size is for the number of pixel you want.
 */
extern void display_line(struct display_point start, int size, bool isHorizontal, uint32_t color);

/**
 * This method is used for updating the value of the thermo.
 *
 * you just need to give the value of the temperature.
 */
extern void display_update_termo(int value_actuel);

#endif
