/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Counter and snake on Beagle Bone
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>


static char str1[] = "sum	10   20   30";
static char str2[] = "print  10   \"salutberte\" ";
static char str3[] = "command3   30   40.20   50e-2";
static char str4[] = "command4";


void tokenize (char str[])
{
	char* argv[20];
	int   argc = 0;

	printf("  line	   = \"%s\"\n", str);
	int i = 0;
	while (1) {
		while ((str[i] <= ' ') && (str[i] != '\0')) i++;
		if (str[i] == '\0') break;

		argv[argc] = &str[i];
		argc++;

		while (str[i] > ' ') i++;
		if (str[i] == '\0') break;

		str[i] = '\0';
		i++;
	}

	printf("  token[%d] = ", argc);
	for (int i = 0; i < argc; i++) printf("\"%s\" ", argv[i]);
	printf("\n\n");
}

//-- implementation of public methods ---------------------------------------

int main()
{
	printf("%c",'c');
tokenize (str1);
//	tokenize (str2);
//	tokenize (str3);
//	tokenize (str4);

	return 0;
}
