#pragma once
#ifndef GAME_MASTER_H_
#define GAME_MASTER_H_
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:  This module is the interface of the game master of this program, it manage all the game.
 *
 * Author:	Mathis Raemy
 * Date: 	03.12.2018
 */
#include <stdint.h>
#include <stdbool.h>

//initialisation of every component and initialisation of the game
extern void init_game();

//this function manage the state machine of the game
extern void gameProcess();


#endif /* GAME_MASTER_H_ */
