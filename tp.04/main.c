/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:		HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:	Introduction the ARM's assembler language
 *
 * Purpose:		This program is a reaction game
 *
 * Author: 	Raemy Mathis
 *
 * Date:    le 3.12.18
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <am335x_gpio.h>
#include "game_master.h"

int main()
{
	init_game();
	while(1){
		gameProcess();
	}
}
