#pragma once
#ifndef TIMER_H
#define TIMER_H
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of dislpay.c
 *
 * Purpose:		This module is the declaration of timer.c that give an interface to the embedded clock counter
 *
 * Author:	Tobias Moullet & Mathis Raemy
 * Date: 	5.12.18
 */

/**
 * enum of the different counter
 */
enum dmtimer_set{
	DMTimer_2,
	DMTimer_3,
	DMTimer_4,
	DMTimer_5,
	DMTimer_6,
	DMTimer_7,
};

/**
 * this method initialize the clock counter that is given as argument
 */
extern void dmtimer_init(enum dmtimer_set timer);

/**
 * this methode return the actual value of the counter that is given as argument
 */
extern uint32_t dmtimer_get_counter(enum dmtimer_set timer);

/**
 * this methode return the frequency of the clock
 */
extern uint32_t dmtimer_get_frequency();

/**
 * this methode reset the counter value
 */
extern void dmtimer_reset_counter(enum dmtimer_set timer);



#endif
