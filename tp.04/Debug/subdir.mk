################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../buttons.c \
../display.c \
../frame.c \
../game_master.c \
../main.c \
../oled.c \
../timer.c \
../view.c 

OBJS += \
./buttons.o \
./display.o \
./frame.o \
./game_master.o \
./main.o \
./oled.o \
./timer.o \
./view.o 

C_DEPS += \
./buttons.d \
./display.d \
./frame.d \
./game_master.d \
./main.d \
./oled.d \
./timer.d \
./view.d 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.c
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM GNU C Compiler'
	arm-none-eabi-gcc -mcpu=cortex-m3 -mthumb -O0 -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections  -g3 -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


