/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Counter and snake on Beagle Bone
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static char str1[] = "command1 10 20 30";
static char str2[] = "command2 10 systemembarque";
static char str3[] = "command3   30   40.20   50e-2";
static char str4[] = "command4";

void (*fct[5])(int);	//table of function pointer

//table of commande (order is important)
static const char command[][100] = {"command1", "command2", "command3", "command4"};


void tokenize (char str[])
{
	char cmd[ARRAY_SIZE(str)];
	int   argc = 0;
	int i = 0;
	while(ARRAY_SIZE(cmd) <= ARRAY_SIZE(str) && str[i] >= '0' && str[i] <= 'z'){
		cmd[i] = str[i];
		i++;
	}
	for(int h = 0; h<ARRAY_SIZE(command); h++){

		if (strcmp(command[h], cmd) == 0){
			fct[h](9);
		}
	}

}

void command1 (int a){
	printf("you called the function command1 \n");
}
void command2(int a){
	printf("you called the function command2 \n");
}
void command3 (int a){
	printf("you called the function command3 \n");
}
void command4 (int a){
	printf("you called the function command4 \n");
}
//-- implementation of public methods ---------------------------------------


int main()
{
//init of table of pointer
	fct[0] = &command1;
	fct[1] = &command2;
	fct[2] = &command3;
	fct[3] = &command4;

	tokenize(str1);
	tokenize(str2);
	tokenize(str3);
	tokenize(str4);


	return 0;
}
