/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Counter and snake on Beagle Bone
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

struct node {
	struct node* next;
	int value;
};

struct list {
	struct node first;
};

struct node generate(int v){
	struct node n = {
			NULL,
			v
	};
	return n;
}

struct node search(struct list* l,int v){
	for(struct node* i = &l->first; i != (&l->first)-1; i= i->next){

		if (i->value == v){
			printf("the value is %d \n", i->value);
//			return *i;
		}
	}
	struct node notFound = {NULL, 0};
	return notFound;
}

void add( struct list* l, struct node n){

	if(&l->first == NULL){
		l->first = n;
		printf("ajouté au début \n");
	}else if(l->first.next == NULL){
		l->first.next = &n;
		n.next = &l->first;

	}else{
		n.next = l->first.next;
		l->first.next = &n;
		printf("Ajout normal \n");
	}

}


int main()
{
	struct list* l = 0;
	add(&l, generate(15));
//	add(&l, generate(12));
//	add(&l, generate(10));
//	add(&l, generate(113));
//	add(&l, generate(125));
//	add(&l, generate(1052));
//	add(&l, generate(11233));
	add(&l, generate(9912));
	add(&l, generate(101));
	add(&l, generate(11389));
	struct node toSearch = search(&l, 15);
	if ( toSearch.next == NULL){
			printf("not found \n");
	}else{
		printf("found \n");
	}


	return 0;
}
