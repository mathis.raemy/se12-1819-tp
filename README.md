# TPs du cours de systèmes embarqués 1 et 2

## Support / Communications

Avec une adresse e-mail de l'école (@edu.hefr.ch ou @hefr.ch) vous pouvez
rejoindre le serveur de discussion slack : [heia-fr-isc-embsys.slack.com](https://heia-fr-isc-embsys.slack.com/signup).

## Informations pour les utilisateurs du nFR24L01

Pour éviter les perturbation dans les groupes qui utilisent le nRF24L01,
nous vous proposons de ["réserver" votre fréquence](https://docs.google.com/spreadsheets/d/1q90_QpZAcsDDSEQ74RTJdXkud1O6OnitF2YnRoD8sZs/edit?usp=sharing).

## CI/CD

Pour tester automatiquement votre code dans l'infrastructure de l'école, ajoutez un fichier `.gitlab-ci.yml` à votre projet avec le contenu suivant:

```
image: heiafr/se12-builder:latest

variables:
  PROJECT: "tp.00"

build:
  stage: build
  script:
    - cd ${PROJECT}
    - make clean all
```
Remplacez la variable `PROJECT` avec le projet que souhaitez valider (par exemple `pi` pour votre projet intégré).

Ajoutez aussi la ligne suivante dans votre `Makefile` (avant le `include $(LMIBASE)/bbb/make/bbb.mk`)

```
CFLAGS+=-Werror
```

Pour tester l'encodage de vos sources, vous pouvez compléter votre _script_ comme suit:

```
    - C_FILES=$(git ls-files | grep '[.]c$')
    - H_FILES=$(git ls-files | grep '[.]h$')
    - S_FILES=$(git ls-files | grep '[.]S$')
    - echo "----- Checking encoding -----"
    - file --mime-encoding $C_FILES $H_FILES $S_FILES
    - file --mime-encoding $C_FILES $H_FILES $S_FILES | grep -v 'utf-8' | grep -v 'us-ascii' | tee __CHECK_ENC__ || true
    - test ! -s __CHECK_ENC__
```

Pour tester le style, compléter encore votre _script_ comme suit:

```
    - echo "----- Checking style -----"
    - cpplint --filter=-,+whitespace/tab,+whitespace/line_length,+readability/utf8 --output=vs7 $C_FILES $H_FILES 
```

si vous voulez pouvoir télécharger le fichier binaire généré (`app_a`), ajoutez les lignes suivantes:

```
  artifacts:
    paths:
    - ${PROJECT}/app_a
```

voici à quoi ressemble un `.gitlab-ci.yml` complet:

```
image: heiafr/se12-builder:latest

variables:
  PROJECT: "tp.00"

build:
  stage: build
  script:
    - cd ${PROJECT}
    - C_FILES=$(git ls-files | grep '[.]c$')
    - H_FILES=$(git ls-files | grep '[.]h$')
    - S_FILES=$(git ls-files | grep '[.]S$')
    - echo "----- Checking encoding -----"
    - file --mime-encoding $C_FILES $H_FILES $S_FILES
    - file --mime-encoding $C_FILES $H_FILES $S_FILES | grep -v 'utf-8' | grep -v 'us-ascii' | tee __CHECK_ENC__ || true
    - test ! -s __CHECK_ENC__
    - echo "----- Checking style -----"
    - cpplint --filter=-,+whitespace/tab,+whitespace/line_length,+readability/utf8 --output=vs7 $C_FILES $H_FILES 
    - echo "----- Building -----"
    - make clean all
    - echo "----- Done -----"
  artifacts:
    paths:
    - ${PROJECT}/app_a

```

## Badge

* link: `https://gitlab.forge.hefr.ch/%{project_path}/pipelines`
* image: `https://gitlab.forge.hefr.ch/%{project_path}/badges/%{default_branch}/pipeline.svg`
