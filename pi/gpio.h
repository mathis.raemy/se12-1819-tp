/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 pi
 *
 * Abstract: Header of a GPIO interruption manager
 *
 * Purpose:	PI
 *
 * Author: 	Mathis Raemy et Tobias Moullet
 * Date: 	27.03.2019
 */

#ifndef HEADER_GPIO
#define HEADER_GPIO

#include <stdint.h>
#include <am335x_gpio.h>
#define GPIO0 AM335X_GPIO0
#define GPIO1 AM335X_GPIO0
#define GPIO2 AM335X_GPIO2

/**
 * Signature de la méthode de appelée par le listener
 *
 * @param est un pointeur non spécifié
 */
typedef void (*gpio_service_routine_t) (void* param);

enum gipo_modes{
	GPIO_DEBOUNCED = (1<<0),
	GPIO_RISING = (1<<1),
	GPIO_FALLING = (1<<2),
	GPIO_HIGH = (1<<3),
	GPIO_LOW = (1<<5)
};

/**
 * Routine d'initialisation
 */
extern void gpio_init_fct();

//methode pour attacher une methode a une interruption gpio
extern int gpio_on_event(
		enum am335x_gpio_modules module,
		uint32_t pin,
		unsigned modes,
		gpio_service_routine_t routine,
		void* param
		);


#endif /* GPIO_H_ */
