# PI.3 : Traitement des interruptions matérielles

## Objectifs

A la fin du laboratoire, les étudiant-e-s seront capables de

* Décrire le traitement des interruptions matérielles des µP ARM
* Développer les bibliothèques permettant de traiter les interruptions matérielles du µP ARM TI AM335x
* Développer des pilotes de périphériques en mode interruptif
* Développer une application en mode interruptif pure
* Etudier le datasheet du µP ARM TI AM335x

Durée du travail pratique

* 1 séance de laboratoire + travail personnel

Livrables

* Le code et le journal de travail seront rendus au travers du dépôt Git centralisé
  * sources: _.../tp/pi_
  * rapport: _.../tp/pi/doc/report\_3.pdf_

* Le journal et le code doivent être rendus au plus tard dans les 7 jours après la fin du TP.

## Travail à réaliser

L'objectif de ce travail est la réalisation d'une infrastructure pour le traitement des interruptions matérielles pouvant être levées au niveau du processeur ARM TI AM3358, ainsi que de la mise à disposition de timers.
![Software Structure][3_sw_struct]

[3_sw_struct]: img/3_sw_struct.png

Cela consiste :

   * Au développement d'une bibliothèque pour le traitement des interruptions et des exceptions et la mise à disposition de timer pour le µP ARM Cortex-A8 et plus spécifiquement du µP TI AM3358. Cette bibliothèque se compose des 3 modules suivants:
      * Au développement du module _`intc`_ pour un traitement des interruptions matérielles des périphériques internes
      * Au développement du module _`gpio`_ pour le traitement des interruptions matérielles de périphériques externes connectés aux broches d'entrée/sortie du µP
      * A l'adaptation du module _`dmtimer`_ du 4e TP pour la mise à disposition de timers

   * Afin de valider l'infrastructure, il est demandé d'implémenter un compteur avec affichage sur le display 7-segments. Pour ce faire, vous pouvez migrer une partie de l'application développée lors du 2e TP du mode par scrutation vers mode interruptif. Pour rappel, cette application met en oeuvre les boutons-poussoirs, les LEDs, l'afficheur 7-segments ainsi que l'encodeur rotatif. Les **boutons-poussoirs** et **l'encodeur rotatif** devront être **traités par interruptions**, tandis qu'un **timer rafraichira l'afficheur 7-segments**. En principe, seuls 2 modules devront être fortement adaptés, soit:
      * Le module _`wheel`_ du 2e TP pour le traitement des événements de l'encodeur rotatif (rotation vers la droite ou la gauche)
      * Le module principal _`main`_ du 2e TP, qui passera du mode par scrutation en mode interrruptif

Une spécification détaillée des modules et de leur interface se trouve plus bas dans ce document.

## Rappel

Le traitement des interruptions (Interrupt Handling) est un aspect incontournable de la programmation de systèmes embarqués. Les interruptions procurent la capacité de traiter de façon asynchrone des événements levés par le matériel. Cette capacité à traiter des événements spontanés procure une grande réactivité au système et à ses applications en offrant un traitement rapide et approprié de l’événement.   
![Principe de traitement des périphériques d'entrée/sortie][3_principe_io]

[3_principe_io]: img/3_principe_io.png

Lors de l'utilisation de système d'exploitation moderne, hormis le traitement d’événements matériels, le traitement d’interruptions permet également de traiter des événements logiciels. Le principal d’entre eux est le traitement d’appels système (syscall). Les interruptions logicielles permettent à un processus en espace utilisateur de franchir la barrière de protection en quittant son mode non privilégié pour accéder au mode privilégié du µP et ainsi aux services fournis par l’OS dans l’espace noyau.   
![Principe des appels système][3_syscall]

[3_syscall]: img/3_syscall.png

Il est intéressant de noter qu’au niveau du µP, ces deux types d’interruptions, ainsi que les exceptions, sont traités de manière similaire.   
![Traitement des interruptions au niveau du µP][3_ll_interrupt_handling]

[3_ll_interrupt_handling]: img/3_ll_interrupt_handling.png


## Traitement des interruptions matérielles

La signalisation d’événements par des périphériques d’entrées/sorties passe par plusieurs multiplexeurs avant d’arriver sur les lignes d’interruptions du µP. Ces multiplexeurs permettent de connecter une grande quantité de périphériques au µP. Ils sont également en charge d’arbitrer, de prioriser et d’identifier efficacement les périphériques ayant levé une interruption. Comme présenté sur la figure ci-dessous, les µP ARM implémentent deux multiplexeurs, le INTC et le GPIO.  
![Hardware Interrupts][3_hw_interrupts]

[3_hw_interrupts]: img/3_hw_interrupts.png

L’INTC est connecté directement au µP. Sa fonction est de multiplexer toutes les lignes d’interruptions des périphériques internes du µP vers les deux lignes d’interruptions IRQ et FIQ implémentées par les µP ARM. L’INTC permet d’arbitrer et de prioriser les différentes requêtes. Pour faciliter le traitement des requêtes par le logiciel, il livre un numéro de vecteur permettant d’identifier très efficacement la source d’interruption. Ce numéro est unique à chaque périphérique. Sur le µP TI AM3358, basé sur un µP ARM Cortex-A8, l’INTC permet de connecter jusqu’à 128 lignes d’interruptions et de les multiplexer vers la ligne IRQ du µP. Il est à noter que la ligne d’interruption FIQ n’est pas supportée par le µP TI AM3358.

Le GPIO, hormis sa fonction de module d’entrées/sorties numériques, permet de connecter au système d’interruptions du µP un périphérique d’entrées/sorties par ses broches. Le GPIO n’offre aucun support pour identifier la source d’interruption. Il ne livre dans un registre qu’un set de bits indiquant quelles broches ont levé une interruption. C’est ensuite au logiciel de scruter chaque bit pour identifier la broche à servir quitancer son traitement. 

Pour trouver la source lors d’une interruption matérielle, le µP va simplement appeler la routine de traitement d’interruption de bas niveau contenue dans la table des vecteurs. Le reste du traitement devra être exécuté par le logiciel.  
![Hardware Interrupt Hanling][3_hw_interrupt_handling]

[3_hw_interrupt_handling]: img/3_hw_interrupt_handling.png

Pour traiter des interruptions matérielles `IRQ` levées par le contrôleur INTC, il suffit d’implémenter une routine de traitement d’interruption, un _`irq_handler`_, que l’on accroche à l'événement en stockant la routine et son paramètre dans la table des listeners de la bibliothèque _`interrupt`_. Cette routine cherchera dans une première étape à identifier le périphérique interne du µP ayant levé l’interruption. Pour cela elle demandera au contrôleur INTC de lui livrer le vecteur d’interruption. Si ce vecteur correspond à un contrôleur GPIO, la routine demandera alors, dans une deuxième étape, au contrôleur GPIO de lui livrer la broche à l’origine de l’interruption. Une fois, la source identifiée, la routine de traitement au niveau de l’application, correspondante au vecteur d’interruption, sera appelée pour effectuer le traitement souhaité.

Le document `06_am335x_technical_reference_manual.pdf` fournit toutes les données techniques des contrôleurs INTC (_chapitre 6_) et GPIO (_chapitre 25_). On trouvera dans ce même document au _chapitre 6.3_ la liste des sources interruptions et leur numéro de vecteur.

## Programmation de l'INTC

Une particularité de ce contrôleur est sa capacité à traiter 128 lignes d’interruptions. Avec des registres sur 32 bits, ses concepteurs ont simplement quadruplé les registres utilisés pour identifier les 128 lignes d’interruptions. Pour un numéro de vecteur donné, on obtient le numéro du registre en divisant le numéro de vecteur par 32 et le bit à l’intérieur de ce registre par un modulo 32 sur le numéro de vecteur, p. ex. pour le vecteur 98 le numéro du registre est 98/32 => 3 et le bit correspondant à la ligne d’interruption est 98%32 => 2.

Voici la structure des registres de l'INTC:

   ```C
      static volatile struct intc_ctrl {
         uint32_t revision;
         uint32_t res1[3];
         uint32_t sysconfig;
         uint32_t sysstatus;
         uint32_t res2[10];
         uint32_t sir_irq;
         uint32_t sir_fiq;
         uint32_t control;
         uint32_t protection;
         uint32_t idle;
         uint32_t res3[3];
         uint32_t irq_priority;
         uint32_t fiq_priority;
         uint32_t threshold;
         uint32_t res4[5];
         struct {
            uint32_t itr;
            uint32_t mir;
            uint32_t mir_clear;
            uint32_t mir_set;
            uint32_t isr_set;
            uint32_t isr_clear;
            uint32_t pending_irq;
            uint32_t pending_fiq;
         } flags[4];
         uint32_t ilr[128];
      } * intc = (struct intc_ctrl*) 0x48200000;
   ```

Dans le cadre de nos laboratoires et pour le système que nous souhaitons réaliser (pas d’interruptions imbriquées), la gestion du contrôleur INTC, une fois initialisé, se limite à très peu d’opérations.

La première étape consiste à implémenter une méthode d’initialisation. Voici les étapes :

   1. Initialiser le contrôleur (_reset_)

      ```C
         intc->sysconfig = 0x2;
         while ((intc->sysstatus & 0x1) == 0);
      ```

   2. Configurer les valeurs de défaut : pas de _threshold_ et pas de _nested interrupt_

      ```C
         intc->sysconfig = 0x0;
         intc->idle = 0;
         intc->threshold = 0xff;
      ```

   3. Masquer toutes les lignes d’interruptions

      ```C
         for (int i=ARRAY_SIZE(intc->flags)-1; i>=0; i--) {
            intc->flags[i].mir = -1;
            intc->flags[i].mir_set = -1;
            intc->flags[i].isr_clear = -1;
         }
      ```

   4. Configurer le contrôleur INTC pour générer des IRQ

      ```C
         for (int i=ARRAY_SIZE(intc->ilr)-1; i>=0; i--) {
            intc->ilr[i] = 0;
         }      
   ```

L'INTC permet d'autoriser et de bloquer les interruptions matérielles.

   1. Pour autoriser la levée d’interruptions pour une source donnée :

      ```C
         intc->flags[vector_nr/32].mir_clear = 1<<(vector_nr%32);
      ```

   2. Pour bloquer la levée d’interruption pour une source donnée :

      ```C
         intc->flags[vector_nr/32].mir_set = 1<<(vector_nr%32);
      ```

Lors de la levée d'une interruption, il faudra effectuer les opérations suivantes:

   1. Pour obtenir le vecteur (la source d’interruption) :

      ```C
         int vector_nr = intc->sir_irq & 0x7f;
      ```

   2. Pour quittancer la fin du traitement de l’interruption et autoriser la levée de nouvelles interruptions :

      ```C
         intc->control = 0x1;
      ```

Ce contrôleur permet également de simuler la levée d’interruption.

   1. Pour simuler la levée d’interruption pour une source :

      ```C
     		intc->flags[vector_nr/32].isr_set = 1<<(vector_nr%32);
      ```

   2. Pour la stopper :

      ```C
     		intc->flags[vector_nr/32].isr_clear = 1<<(vector_nr%32);
      ```

## Programmation du GPIO

La bibliothèque `bbb` propose, par son module _`am335x_gpio`_, les services nécessaires au traitement des interruptions des périphériques externes au µP connectés au µP par les broches des contrôleurs GPIO.

Le service _`am335x_gpio_setup_pin_irq`_ permet de configurer une broche d’un contrôleur GPIO en ligne d’interruption. Cette méthode permet de choisir le mode de génération de l’interruption et d'activer la logique d’anti-rebond intégrée dans le contrôleur.

Les services _`am335x_gpio_enable`_ et _`am335x_gpio_disable`_ permettent d’autoriser et de bloquer la levée d’interruptions pour une broche d’un contrôleur.


Le service _`am335x_gpio_vector`_ livre le numéro de la première broche ayant levé l’interruption. L’appel de cette méthode effectue directement le quittancement de l’interruption, c.à.d. elle indique au contrôleur GPIO que l'interruption a été traitée. Si plusieurs broches lèvent une interrution au même instant, il suffit d'itérer au temps de fois que nécessaire dans la routine d'interruption, ceci jusqu'à cette méthode indique que tous les événements ont bien été traités. Il est également possible de quitter la routine d'interruption après le premier traitement. Dans ce cas le µP générera une interruption pour chaque événement.

La routine de traitment des interruptions au niveau du contrôleur GPIO peut prendre la forme suivante:

```C
static void gpio_handler(enum intc_vectors i_vector_nr, void* param)
{
   (void)i_vector_nr; // unused param
   enum am335x_gpio_modules module = (enum am335x_gpio_modules)param;

	// get pin number having raised interrupt and acknowledge it
	int pin = am335x_gpio_vector(module);
	if (pin < 0) return;

	struct listener* listener = &listeners[module][pin];
	if (listener->routine != 0) {
		listener->routine (listener->param);
	} else {
		// spurious interrupt
		am335x_gpio_disable(module, pin);
	}
}
```

Afin qu’une interruption levée au niveau d’une broche d’un contrôleur GPIO atteigne le µP, il faudra encore autoriser la levée d’interruptions des contrôleurs GPIO au niveau du contrôleur INTC, soit les lignes `GPIO0A`, `GPIO1A`, `GPIO2A` et `GPIO3A`. Ne souhaitant pas implémenter des interruptions imbriquées, il n'est pas utile de connecter les sorties B des GPIO.
```C
void gpio_init()
{
    am335x_gpio_init(AM335X_GPIO0);
    am335x_gpio_init(AM335X_GPIO1);
    am335x_gpio_init(AM335X_GPIO2);
    am335x_gpio_init(AM335X_GPIO3);
	intc_on_event (INTC_GPIO0A, gpio_handler, (void*)AM335X_GPIO0);
	intc_on_event (INTC_GPIO1A, gpio_handler, (void*)AM335X_GPIO1);
	intc_on_event (INTC_GPIO2A, gpio_handler, (void*)AM335X_GPIO2);
	intc_on_event (INTC_GPIO3A, gpio_handler, (void*)AM335X_GPIO3);
}
```

## Programmation du DMTimer

Les timers sont généralement programmés pour lever une interruption à intervalle régulier. Les DMTimer implémentent bien naturellement ce mode.   
![Timer][3_timer]

[3_timer]: img/3_timer.png

Les DMTimer sont capables de générer une interruption lorsqu'ils détectent un _`"overflow"`_ . Il suffit de charger la période dans le registre _`tldr`_ afin que lorsque l'overlow est détectée le timer recharge la valeur correspondante à la période désirée dans le registre _`tccr`_. Il faudra encore autoriser la levée d'interruption dans le registre _`irqenable_set`_. Le code ci-dessous donne un exemple d'implémentation.

```C
   #define IRQENABLE_OVF_IT_FLAG (1<<1)
   uint32_t period = period_ms * (TIMER_FREQUENCY / 1000);
   dmtimer->tcrr = -period;
   dmtimer->tldr = -period;
   dmtimer->ttgr = -period;
   dmtimer->irqenable_set = IRQENABLE_OVF_IT_FLAG;
   dmtimer->tclr = TCLR_AR | TCLR_ST;
```
La valeur de la période à charger dans les registres se calcule en fonction d'une période (millisecondes) et de la fréquence de l'horloge du timer. Pour rappel, la cible est configurée avec une horloge de 24MHz. On utilise une valeur négative, car le timer incrémente à chaque cycle d'horloge le registre _`tccr`_ jusqu'à atteindre le 0 pour générer l'interruption _`overflow`_.

Pour un traitement correct de l'interruption levée par un timer, il faudra encore la quittancer. Pour cela, il suffit de le faire à l'aide du registre _`irqstatus`_, comme suit:

```c
   #define IRQSTATUS_OVF_IT_FLAG (1<<1)
   dmtimer->irqstatus = IRQSTATUS_OVF_IT_FLAG;
```

## Programmation de l'encodeur rotatif

La détection des mouvements de rotation de l'encodeur rotatif par interruptions est plus simple et surtout plus fiable et plus robuste que par scrutation. Pour ce faire, il suffit de détecter par interruption les changements d'état (flanc montant et flanc décescendant) d'un des deux canaux de l'encodeur, dans notre cas le canal A. Puis,dans la routine de traitement d'interruption de lire l'état des canaux pour déterminer le sens de rotation. Si les 2 canaux ont le même état, la rotation s'est faite vers la gauche. Dans le cas inverse, il a été vers la droite.

```c
	enum wheel_direction dir = WHEEL_RIGHT;
	bool cha = am335x_gpio_get_state (CHA_GPIO, CHA_PIN);
	bool chb = am335x_gpio_get_state (CHB_GPIO, CHB_PIN);
	if (cha == chb) dir = WHEEL_LEFT;
```

## Modules et méthodes à réaliser

La spécification des méthodes publiques de chaque module à implémenter pour la réalisation de ce travail pratique est décrite ci-dessous.

### Module _`intc`_

Le module _`intc.c`_ servira au traitement des interruptions matérielles levées pour les périphériques internes du µP. Pour réaliser une infrastructure générique, tous les _listener_ (routines de traitement des interruptions) auront la même signature, soit:

```C
   typedef void (*intc_service_routine_t) (enum intc_vectors vector_nr, void* param);
```

Le contrôleur INTC permet de connecter 128 périphériques différents, cependant dans le cadre de ce projet, seuls les `GPIO` et les `dmtimer` sont nécessaires, voici leur numéro de vecteur:

```C
   enum intc_vectors {
      INTC_GPIO2A=32,
      INTC_GPIO2B,

      INTC_GPIO3A=62,
      INTC_GPIO3B,

      INTC_TIMER0=66,
      INTC_TIMER1,
      INTC_TIMER2,
      INTC_TIMER3,

      INTC_TIMER4=92,
      INTC_TIMER5,
      INTC_TIMER6,
      INTC_TIMER7,
      INTC_GPIO0A,
      INTC_GPIO0B,
      INTC_GPIO1A,
      INTC_GPIO1B,
   };
```

Les méthodes ci-dessous seront offertes via le module d'interface _`intc.h`_ :

```C
/**
 * initialization method
 * should be called prior any other method of this module
 */
extern void intc_init();
```

```C
/**
 * method to hook and unhook an application specific interrupt service routine
 * to the specified vector
 *
 * @param vector_nr interrupt vector number (the source)
 * @param routine interrupt service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 *
 * @return execution status: 0 <=> success, -1 <=> error
 */
extern int intc_on_event (
	enum intc_vectors vector_nr,
	intc_service_routine_t routine,
	void* param);
```

```C
/**
 * method to force/simulate an interrupt request
 *
 * @param vector_nr interrupt vector number for which the interrupt should
 * 				     be forced/simulated
 * @param force  true to force the interrupt request, false otherwise
 */
extern void intc_force (enum intc_vectors vector_nr, bool force);
```

### Module _`gpio`_

Le module _`gpio.c`_ servira au traitement des interruptions matérielles levées pour des périphériques externes connectés au µP par les broches des modules `GPIO`. Afin de réaliser une infrastructure générique, tous les _listener_ (routines de traitement des interruptions) auront la même signature, soit:

```C
   typedef void (*gpio_service_routine_t) (void* param);
```

Les contrôleurs `GPIO` peuvent lever une interruption selon le niveau du signal ou lors de changement de niveau. Ces contrôleurs implémentent également une logique anti-rebond pour des périphériques n'en disposant pas. Pour l'encodeur rotatif, celle-ci devra impérativement être activée.

```C
   enum gpio_modes {
      GPIO_DEBOUNCED = (1<<0),  ///< event generation shall be debounced
      GPIO_PULL_UP   = (1<<1),  ///< signal pull-up shall be enabled
      GPIO_RISING    = (1<<2),  ///< event generation on rising edge
      GPIO_FALLING   = (1<<3),  ///< event generation on falling edge
      GPIO_HIGH      = (1<<4),  ///< event generation on high level
      GPIO_LOW       = (1<<5),  ///< event generation on low level
   };
```

Les méthodes ci-dessous seront offertes via le module d'interface _`gpio.h`_ :

```C
/**
 * initialization method
 * should be called prior any other method of this module
 */
extern void gpio_init();
```

```C
/**
 * method to hook and unhook an gpio service routine to a specified
 * gpio interrupt source
 *
 * @param module GPIO module number
 * @param pin GPIO pin number
 * @param modes detection modes and debounced activation
 * @param routine interrupt service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
extern int gpio_on_event(
    enum am335x_gpio_modules module,
    uint32_t pin,
   	unsigned modes,
    gpio_service_routine_t routine,
    void* param);
```

### Module _`dmtimer`_

La fonctionnalité du module _`dmtimer.c`_ devra étendue pour permettre un traitement par interruption. Afin de réaliser une infrastructure générique, tous les _listener_ (routines de traitement des interruptions) auront la même signature, soit:

```C
   typedef void (*dmtimer_service_routine_t) (void* param);
```

Les méthodes ci-dessous seront offertes via le module d'interface _`dmtimer.h`_ :

```C
/**
 * method to initialize a DMTimer for interrupt service handling
 * an event will be generated at specified period interval
 *
 * @param timer DMTimer number
 * @param period_ms period in milliseconds
 */
extern void dmtimer_set_period(enum dmtimer_timers timer, uint32_t period_ms);
```

```C
/**
 * method to get the timer period interval
 *
 * @param timer DMTimer number
 * @return period in milliseconds
 */
extern uint32_t dmtimer_get_period(enum dmtimer_timers timer);
```

```C
/**
 * method to hook and unhook an timer service routine to a specified DMTimer
 *
 * @param timer DMTimer number
 * @param routine interrupt service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
extern int dmtimer_on_event(
    enum dmtimer_timers timer,
    dmtimer_service_routine_t routine,
    void* param);
```

```C
/**
 * method to start the timer
 *
 * @param timer DMTimer number
 */
extern void dmtimer_start(enum dmtimer_timers timer);
```

```C
/**
 * method to stop the timer
 *
 * @param timer DMTimer number
 */
extern void dmtimer_stop(enum dmtimer_timers timer);
```

### Module _`wheel`_

La fonctionnalité du module _`wheel.c`_ devra être fortement adaptée pour permettre un traitement par interruption. Comme pour les autres modules, un _listener_ (routines de traitement des interruptions) permettra le traitement des événements de rotation de l'encodeur rotatif, soit:

```C
   typedef void (*wheel_handler_t)(enum wheel_direction direction, void* param);
```

Les méthodes ci-dessous seront offertes via le module d'interface _`wheel.h`_ :

```c
/**
 * wheel rotation directions
 */
enum wheel_direction {WHEEL_STILL, WHEEL_RIGHT, WHEEL_LEFT};
```

```c
/**
 * method to initialize the resoures of the wheel
 * this method shall be called prior any other.
 */
extern void wheel_init();
```

```c
/**
 * method to hook and unhook a wheel service routine to a specified
 * interrupt source
 *
 * @param routine wheel service routine to hook to the specified
 *                interrupt source, use 0 to unhook the method
 * @param param parameter to be passed as argument while calling the
 *              specified interrupt service routine
 * @return execution status, 0 if success, -1 if already attached
 */
extern int wheel_on_event(wheel_handler_t routine, void* param);
```
