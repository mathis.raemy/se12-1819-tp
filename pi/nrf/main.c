/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 2
 *
 * Abstract:    NRF24 Demo Program
 *
 * Purpose:     This module implements a basic sending/receiving test
 * based on the NRF24 communication device.
 *
 * Author:      Daniel Gachet / HEIA-FR
 * Date:        05.04.2019
 */

#include <stdio.h>
#include <string.h>

#include "kernel.h"
#include "nrf24.h"
#include "thread.h"

typedef union mac {
    uint64_t a;
    char n[8];
} mac_t;

#define NRF_CHANNEL 85
#define NRF_TX_SIZE NRF24_DEFAULT_TRANSMIT_SIZE
#define NRF_ADDR_SIZE NRF24_ADDRESS_5_BYTES
#define NRF_CRC_SIZE NRF24_CRC_16_BIT
#define NRF_BAND_WIDTH NRF24_DATARATE_1_MBPS
#define NRF_OUTPUT_POWER NRF24_OUTPUT_POWER_MINUS_12_DB

static const unsigned e2dr[] = {
    [NRF24_DATARATE_250_KBPS] = 250,
    [NRF24_DATARATE_1_MBPS]   = 1000,
    [NRF24_DATARATE_2_MBPS]   = 2000,
};

static const int e2power[] = {
    [NRF24_OUTPUT_POWER_ZERO_DB]     = 0,
    [NRF24_OUTPUT_POWER_MINUS_6_DB]  = -6,
    [NRF24_OUTPUT_POWER_MINUS_12_DB] = -12,
    [NRF24_OUTPUT_POWER_MINUS_18_DB] = -18,
};

static const unsigned e2crc[] = {
    [NRF24_CRC_NONE]   = 0,
    [NRF24_CRC_8_BIT]  = 8,
    [NRF24_CRC_16_BIT] = 16,
};

static void thread_nrf_tx(void* p)
{
    (void)p;
    unsigned msg_nr = 0;
    mac_t p0_addr   = {
        .a = nrf24_get_rx_address(NRF24_PIPE_P0),
    };
    while (1) {
        char txdata[NRF_TX_SIZE];
        snprintf(txdata, sizeof(txdata) - 1, "%s: %u\n", p0_addr.n, msg_nr++);
        int status = nrf24_write(txdata, NRF_TX_SIZE);
        if (status == -1) printf("error while sending data...\n");
        thread_delay(500);
    }
}

static void thread_nrf_rx()
{
    while (1) {
        nrf24_set_receive_mode();
        for (int p = NRF24_PIPE_P1; p <= NRF24_PIPE_P5; p++) {
            if (nrf24_readable(p)) {
                char rx_data[NRF_TX_SIZE];
                int len = nrf24_read(p, rx_data, sizeof(rx_data));
                printf("%s", rx_data);
                (void)len;
            }
        }
        thread_yield();
    }
}

int main()
{
    kernel_init();
    thread_init();

    nrf24_init();
    nrf24_set_rf_channel(NRF_CHANNEL);
    nrf24_set_rf_output_power(NRF_OUTPUT_POWER);
    nrf24_set_air_datarate(NRF_BAND_WIDTH);
    nrf24_set_crc_width(NRF_CRC_SIZE);
    nrf24_set_transfer_size(NRF_TX_SIZE);
    nrf24_enable_auto_acknowledge();
    nrf24_enable_auto_retransmit(1500, 15);

    mac_t tgt01 = {
        .n = "10tgt",
    };
    mac_t tgt02 = {
        .n = "20tgt",
    };

    bool is_config = false;
    char nr        = 0;
    while (!is_config) {
        printf("Enter target number\n");
        printf("  type 1, for 1st target\n");
        printf("  type 2, for 2nd target\n");
        nr        = getchar();
        is_config = true;

        switch (nr) {
            case '1':
                nrf24_set_tx_address(tgt01.a, NRF_ADDR_SIZE);
                nrf24_set_rx_address(tgt01.a, NRF_ADDR_SIZE, NRF24_PIPE_P0);
                nrf24_set_rx_address(tgt02.a, NRF_ADDR_SIZE, NRF24_PIPE_P1);
                break;

            case '2':
                nrf24_set_tx_address(tgt02.a, NRF_ADDR_SIZE);
                nrf24_set_rx_address(tgt02.a, NRF_ADDR_SIZE, NRF24_PIPE_P0);
                nrf24_set_rx_address(tgt01.a, NRF_ADDR_SIZE, NRF24_PIPE_P1);
                break;

            default:
                is_config = false;
                break;
        }
    }

    printf("Config for target nr %c\n", nr);
    // Display the setup of the nRF24L01+ chip
    printf("nRF24L01+ Channel        : %u \n", nrf24_get_rf_channel());
    printf("nRF24L01+ Output power   : %d dBm\n",
           e2power[nrf24_get_rf_output_power()]);
    printf("nRF24L01+ Data Rate      : %u kbps\n",
           e2dr[nrf24_get_air_datarate()]);
    printf("nRF24L01+ Transfert Size : %u bytes\n", nrf24_get_transfer_size());
    printf("nRF24L01+ CRC width      : %u bits\n",
           e2crc[nrf24_get_crc_width()]);
    printf("nRF24L01+ TX Address     : 0x%010llX\n", nrf24_get_tx_address());
    printf("nRF24L01+ RX Address P0  : 0x%010llX\n",
           nrf24_get_rx_address(NRF24_PIPE_P0));
    printf("nRF24L01+ RX Address P1  : 0x%010llX\n",
           nrf24_get_rx_address(NRF24_PIPE_P1));
    thread_create(thread_nrf_tx, 0, "nrf_tx", 0);
    thread_create(thread_nrf_rx, 0, "nrf_rx", 0);

    kernel_launch();
}
