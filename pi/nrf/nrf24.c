/*
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * This module has been adapted from the nRF24L01P.h/.cpp file developped
 * by Owen Edwards, see below, and rewritten in C for the beaglebone black
 * running baremetal software using the HEIA-FR libbbb software.
 */

/*
 * @author Owen Edwards
 *
 * @section LICENSE
 *
 * Copyright (c) 2010 Owen Edwards
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 * nRF24L01+ Single Chip 2.4GHz Transceiver from Nordic Semiconductor.
 *
 * Datasheet:
 *
 * http://www.nordicsemi.no/files/Product/data_sheet/NRF24_Product_Specification_1_0.pdf
 */

/**
 * Includes
 */
#include <am335x_dmtimer1.h>
#include <am335x_gpio.h>
#include <am335x_spi.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include "nrf24.h"

enum nrf24_modes {
    _NRF24_MODE_UNKNOWN,
    _NRF24_MODE_POWER_DOWN,
    _NRF24_MODE_STANDBY,
    _NRF24_MODE_RX,
    _NRF24_MODE_TX,
};

enum nrf24_capes {
    NRF24_CAPE_1,
    NRF24_CAPE_2,
};

#define _NRF24_MAX_RF_CHANNEL 125

/*
 * The following FIFOs are present in nRF24L01+:
 *   TX three level, 32 byte FIFO
 *   RX three level, 32 byte FIFO
 */
#define _NRF24_TX_FIFO_SIZE 32
#define _NRF24_RX_FIFO_SIZE 32

#define _NRF24_SPI_CMD_RD_REG 0x00
#define _NRF24_SPI_CMD_WR_REG 0x20
#define _NRF24_SPI_CMD_RD_RX_PAYLOAD 0x61
#define _NRF24_SPI_CMD_WR_TX_PAYLOAD 0xa0
#define _NRF24_SPI_CMD_FLUSH_TX 0xe1
#define _NRF24_SPI_CMD_FLUSH_RX 0xe2
#define _NRF24_SPI_CMD_REUSE_TX_PL 0xe3
#define _NRF24_SPI_CMD_R_RX_PL_WID 0x60
#define _NRF24_SPI_CMD_W_ACK_PAYLOAD 0xa8
#define _NRF24_SPI_CMD_W_TX_PYLD_NO_ACK 0xb0
#define _NRF24_SPI_CMD_NOP 0xff

#define _NRF24_REG_CONFIG 0x00
#define _NRF24_REG_EN_AA 0x01
#define _NRF24_REG_EN_RXADDR 0x02
#define _NRF24_REG_SETUP_AW 0x03
#define _NRF24_REG_SETUP_RETR 0x04
#define _NRF24_REG_RF_CH 0x05
#define _NRF24_REG_RF_SETUP 0x06
#define _NRF24_REG_STATUS 0x07
#define _NRF24_REG_OBSERVE_TX 0x08
#define _NRF24_REG_RPD 0x09
#define _NRF24_REG_RX_ADDR_P0 0x0a
#define _NRF24_REG_RX_ADDR_P1 0x0b
#define _NRF24_REG_RX_ADDR_P2 0x0c
#define _NRF24_REG_RX_ADDR_P3 0x0d
#define _NRF24_REG_RX_ADDR_P4 0x0e
#define _NRF24_REG_RX_ADDR_P5 0x0f
#define _NRF24_REG_TX_ADDR 0x10
#define _NRF24_REG_RX_PW_P0 0x11
#define _NRF24_REG_RX_PW_P1 0x12
#define _NRF24_REG_RX_PW_P2 0x13
#define _NRF24_REG_RX_PW_P3 0x14
#define _NRF24_REG_RX_PW_P4 0x15
#define _NRF24_REG_RX_PW_P5 0x16
#define _NRF24_REG_FIFO_STATUS 0x17
#define _NRF24_REG_DYNPD 0x1c
#define _NRF24_REG_FEATURE 0x1d

#define _NRF24_REG_ADDRESS_MASK 0x1f
#define _NRF24_MASK_FIFO_EMPTY	0x1

// CONFIG register:
#define _NRF24_CONFIG_PRIM_RX (1 << 0)
#define _NRF24_CONFIG_PWR_UP (1 << 1)
#define _NRF24_CONFIG_CRC0 (1 << 2)
#define _NRF24_CONFIG_EN_CRC (1 << 3)
#define _NRF24_CONFIG_MASK_MAX_RT (1 << 4)
#define _NRF24_CONFIG_MASK_TX_DS (1 << 5)
#define _NRF24_CONFIG_MASK_RX_DR (1 << 6)

#define _NRF24_CONFIG_CRC_MASK (_NRF24_CONFIG_EN_CRC | _NRF24_CONFIG_CRC0)
#define _NRF24_CONFIG_CRC_NONE (0)
#define _NRF24_CONFIG_CRC_8BIT (_NRF24_CONFIG_EN_CRC)
#define _NRF24_CONFIG_CRC_16BIT (_NRF24_CONFIG_EN_CRC | _NRF24_CONFIG_CRC0)

// EN_AA register:
#define _NRF24_EN_AA_NONE 0
#define _NRF24_EN_AA_ALL 0x3f

// EN_RXADDR register:
#define _NRF24_EN_RXADDR_NONE 0

// SETUP_AW register:
#define _NRF24_SETUP_AW_AW_MASK (0x3 << 0)
#define _NRF24_SETUP_AW_AW_3BYTE (0x1 << 0)
#define _NRF24_SETUP_AW_AW_4BYTE (0x2 << 0)
#define _NRF24_SETUP_AW_AW_5BYTE (0x3 << 0)

// SETUP_RETR register:
#define _NRF24_SETUP_RETR_NONE 0
#define _NRF24_SETUP_RETR_DELAY(x) (((((x + 249) / 250) - 1) & 0xf) << 4)
#define _NRF24_SETUP_RETR_COUNT(x) ((x & 0xf) << 0)

// RF_SETUP register:
#define _NRF24_RF_SETUP_RF_PWR_MASK (0x3 << 1)
#define _NRF24_RF_SETUP_RF_PWR_0DBM (0x3 << 1)
#define _NRF24_RF_SETUP_RF_PWR_MINUS_6DBM (0x2 << 1)
#define _NRF24_RF_SETUP_RF_PWR_MINUS_12DBM (0x1 << 1)
#define _NRF24_RF_SETUP_RF_PWR_MINUS_18DBM (0x0 << 1)

#define _NRF24_RF_SETUP_RF_DR_HIGH_BIT (1 << 3)
#define _NRF24_RF_SETUP_RF_DR_LOW_BIT (1 << 5)
#define _NRF24_RF_SETUP_RF_DR_MASK \
    (_NRF24_RF_SETUP_RF_DR_LOW_BIT | _NRF24_RF_SETUP_RF_DR_HIGH_BIT)
#define _NRF24_RF_SETUP_RF_DR_250KBPS (_NRF24_RF_SETUP_RF_DR_LOW_BIT)
#define _NRF24_RF_SETUP_RF_DR_1MBPS (0)
#define _NRF24_RF_SETUP_RF_DR_2MBPS (_NRF24_RF_SETUP_RF_DR_HIGH_BIT)

// STATUS register:
#define _NRF24_STATUS_TX_FULL (1 << 0)
#define _NRF24_STATUS_RX_P_NO (0x7 << 1)
#define _NRF24_STATUS_MAX_RT (1 << 4)
#define _NRF24_STATUS_TX_DS (1 << 5)
#define _NRF24_STATUS_RX_DR (1 << 6)

// RX_PW_P0..RX_PW_P5 registers:
#define _NRF24_RX_PW_Px_MASK 0x3F

#define _NRF24_TIMING_Tundef2pd_us 100000  // 100mS
#define _NRF24_TIMING_Tstby2a_us 130       // 130uS
#define _NRF24_TIMING_Thce_us 1500         //  10uS --> 15 Gac
#define _NRF24_TIMING_Tpd2stby_us 4500     // 4.5mS worst case
#define _NRF24_TIMING_Tpece2csn_us 4       //   4uS

// some BBB specific defines
#define CE_GPIO AM335X_GPIO0
#define CE_PIN 10
#define SPI_CTRL AM335X_SPI1
#define SPI_CHAN AM335X_CHAN0
#define SPI_FREQ 2000000
#define SPI_WORD 8
#define SPI_NOP _NRF24_SPI_CMD_NOP

static struct spi {
    enum am335x_gpio_modules ce_gpio;
    uint32_t ce_pin;
    enum am335x_spi_controllers ctrl;
    enum am335x_spi_channels chan;
} spi[] = {
    [NRF24_CAPE_1] =
        {
            .ce_gpio = AM335X_GPIO0,
            .ce_pin = 10,
            .ctrl = AM335X_SPI1,
            .chan = AM335X_CHAN0,
        },
    [NRF24_CAPE_2] =
        {
            .ce_gpio = AM335X_GPIO0,
            .ce_pin = 11,
            .ctrl = AM335X_SPI1,
            .chan = AM335X_CHAN1,
        },
};

#define wait_us(us) am335x_dmtimer1_wait_us(us)
#define error(x...) printf(x)

// global variables -----------------------------------------------------------

static enum nrf24_modes mode = _NRF24_MODE_UNKNOWN;
static unsigned payload_size = _NRF24_TX_FIFO_SIZE;
static enum nrf24_capes cape = NRF24_CAPE_1;

// local inline methods -------------------------------------------------------

static inline void send_data(const void *val, size_t nb)
{
    am335x_spi_write_b(spi[cape].ctrl, spi[cape].chan, val, nb);
}

static inline void read_data(uint8_t cmd, void *buffer, size_t nb)
{
    uint8_t buf[nb + 1];
    am335x_spi_read_b(spi[cape].ctrl, spi[cape].chan, cmd, SPI_NOP, buf,
                      sizeof(buf));
    memcpy(buffer, buf + 1, nb);
}

static inline unsigned read_value(uint8_t cmd)
{
    uint8_t buf[2];
    am335x_spi_read_b(spi[cape].ctrl, spi[cape].chan, cmd, SPI_NOP, buf,
                      sizeof(buf));
    return buf[1];
}

static inline unsigned read_status(uint8_t cmd)
{
    uint8_t buf[1];
    am335x_spi_read_b(spi[cape].ctrl, spi[cape].chan, cmd, SPI_NOP, buf,
                      sizeof(buf));
    return buf[0];
}

static inline void enable(bool state)
{
    am335x_gpio_change_state(spi[cape].ce_gpio, spi[cape].ce_pin, state);
}

static inline bool disable(void)
{
    bool state = am335x_gpio_get_state(spi[cape].ce_gpio, spi[cape].ce_pin);
    am335x_gpio_change_state(spi[cape].ce_gpio, spi[cape].ce_pin, false);
    return state;
}

static inline void set_register(unsigned reg, unsigned value)
{
    uint8_t cmd[] = {
        _NRF24_SPI_CMD_WR_REG | (reg & _NRF24_REG_ADDRESS_MASK),
        value,
    };
    send_data(cmd, sizeof(cmd));
}

static inline unsigned get_register(unsigned reg)
{
    unsigned cn = (_NRF24_SPI_CMD_RD_REG | (reg & _NRF24_REG_ADDRESS_MASK));
    unsigned dn = read_value(cn);
    return dn;
}

static void set_transfer_size(unsigned size, enum nrf24_pipe pipe)
{
    int rxPwPxRegister = _NRF24_REG_RX_PW_P0 + (pipe - NRF24_PIPE_P0);
    set_register(rxPwPxRegister, (size & _NRF24_RX_PW_Px_MASK));
}

// global methods -------------------------------------------------------------

unsigned nrf24_get_status_register(void)
{
    unsigned status = read_status(_NRF24_SPI_CMD_NOP);
    return status;
}

unsigned nrf24_get_rpd(void)
{
    unsigned cn = (_NRF24_SPI_CMD_RD_REG | _NRF24_REG_RPD);
    unsigned status = read_value(cn);
    return status;
}

unsigned nrf24_get_config(void)
{
    unsigned cn = (_NRF24_SPI_CMD_RD_REG | _NRF24_REG_CONFIG);
    unsigned status = read_value(cn);
    return status;
}

void nrf24_select_cape_2(void) { cape = NRF24_CAPE_2; }

void nrf24_init()
{
    mode = _NRF24_MODE_UNKNOWN;

    // init GPIO and SPI interfaces
    am335x_gpio_init(spi[cape].ce_gpio);
    am335x_gpio_setup_pin_out(spi[cape].ce_gpio, spi[cape].ce_pin, false);
    am335x_spi_init(spi[cape].ctrl, spi[cape].chan, SPI_FREQ, SPI_WORD);

    // disable carrier
    disable();
    wait_us(100000);

    // Wait for Power-on reset put device in power down mode
    wait_us(_NRF24_TIMING_Tundef2pd_us);
    set_register(_NRF24_REG_CONFIG, 0);
    wait_us(_NRF24_TIMING_Tpd2stby_us);

    // Setup default configuration and clear pending interrupts
    nrf24_disable_all_rx_pipes();
    uint8_t cmd_flush_tx[] = {_NRF24_SPI_CMD_FLUSH_TX};
    send_data(cmd_flush_tx, 1);
    uint8_t cmd_flush_rx[] = {_NRF24_SPI_CMD_FLUSH_RX};
    send_data(cmd_flush_rx, 1);
    set_register(_NRF24_REG_STATUS, _NRF24_STATUS_MAX_RT | _NRF24_STATUS_TX_DS |
                                        _NRF24_STATUS_RX_DR);

    mode = _NRF24_MODE_POWER_DOWN;
}

void nrf24_power_up(void)
{
    int config = get_register(_NRF24_REG_CONFIG);
    config |= _NRF24_CONFIG_PWR_UP;
    set_register(_NRF24_REG_CONFIG, config);

    // Wait until the nRF24L01+ powers up
    wait_us(_NRF24_TIMING_Tpd2stby_us);
    mode = _NRF24_MODE_STANDBY;
}

void nrf24_power_down(void)
{
    int config = get_register(_NRF24_REG_CONFIG);
    config &= ~_NRF24_CONFIG_PWR_UP;
    set_register(_NRF24_REG_CONFIG, config);

    // Wait until the nRF24L01+ powers down
    // This *may* not be necessary (no timing is shown in the Datasheet),
    // but just to be safe
    wait_us(_NRF24_TIMING_Tpd2stby_us);

    mode = _NRF24_MODE_POWER_DOWN;
}

void nrf24_set_receive_mode(void)
{
    if (_NRF24_MODE_POWER_DOWN == mode) nrf24_power_up();

    int config = get_register(_NRF24_REG_CONFIG);
    config |= _NRF24_CONFIG_PRIM_RX;
    set_register(_NRF24_REG_CONFIG, config);

    mode = _NRF24_MODE_RX;
    enable(true);
}

void nrf24_set_transmit_mode(void)
{
    if (_NRF24_MODE_POWER_DOWN == mode) nrf24_power_up();

    unsigned config = get_register(_NRF24_REG_CONFIG);
    config &= ~_NRF24_CONFIG_PRIM_RX;
    set_register(_NRF24_REG_CONFIG, config);

    mode = _NRF24_MODE_TX;
    disable();
}

void nrf24_set_rf_channel(unsigned channel)
{
    if (channel > (_NRF24_MAX_RF_CHANNEL)) {
        error("nRF24L01P: Invalid RF Channel setting %d\r\n", channel);
        return;
    }
    set_register(_NRF24_REG_RF_CH, channel);
}

unsigned nrf24_get_rf_channel(void)
{
    int channel = get_register(_NRF24_REG_RF_CH) & 0x7F;
    return channel;
}

void nrf24_set_rf_output_power(enum nrf24_output_power power)
{
    int rfSetup =
        get_register(_NRF24_REG_RF_SETUP) & ~_NRF24_RF_SETUP_RF_PWR_MASK;

    switch (power) {
        case NRF24_OUTPUT_POWER_ZERO_DB:
            rfSetup |= _NRF24_RF_SETUP_RF_PWR_0DBM;
            break;

        case NRF24_OUTPUT_POWER_MINUS_6_DB:
            rfSetup |= _NRF24_RF_SETUP_RF_PWR_MINUS_6DBM;
            break;

        case NRF24_OUTPUT_POWER_MINUS_12_DB:
            rfSetup |= _NRF24_RF_SETUP_RF_PWR_MINUS_12DBM;
            break;

        case NRF24_OUTPUT_POWER_MINUS_18_DB:
            rfSetup |= _NRF24_RF_SETUP_RF_PWR_MINUS_18DBM;
            break;

        default:
            error("nRF24L01P: Invalid RF Output Power setting %d\r\n", power);
            return;
    }

    set_register(_NRF24_REG_RF_SETUP, rfSetup);
}

enum nrf24_output_power nrf24_get_rf_output_power(void)
{
    int rfPwr = get_register(_NRF24_REG_RF_SETUP) & _NRF24_RF_SETUP_RF_PWR_MASK;

    switch (rfPwr) {
        case _NRF24_RF_SETUP_RF_PWR_0DBM:
            return NRF24_OUTPUT_POWER_ZERO_DB;

        case _NRF24_RF_SETUP_RF_PWR_MINUS_6DBM:
            return NRF24_OUTPUT_POWER_MINUS_6_DB;

        case _NRF24_RF_SETUP_RF_PWR_MINUS_12DBM:
            return NRF24_OUTPUT_POWER_MINUS_12_DB;

        case _NRF24_RF_SETUP_RF_PWR_MINUS_18DBM:
            return NRF24_OUTPUT_POWER_MINUS_18_DB;

        default:
            error("nRF24L01P: Unknown RF Output Power value %d\r\n", rfPwr);
            return 0;
    }
}

void nrf24_set_air_datarate(enum nrf24_datarate rate)
{
    int rfSetup =
        get_register(_NRF24_REG_RF_SETUP) & ~_NRF24_RF_SETUP_RF_DR_MASK;

    switch (rate) {
        case NRF24_DATARATE_250_KBPS:
            rfSetup |= _NRF24_RF_SETUP_RF_DR_250KBPS;
            break;

        case NRF24_DATARATE_1_MBPS:
            rfSetup |= _NRF24_RF_SETUP_RF_DR_1MBPS;
            break;

        case NRF24_DATARATE_2_MBPS:
            rfSetup |= _NRF24_RF_SETUP_RF_DR_2MBPS;
            break;

        default:
            error("nRF24L01P: Invalid Air Data Rate setting %d\r\n", rate);
            return;
    }

    set_register(_NRF24_REG_RF_SETUP, rfSetup);
}

enum nrf24_datarate nrf24_get_air_datarate(void)
{
    int rfDataRate =
        get_register(_NRF24_REG_RF_SETUP) & _NRF24_RF_SETUP_RF_DR_MASK;

    switch (rfDataRate) {
        case _NRF24_RF_SETUP_RF_DR_250KBPS:
            return NRF24_DATARATE_250_KBPS;

        case _NRF24_RF_SETUP_RF_DR_1MBPS:
            return NRF24_DATARATE_1_MBPS;

        case _NRF24_RF_SETUP_RF_DR_2MBPS:
            return NRF24_DATARATE_2_MBPS;

        default:
            error("nRF24L01P: Unknown Air Data Rate value %d\r\n", rfDataRate);
            return 0;
    }
}

void nrf24_set_crc_width(enum nrf24_crc_width width)
{
    int config = get_register(_NRF24_REG_CONFIG) & ~_NRF24_CONFIG_CRC_MASK;

    switch (width) {
        case NRF24_CRC_NONE:
            config |= _NRF24_CONFIG_CRC_NONE;
            break;

        case NRF24_CRC_8_BIT:
            config |= _NRF24_CONFIG_CRC_8BIT;
            break;

        case NRF24_CRC_16_BIT:
            config |= _NRF24_CONFIG_CRC_16BIT;
            break;

        default:
            error("nRF24L01P: Invalid CRC Width setting %d\r\n", width);
            return;
    }

    set_register(_NRF24_REG_CONFIG, config);
}

enum nrf24_crc_width nrf24_get_crc_width(void)
{
    int crcWidth = get_register(_NRF24_REG_CONFIG) & _NRF24_CONFIG_CRC_MASK;

    switch (crcWidth) {
        case _NRF24_CONFIG_CRC_NONE:
            return NRF24_CRC_NONE;

        case _NRF24_CONFIG_CRC_8BIT:
            return NRF24_CRC_8_BIT;

        case _NRF24_CONFIG_CRC_16BIT:
            return NRF24_CRC_16_BIT;

        default:
            error("nRF24L01P: Unknown CRC Width value %d\r\n", crcWidth);
            return 0;
    }
}

void nrf24_set_transfer_size(unsigned size)
{
    if (size > _NRF24_RX_FIFO_SIZE) {
        error("nRF24L01P: Invalid Transfer Size setting %d\r\n", size);
        return;
    }

    payload_size = size;
}

unsigned nrf24_get_transfer_size() { return payload_size; }

void nrf24_disable_all_rx_pipes(void)
{
    set_register(_NRF24_REG_EN_RXADDR, _NRF24_EN_RXADDR_NONE);
    for (int pipe = NRF24_PIPE_P0; pipe <= NRF24_PIPE_P5; pipe++) {
        set_transfer_size(0, pipe);
    }
}


void nrf24_disable_auto_acknowledge(void)
{
    set_register(_NRF24_REG_EN_AA, _NRF24_EN_AA_NONE);
}

void nrf24_enable_auto_acknowledge(void)
{
    set_register(_NRF24_REG_EN_AA, _NRF24_EN_AA_ALL);
}

void nrf24_disable_auto_retransmit(void)
{
    set_register(_NRF24_REG_SETUP_RETR, _NRF24_SETUP_RETR_NONE);
}

void nrf24_enable_auto_retransmit(unsigned delay, unsigned count)
{
    set_register(_NRF24_REG_SETUP_RETR, _NRF24_SETUP_RETR_DELAY(delay) +
                                            _NRF24_SETUP_RETR_COUNT(count));
}

void nrf24_set_rx_address(uint64_t address, enum nrf24_address_size a_width,
                          enum nrf24_pipe pipe)
{
    if (pipe > NRF24_PIPE_P5) {
        error("nRF24L01P: Invalid setRxAddress pipe number %d\r\n", pipe);
        return;
    }

    unsigned adr_sz = 1;
    if ((pipe == NRF24_PIPE_P0) || (pipe == NRF24_PIPE_P1)) {
        unsigned setupAw =
            get_register(_NRF24_REG_SETUP_AW) & ~_NRF24_SETUP_AW_AW_MASK;
        switch (a_width) {
            case NRF24_ADDRESS_3_BYTES:
                setupAw |= _NRF24_SETUP_AW_AW_3BYTE;
                adr_sz = 3;
                break;

            case NRF24_ADDRESS_4_BYTES:
                setupAw |= _NRF24_SETUP_AW_AW_4BYTE;
                adr_sz = 4;
                break;

            case NRF24_ADDRESS_5_BYTES:
                setupAw |= _NRF24_SETUP_AW_AW_5BYTE;
                adr_sz = 5;
                break;

            default:
                error("nRF24L01P: Invalid setRxAddress width setting %d\r\n",
                      a_width);
                return;
        }
        set_register(_NRF24_REG_SETUP_AW, setupAw);
    }

    unsigned rxAddrPxRegister = _NRF24_REG_RX_ADDR_P0 + (pipe - NRF24_PIPE_P0);
    unsigned cn =
        (_NRF24_SPI_CMD_WR_REG | (rxAddrPxRegister & _NRF24_REG_ADDRESS_MASK));
    uint8_t cmd[] = {
        cn,
        (address >> 0) & 0xff,
        (address >> 8) & 0xff,
        (address >> 16) & 0xff,
        (address >> 24) & 0xff,
        (address >> 32) & 0xff,
        (address >> 40) & 0xff,
        (address >> 48) & 0xff,
        (address >> 56) & 0xff,
    };
    send_data(cmd, adr_sz + 1);

    unsigned enRxAddr = get_register(_NRF24_REG_EN_RXADDR);
    enRxAddr |= (1 << (pipe - NRF24_PIPE_P0));
    set_register(_NRF24_REG_EN_RXADDR, enRxAddr);
    set_transfer_size(payload_size, pipe);
}

void nrf24_set_tx_address(uint64_t address, enum nrf24_address_size a_width)
{
    int setupAw = get_register(_NRF24_REG_SETUP_AW) & ~_NRF24_SETUP_AW_AW_MASK;
    unsigned adr_sz = 1;
    switch (a_width) {
        case NRF24_ADDRESS_3_BYTES:
            setupAw |= _NRF24_SETUP_AW_AW_3BYTE;
            adr_sz = 3;
            break;

        case NRF24_ADDRESS_4_BYTES:
            setupAw |= _NRF24_SETUP_AW_AW_4BYTE;
            adr_sz = 4;
            break;

        case NRF24_ADDRESS_5_BYTES:
            setupAw |= _NRF24_SETUP_AW_AW_5BYTE;
            adr_sz = 5;
            break;

        default:
            error("nRF24L01P: Invalid setTxAddress width setting %d\r\n",
                  a_width);
            return;
    }

    set_register(_NRF24_REG_SETUP_AW, setupAw);
    int cn = (_NRF24_SPI_CMD_WR_REG |
              (_NRF24_REG_TX_ADDR & _NRF24_REG_ADDRESS_MASK));
    uint8_t cmd[] = {
        cn,
        (address >> 0) & 0xff,
        (address >> 8) & 0xff,
        (address >> 16) & 0xff,
        (address >> 24) & 0xff,
        (address >> 32) & 0xff,
        (address >> 40) & 0xff,
        (address >> 48) & 0xff,
        (address >> 56) & 0xff,
    };
    send_data(cmd, adr_sz + 1);
}

uint64_t nrf24_get_rx_address(enum nrf24_pipe pipe)
{
    if (pipe > NRF24_PIPE_P5) {
        error("nRF24L01P: Invalid setRxAddress pipe number %d\r\n", pipe);
        return 0;
    }

    int width = 1;
    if ((pipe == NRF24_PIPE_P0) || (pipe == NRF24_PIPE_P1)) {
        int setupAw =
            get_register(_NRF24_REG_SETUP_AW) & _NRF24_SETUP_AW_AW_MASK;

        switch (setupAw) {
            case _NRF24_SETUP_AW_AW_3BYTE:
                width = 3;
                break;

            case _NRF24_SETUP_AW_AW_4BYTE:
                width = 4;
                break;

            case _NRF24_SETUP_AW_AW_5BYTE:
                width = 5;
                break;

            default:
                error("nRF24L01P: Unknown getRxAddress width value %d\r\n",
                      setupAw);
                return 0;
        }
    }

    int rxAddrPxRegister = _NRF24_REG_RX_ADDR_P0 + (pipe - NRF24_PIPE_P0);
    int cn =
        (_NRF24_SPI_CMD_RD_REG | (rxAddrPxRegister & _NRF24_REG_ADDRESS_MASK));
    uint64_t address = 0;
    read_data(cn, &address, width);

    if (!((pipe == NRF24_PIPE_P0) || (pipe == NRF24_PIPE_P1))) {
        address |= (nrf24_get_rx_address(NRF24_PIPE_P1) & ~(0xffull));
    }

    return address;
}

uint64_t nrf24_get_tx_address(void)
{
    int setupAw = get_register(_NRF24_REG_SETUP_AW) & _NRF24_SETUP_AW_AW_MASK;
    int width = 1;

    switch (setupAw) {
        case _NRF24_SETUP_AW_AW_3BYTE:
            width = 3;
            break;

        case _NRF24_SETUP_AW_AW_4BYTE:
            width = 4;
            break;

        case _NRF24_SETUP_AW_AW_5BYTE:
            width = 5;
            break;

        default:
            error("nRF24L01P: Unknown getTxAddress width value %d\r\n",
                  setupAw);
            return 0;
    }

    int cn = (_NRF24_SPI_CMD_RD_REG |
              (_NRF24_REG_TX_ADDR & _NRF24_REG_ADDRESS_MASK));
    uint64_t address = 0;
    read_data(cn, &address, width);

    return address;
}

bool nrf24_readable(enum nrf24_pipe pipe)
{
    if (pipe > NRF24_PIPE_P5) {
        error("nRF24L01P: Invalid readable pipe number %d\r\n", pipe);
        return false;
    }

    unsigned status = nrf24_get_status_register();
    bool ok = ((status & _NRF24_STATUS_RX_P_NO) >> 1) == (pipe & 0x7);
    if (!ok) {
//    	printf ("status=%02x\n", status);
    }
    return ok;
}


int nrf24_write(const void *data, size_t count)
{
    int originalCe = disable();

    if (count > _NRF24_TX_FIFO_SIZE) count = _NRF24_TX_FIFO_SIZE;

    // Clear the Status bit
    set_register(_NRF24_REG_STATUS, _NRF24_STATUS_TX_DS);

    uint8_t cmd[count + 1];
    cmd[0] = _NRF24_SPI_CMD_WR_TX_PAYLOAD;
    memcpy(&cmd[1], data, count);
    send_data(cmd, count + 1);

    int originalMode = mode;
    nrf24_set_transmit_mode();

    enable(true);
    wait_us(_NRF24_TIMING_Thce_us);
    disable();

    while (1) {
        // Wait for the transfer to complete
        unsigned status = nrf24_get_status_register();
        if ((status & _NRF24_STATUS_TX_DS) != 0) break;
        if ((status & _NRF24_STATUS_MAX_RT) != 0) {
            count = -1;
            break;
        }
    }

    // Clear the Status bit
    set_register(_NRF24_REG_STATUS, _NRF24_STATUS_TX_DS | _NRF24_STATUS_MAX_RT);

    if (originalMode == _NRF24_MODE_RX) {
        nrf24_set_receive_mode();
    }

    enable(originalCe);
    wait_us(_NRF24_TIMING_Tpece2csn_us);

    return count;
}

int nrf24_read(enum nrf24_pipe pipe, void *data, size_t count)
{
    if (!nrf24_readable(pipe)) {
        return 0;
    }
    unsigned rxPayloadWidth = read_value(_NRF24_SPI_CMD_R_RX_PL_WID);
    if (rxPayloadWidth > _NRF24_RX_FIFO_SIZE) {
        read_value(_NRF24_SPI_CMD_FLUSH_RX);
        error("Gac: error while reading data...\n");
        //
        // At this point, we should retry the reception,
        //  but for now we'll just fall through...
        //
        return -1;
    }

    if (count > _NRF24_RX_FIFO_SIZE) count = _NRF24_RX_FIFO_SIZE;
    if (count > rxPayloadWidth) count = rxPayloadWidth;
    read_data(_NRF24_SPI_CMD_RD_RX_PAYLOAD, data, count);

    // Clear the Status bit
    set_register(_NRF24_REG_STATUS, _NRF24_STATUS_RX_DR);
    return count;
}
