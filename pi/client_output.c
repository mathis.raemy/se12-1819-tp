/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract: Switches Device Driver
 *
 * Purpose:	This module listen the msgQ and active the output of the client
 *
 * Modified:		Mathis Raemy
 * Modification: 	-
 * Date: 			22.05.2019
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include "client_output.h"
#include "timer.h"
#include "leds.h"
#include "thread.h"
#include "msgq.h"
#include "seg7.h"
#define CMD_WIN_POINT		5				//valeur indiquant a un client qu'il a gagne un point
#define CMD_WIN_GAME		6			//valeur indiquant a un client qu'il a gagne la partie

static bool seg7_inversion = false;
static int targetID = 0;
static bool led_inversion = false;
static bool isSequenceInProgress = false;

static void timer_change_led_state(enum intc_vectors timer, void* param){
	(void)param;
	(void)timer;
	if (led_inversion)leds_set_state(leds_get_state() | (LEDS_1 | LEDS_2 |LEDS_3));
	else leds_set_state(leds_get_state() & (~(LEDS_1 | LEDS_2 |LEDS_3)));
	led_inversion^=true;

}
static void  timer_stop_clignotte(enum intc_vectors timer, void* param){
	(void)param;
	(void)timer;
	timer_set_period(DMTimer_3, 0);
	timer_on_event(DMTimer_3,0,0);
	timer_set_period(DMTimer_5, 0);
	timer_on_event(DMTimer_5,0,0);
	leds_set_state(0);
	isSequenceInProgress = false;
}

static void win_sequence_start(enum intc_vectors timer, void* param){
	(void)param;
	(void)timer;
	if(!seg7_inversion){
		seg7_display_number(66,0, true, true);
		printf("Turning on\n");
	}
	else{
		printf("Turning off\n");
		seg7_reset();
	}
	seg7_inversion = seg7_inversion ^ true;
}
static void win_sequence_stop(enum intc_vectors timer, void* param){
	(void)param;
	(void)timer;
	timer_set_period(DMTimer_3, 0);
	timer_on_event(DMTimer_3,0,0);
	timer_set_period(DMTimer_5, 0);
	timer_on_event(DMTimer_5,0,0);
	seg7_reset();
	isSequenceInProgress = false;

}
static void win_game(){
	isSequenceInProgress = true;
	timer_set_period(DMTimer_3, 4000000);
	timer_set_period(DMTimer_5, 400000);
	timer_on_event(DMTimer_3,win_sequence_stop,0);
	timer_on_event(DMTimer_5,win_sequence_start,0);
	seg7_inversion =false;
}
static void win_point(){
	isSequenceInProgress = true;
	timer_set_period(DMTimer_3, 2000000);
	timer_set_period(DMTimer_5, 200000);
	timer_on_event(DMTimer_3,timer_stop_clignotte,0);
	timer_on_event(DMTimer_5,timer_change_led_state,0);

}

void client_output_init(int ID){
	targetID = ID;
	timer_init(DMTimer_3);
	timer_init(DMTimer_4);
	timer_init(DMTimer_5);
	leds_init();
	seg7_init(0);
	seg7_init(1);
}
void client_output_thread(void* p){
	struct msgQstr* msgQID = (struct msgQstr*) p;
	while(1){
		if(!isSequenceInProgress) {
			int cmd = (int)msgq_fetch(msgQID->msgQArray[0]);
			if(targetID == 2 && cmd > 10){
				cmd -=10;
				if (cmd == CMD_WIN_POINT)win_point();
				if (cmd == CMD_WIN_GAME)win_game();
			}else if(targetID == 1 && cmd < 10){
				if (cmd == CMD_WIN_POINT)win_point();
				if (cmd == CMD_WIN_GAME)win_game();
		}
		}
		thread_yield();
	}
}
