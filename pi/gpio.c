/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 pi
 *
 * Abstract: Module that manage the Interruption of the GPIO
 *
 * Purpose:	PI
 *
 * Author: 	Mathis Raemy et Tobias Moullet
 * Date: 	27.03.2019
 */
#include <stdint.h>
#include <am335x_gpio.h>
#include "intc.h"
#include "gpio.h"


static struct listener{
	gpio_service_routine_t routine;
	void* param;
}listeners[4][32];

static void gpio_handler(enum intc_vectors i_vector_nr, void* param)
{
	(void)i_vector_nr; // unused param
	enum am335x_gpio_modules module = (enum am335x_gpio_modules)param;
	// get pin number having raised interrupt and acknowledge it
	int pin = am335x_gpio_vector(module);
	if (pin < 0) return;
	struct listener* listener = &listeners[module][pin];
	if (listener->routine != 0) {
		listener->routine (listener->param);
	} else {
		// spurious interrupt
		am335x_gpio_disable(module, pin);
	}
}

void gpio_init_fct(){
	am335x_gpio_init(AM335X_GPIO0);
	am335x_gpio_init(AM335X_GPIO1);
	am335x_gpio_init(AM335X_GPIO2);
	am335x_gpio_init(AM335X_GPIO3);
	intc_on_event(INTC_GPIO0A, gpio_handler, (void*)AM335X_GPIO0);
	intc_on_event(INTC_GPIO1A, gpio_handler, (void*)AM335X_GPIO1);
	intc_on_event(INTC_GPIO2A, gpio_handler, (void*)AM335X_GPIO2);
	intc_on_event(INTC_GPIO3A, gpio_handler, (void*)AM335X_GPIO3);
}
int gpio_on_event(enum am335x_gpio_modules module, uint32_t pin, unsigned modes,
		gpio_service_routine_t routine, void* param) {

	if (pin > 31) return -1;
	struct listener* listener = &listeners[module][pin];
	if (routine == 0) {
		am335x_gpio_disable(module, pin);
		listener->routine = 0;
		listener->param = 0;
		return 0;
	}

	if (listener->routine == 0) {
			listener->routine = routine;
			listener->param = param;
			unsigned both = GPIO_RISING | GPIO_FALLING;
			enum am335x_gpio_interrupt_modes mode = AM335X_GPIO_IRQ_FALLING;
			if (GPIO_HIGH & modes){
				mode = AM335X_GPIO_IRQ_HIGH;
			}
			if (GPIO_LOW & modes){
				mode = AM335X_GPIO_IRQ_LOW;
			}
			if (GPIO_RISING & modes){
				mode = AM335X_GPIO_IRQ_RISING;
			}
			if ((both & modes) == both){
				mode = AM335X_GPIO_IRQ_BOTH;
			}
			am335x_gpio_setup_pin_irq(module, pin, mode, modes & GPIO_DEBOUNCED, AM335X_GPIO_PULL_NONE);
			am335x_gpio_enable(module, pin);
		}


	return -1;
}
