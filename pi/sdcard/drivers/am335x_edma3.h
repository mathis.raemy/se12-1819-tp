#pragma once
#ifndef AM335X_EDMA3_H
#define AM335X_EDMA3_H
/**
 * Copyright 2017 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: HEIA-FR / Embedded Systems Laboratory
 *
 * Abstract: AM335x EDMA3 Driver
 *
 * Purpose: This module implements basic services to drive the AM335x
 *          EDMA3 module.
 *
 * Author:  Romain Plouzin / Daniel Gachet
 * Date:    03.01.2017
 */

#include "stdint.h"

enum am335x_edma3_trigger_mode {
    AM335X_EDMA3_TRIG_MODE_MANUAL,
    AM335X_EDMA3_TRIG_MODE_QDMA,
    AM335X_EDMA3_TRIG_MODE_EVENT,
};

enum am335x_edma3_channel_type {
    AM335X_EDMA3_CHANNEL_TYPE_DMA,
    AM335X_EDMA3_CHANNEL_TYPE_QDMA,
};

// Channel Options Parameters (OPT) Field Descriptions
#define AM335X_EDMA3_OPT_ITCCHEN (1 << 23)
#define AM335X_EDMA3_OPT_TCCHEN (1 << 22)
#define AM335X_EDMA3_OPT_ITCENTEN (1 << 21)
#define AM335X_EDMA3_OPT_TCINTEN (1 << 20)
#define AM335X_EDMA3_OPT_TCC_SHIFT (12)
#define AM335X_EDMA3_OPT_TCC_MASK (63 << 12)
#define AM335X_EDMA3_OPT_TCCMOD (1 << 11)
#define AM335X_EDMA3_OPT_FWID_SHIFT (8)
#define AM335X_EDMA3_OPT_STATIC (1 << 3)
#define AM335X_EDMA3_OPT_SYNCDIM (1 << 2)
#define AM335X_EDMA3_OPT_SYNCDIM_A (0 << 2)
#define AM335X_EDMA3_OPT_SYNCDIM_AB (1 << 2)
#define AM335X_EDMA3_OPT_DAM (1 << 1)
#define AM335X_EDMA3_OPT_SAM (1 << 0)

/**
 * EDMA3 Parameter RAM Set in User Configurable format
 *
 * This is a mapping of the EDMA3 PaRAM set provided to the user
 * for ease of modification of the individual fields
 */
struct am335x_edma3_param {
    // option field set
    uint32_t opt;

    // starting byte address of source
    // for FIFO mode, srcAddr must be a 256-bit aligned address.
    volatile void* src;

    // number of bytes in each array
    uint16_t acnt;

    // number of arrays in each frame
    uint16_t bcnt;

    // starting byte address of destination
    // for FIFO mode, destAddr must be a 256-bit aligned address.
    volatile void* dst;

    // index between consec. arrays of a source frame
    int16_t srcbidx;

    // index between consec. arrays of a destination frame
    int16_t dstbidx;

    // address for linking (AutoReloading of a PaRAM Set)
    // this must point to a valid aligned 32-byte PaRAM set
    // a value of 0xFFFF means no linking
    uint16_t link;

    // reload value of the numArrInFrame
    // relevant only for A-sync transfers
    uint16_t bcntrld;

    // index between consecutive frames of a source block
    int16_t srccidx;

    // index between consecutive frames of a dest block
    int16_t dstcidx;

    // number of Frames in a block
    uint16_t ccnt;

    // this field is reserved. Write zero to this field.
    uint16_t rsvd;
};

/**
 * method to initialize the EDMA3 Driver
 * clears the error specific registers & initializes the Queue Number Registers
 *
 * @param queue event queue number to which the channel will be mapped
 *              (valid only for the Master Channel (DMA/QDMA) request)
 */
extern void am335x_edma3_init(uint32_t queue);

/**
 * method to initialize a DMA/QDMA/Link channel
 *
 * Each channel (DMA/QDMA/Link) must be initialized  before initiating a DMA
 * transfer on that channel.
 *
 * This API is used to allocate a logical channel (DMA/QDMA/Link) along with
 * the associated resources. For DMA and QDMA channels, TCC and PaRAM Set are
 * also allocated along with the requested channel.
 *
 * User can request a specific logical channel by passing the channel number
 * in 'channel'.
 *
 * For DMA/QDMA channels, after allocating all the EDMA3 resources, this API
 * sets the TCC field of the OPT PaRAM Word with the allocated TCC. It also
 * sets the event queue for the channel allocated. The event queue needs to be
 * specified by the user.
 *
 * For DMA channel, it also sets the DCHMAP register.
 *
 * For QDMA channel, it sets the QCHMAP register and CCNT as trigger word and
 * enables the QDMA channel by writing to the QEESR register.
 *
 * @param type      channel type (DMA/QDMA)
 * @param channel   channel number requested for a particular event and on which
 *                  the completion/error interrupt is generated
 *                  Not used if user requested for a Link channel.
 * @param queue     event queue number to which the channel will be mapped
 *                  (valid only for the Master Channel (DMA/QDMA) request).
 *
 * @return          status information (0 == success, -1 == error)
 */
extern int am335x_edma3_channel_init(enum am335x_edma3_channel_type type,
                                     uint32_t channel,
                                     uint32_t queue,
                                     void (*callback)(void* param),
                                     void* param);

/**
 * method to copy the user specified PaRAM Set onto the PaRAM Set associated
 * with the logical channel (DMA/Link).
 *
 * This API takes a PaRAM Set as input and copies it onto the actual PaRAM Set
 * associated with the logical channel. OPT field of the PaRAM Set is written
 * first and the CCNT field is written last.
 *
 * @param channel   logical channel whose PaRAM set is requested
 * @param param     parameter RAM set to be copied onto existing PaRAM
 */
extern void am335x_edma3_set_param(uint32_t channel,
                                   struct am335x_edma3_param* param);

/**
 * method to start EDMA transfer on the specified channel
 *
 * There are multiple ways to trigger an EDMA3 transfer. The triggering mode
 * option allows choosing from the available triggering modes: Event,
 * Manual or QDMA.
 *
 * In event triggered, a peripheral or an externally generated event triggers
 * the transfer. This API clears the Event and Event Miss Register and then
 * enables the DMA channel by writing to the EESR.
 *
 * In manual triggered mode, CPU manually triggers a transfer by writing a 1
 * in the Event Set Register ESR. This API writes to the ESR to start the
 * transfer.
 *
 * In QDMA triggered mode, a QDMA transfer is triggered when a CPU (or other
 * EDMA3 programmer) writes to the trigger word of the QDMA channel PaRAM set
 * (auto-triggered) or when the EDMA3CC performs a link update on a PaRAM set
 * that has been mapped to a QDMA channel (link triggered). This API enables
 * the QDMA channel by writing to the QEESR register.
 *
 * @param channel   channel being used to enable transfer
 * @param mode      mode of triggering start of transfer (Manual, QDMA or Event)
 *
 * @return          status information (0 == success, -1 == error)
 *
 */
extern int am335x_edma3_enable_transfer(uint32_t channel,
                                        enum am335x_edma3_trigger_mode mode);

/**
 * interrupt service routine to treat the transfer completion events
 *
 * this method should be connect the interrupt controller at the
 * EDMA Completion Interrupt vector
 */
extern void am335x_edma3_completion_isr();

/**
 * interrupt service routine to treat the transfer error events
 *
 * this method should be connect the interrupt controller at the
 * EDMA Error Interrupt vector
 */
extern void am335x_edma3_error_isr();

#endif
