/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:		header of timer.c
 *
 * Author:	Tobias Moullet & Mathis Raemy
 * Date: 	27.03.19
 */
#pragma once
#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>
#include "intc.h"



/**
 * enum of the different counter
 */
enum dmtimer_set{
	DMTimer_2,
	DMTimer_3,
	DMTimer_4,
	DMTimer_5,
	DMTimer_6,
	DMTimer_7,
	TIMER_COUNT
};

typedef void (*dmtimer_service_routine_t) (enum intc_vectors vector_nr,
      void* param);

/**
 * this method initialize the clock counter that is given as argument
 */
extern void timer_init(enum dmtimer_set timer);

/**
 * this methode return the actual value of the counter that is given as argument
 */
extern uint32_t timer_get_counter(enum dmtimer_set timer);

/**
 * this methode return the frequency of the clock
 */
extern uint32_t timer_get_frequency();

/**
 * this methode reset the counter value
 */
extern void timer_reset_counter(enum dmtimer_set timer);

/*
 * used to attach a methode to a timer interruption
 */
extern int timer_on_event(enum dmtimer_set timer,
						  dmtimer_service_routine_t routine,
                          void* param);

/*
 * used to set the interruption interval
 */
extern void timer_set_period(enum dmtimer_set timer, uint32_t period_ms);


#endif
