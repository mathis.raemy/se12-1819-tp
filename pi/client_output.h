/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract: Switches Device Driver
 *
 * Purpose:	Header of client_output.c
 *
 * Modified:		Mathis Raemy
 * Modification: 	-
 * Date: 			22.05.2019
 */

#ifndef CLIENT_OUTPUT_H_
#define CLIENT_OUTPUT_H_

//initialisation of the client_output module
extern void client_output_init(int ID);

//thread to read the messageQ and active the output
extern void client_output_thread(void* p);

#endif /* CLIENT_OUTPUT_H_ */
