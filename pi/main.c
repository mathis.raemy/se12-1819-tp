/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 pi
 *
 * Abstract: Main of a TP to test the GPIO interruption
 *
 * Purpose:	PI
 *
 * Author: 	Raemy Mathis et Tobias Moullet
 * Date: 	27.03.2019
 */


#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <am335x_gpio.h>
#include "buttons.h"
#include "interrupt.h"
#include "exception.h"
#include "gpio.h"
#include "intc.h"
#include "timer.h"
#include "leds.h"
#include "com_controller.h"
#include "kernel.h"
#include "thread.h"
#include "msgq.h"
#include "client_output.h"


#define SW_GPIO   AM335X_GPIO1
#define S1_PIN	 	15
#define S2_PIN	 	16
#define S3_PIN	 	17
#define MSGQ_SIZE	50
#define TARGET_ID	2
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]));
#define CMD_CONNECT_PRESS	1				//valeur indiquant que le bouton de connexion S2 a été pressé
#define CMD_CONNECT_RELEASE	2				//valeur indiquant que le bouton de connexion S2 a été relaché
#define	CMD_PLAY_PRESS		3				//valeur indiquant que le bouton de jeux S1 a été pressé
#define CMD_PLAY_RELEASE	4 				//valeur indiquant que le bouton de jeux S1 a été relaché
#define CMD_WIN_POINT		5				//valeur indiquant a un client qu'il a gagne un point
#define CMD_WIN_GAME		6				//valeur indiquant a un client qu'il a gagne la partie
#define CMD_DISCONNECT		7				//valeur indiquant a un client qu'il a été deconnecté car la partie es terminée



static void s1_handler(void* param) {
	struct msgQstr* msgQID = (struct msgQstr*) param;
	printf("S1 HANDLER\n");
	(void) param;
	enum buttons_states btn = buttons_get_state(BUTTONS_1);
	int cmd = 0;
	if(btn == BUTTONS_PRESSED){
		leds_set_state(leds_get_state() | LEDS_1);
		cmd = CMD_CONNECT_PRESS;
	}else if (btn == BUTTONS_RELEASED){
		leds_set_state(leds_get_state() & (~LEDS_1));
		cmd = CMD_CONNECT_RELEASE;
	}
	msgq_post(msgQID->msgQArray[1],(void*)cmd);
}

static void s2_handler(void* param) {
	struct msgQstr* msgQID = (struct msgQstr*) param;
	printf("S2 HANDLER\n");
	(void) param;
	enum buttons_states btn = buttons_get_state(BUTTONS_2);
	int cmd = 0;
	if(btn == BUTTONS_PRESSED){
		leds_set_state(leds_get_state() | LEDS_2);
		cmd = CMD_PLAY_PRESS;
	}else if (btn == BUTTONS_RELEASED){
		leds_set_state(leds_get_state() & (~LEDS_2));
		cmd = CMD_PLAY_RELEASE;
	}
	msgq_post(msgQID->msgQArray[1],(void*)cmd);
}

int main() {
	buttons_init();
	kernel_init();
	thread_init();
	msgq_init();
	interrupt_init();
	exception_init();
	intc_init();
	gpio_init_fct();
	leds_init();
	interrupt_enable();
	client_output_init(TARGET_ID);
	struct msgQstr msgQID = {
			.msgQArray[0]= msgq_create(MSGQ_SIZE),						//message Q for the channel rx of the client
			.msgQArray[1]= msgq_create(MSGQ_SIZE),						//message Q for the channel tx of the client
	};
	gpio_on_event(SW_GPIO, S1_PIN, GPIO_FALLING | GPIO_RISING, s1_handler, (void*) &msgQID);
	gpio_on_event(SW_GPIO, S2_PIN, GPIO_FALLING | GPIO_RISING, s2_handler, (void*) &msgQID);

	com_controller_init(TARGET_ID);
    thread_create(thread_nrf_rx, &msgQID, "nrf_rx", 0);
    thread_create(thread_nrf_tx, &msgQID, "nrf_tx", 0);
    thread_create(client_output_thread, &msgQID, "nrf_tx", 0);
    kernel_launch();
	while (1);
}

