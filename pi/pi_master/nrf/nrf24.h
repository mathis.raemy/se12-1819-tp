
#pragma once
#ifndef __NRF24_H__
#define __NRF24_H__
/*
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * This module has been adapted from the nRF24L01P.h/.cpp file developped
 * by Owen Edwards, see below, and rewritten in C for the beaglebone black
 * running baremetal software using the HEIA-FR libbbb software.
 */

/*
 *
 * @author Owen Edwards
 *
 * @section LICENSE
 *
 * Copyright (c) 2010 Owen Edwards
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * @section DESCRIPTION
 *
 * nRF24L01+ Single Chip 2.4GHz Transceiver from Nordic Semiconductor.
 *
 * Datasheet:
 *
 * http://www.nordicsemi.no/files/Product/data_sheet/NRF24_Product_Specification_1_0.pdf
 */

/**
 * Includes
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

/**
 * Defines
 */

#define NRF24_DEFAULT_TRANSMIT_SIZE 32

/**
 * Enums
 */
enum nrf24_output_power {
    NRF24_OUTPUT_POWER_ZERO_DB,
    NRF24_OUTPUT_POWER_MINUS_6_DB,
    NRF24_OUTPUT_POWER_MINUS_12_DB,
    NRF24_OUTPUT_POWER_MINUS_18_DB,
};

enum nrf24_datarate {
    NRF24_DATARATE_250_KBPS,
    NRF24_DATARATE_1_MBPS,
    NRF24_DATARATE_2_MBPS,
};

enum nrf24_crc_width {
    NRF24_CRC_NONE,
    NRF24_CRC_8_BIT,
    NRF24_CRC_16_BIT,
};

enum nrf24_pipe {
    NRF24_PIPE_P0,
    NRF24_PIPE_P1,
    NRF24_PIPE_P2,
    NRF24_PIPE_P3,
    NRF24_PIPE_P4,
    NRF24_PIPE_P5,
};

enum nrf24_address_size {
    NRF24_ADDRESS_1_BYTE,
    NRF24_ADDRESS_3_BYTES,
    NRF24_ADDRESS_4_BYTES,
    NRF24_ADDRESS_5_BYTES,
};

/**
 * This method should be called prior nrf24_init if the click board is
 * connected the cape 2. By default the cape 1 is selected.
 */
void nrf24_select_cape_2(void);

/**
 * nRF24L01+ Single Chip 2.4GHz Transceiver from Nordic Semiconductor.
 */

/**
 * Constructor.
 */
void nrf24_init();

/**
 * Set the RF frequency.
 *
 * @param frequency the frequency of RF transmission in MHz (0..125).
 */
void nrf24_set_rf_channel(unsigned channel);

/**
 * Get the RF frequency.
 *
 * @return the frequency of RF transmission in MHz (2400..2525).
 */
unsigned nrf24_get_rf_channel(void);

/**
 * Set the RF output power.
 *
 * @param power the RF output power in dBm (0, -6, -12 or -18).
 */
void nrf24_set_rf_output_power(enum nrf24_output_power power);

/**
 * Get the RF output power.
 *
 * @return the RF output power in dBm (0, -6, -12 or -18).
 */
enum nrf24_output_power nrf24_get_rf_output_power(void);

/**
 * Set the Air data rate.
 *
 * @param rate the air data rate in kbps (250, 1M or 2M).
 */
void nrf24_set_air_datarate(enum nrf24_datarate rate);

/**
 * Get the Air data rate.
 *
 * @return the air data rate in kbps (250, 1M or 2M).
 */
enum nrf24_datarate nrf24_get_air_datarate(void);

/**
 * Set the CRC width.
 *
 * @param width the number of bits for the CRC (0, 8 or 16).
 */
void nrf24_set_crc_width(enum nrf24_crc_width width);

/**
 * Get the CRC width.
 *
 * @return the number of bits for the CRC (0, 8 or 16).
 */
enum nrf24_crc_width nrf24_get_crc_width(void);

/**
 * Set the Receive address.
 *
 * @param address address associated with the particular pipe
 * @param width width of the address in bytes (3..5)
 * @param pipe pipe to associate the address with (0..5, default 0)
 *
 * Note that Pipes 0 & 1 have 3, 4 or 5 byte addresses,
 *  while Pipes 2..5 only use the lowest byte (bits 7..0) of the
 *  address provided here, and use 2, 3 or 4 bytes from Pipe 1's address.
 *  The width parameter is ignored for Pipes 2..5.
 */
void nrf24_set_rx_address(uint64_t address, enum nrf24_address_size width,
                          enum nrf24_pipe pipe);

/**
 * Set the Transmit address.
 *
 * @param address address for transmission
 * @param width width of the address in bytes (3..5)
 *
 * Note that the address width is shared with the Receive pipes,
 *  so a change to that address width affect transmissions.
 */
void nrf24_set_tx_address(uint64_t address, enum nrf24_address_size width);

/**
 * Get the Receive address.
 *
 * @param pipe pipe to get the address from (0..5, default 0)
 * @return the address associated with the particular pipe
 */
uint64_t nrf24_get_rx_address(enum nrf24_pipe pipe);

/**
 * Get the Transmit address.
 *
 * @return address address for transmission
 */
uint64_t nrf24_get_tx_address(void);

/**
 * Set the transfer size.
 *
 * @param size the size of the transfer, in bytes (1..32)
 */
void nrf24_set_transfer_size(unsigned size);

/**
 * Get the transfer size.
 *
 * @return the size of the transfer, in bytes (1..32).
 */
unsigned nrf24_get_transfer_size();

/**
 * Get the RPD (Received Power Detector) state.
 *
 * @return true if the received power exceeded -64dBm
 */
unsigned nrf24_get_rpd(void);

/**
 * Put the nRF24L01+ into Receive mode
 */
void nrf24_set_receive_mode(void);

/**
 * Put the nRF24L01+ into Transmit mode
 */
void nrf24_set_transmit_mode(void);

/**
 * Power up the nRF24L01+ into Standby mode
 */
void nrf24_power_up(void);

/**
 * Power down the nRF24L01+ into Power Down mode
 */
void nrf24_power_down(void);

/**
 * Transmit data
 *
 * @param data pointer to an array of bytes to write
 * @param count the number of bytes to send (1..32)
 * @return the number of bytes actually written, or -1 for an error
 */
int nrf24_write(const void *data, size_t count);

/**
 * Receive data
 *
 * @param pipe the receive pipe to get data from
 * @param data pointer to an array of bytes to store the received data
 * @param count the number of bytes to receive (1..32)
 * @return the number of bytes actually received, 0 if none are received, or -1
 * for an error
 */
int nrf24_read(enum nrf24_pipe pipe, void *data, size_t count);

/**
 * Determine if there is data available to read
 *
 * @param pipe the receive pipe to check for data
 * @return true if the is data waiting in the given pipe
 */
bool nrf24_readable(enum nrf24_pipe pipe);

/**
 * Disable all receive pipes
 *
 * Note: receive pipes are enabled when their address is set.
 */
void nrf24_disable_all_rx_pipes(void);

/**
 * Disable AutoAcknowledge function
 */
void nrf24_disable_auto_acknowledge(void);

/**
 * Enable AutoAcknowledge function
 */
void nrf24_enable_auto_acknowledge(void);

/**
 * Disable AutoRetransmit function
 */
void nrf24_disable_auto_retransmit(void);

/**
 * Enable AutoRetransmit function
 *
 * @param delay the delay between restransmits, in uS (250uS..4000uS)
 * @param count number of retransmits before generating an error (1..15)
 */
void nrf24_enable_auto_retransmit(unsigned delay, unsigned count);


#endif /* __NRF24_H__ */
