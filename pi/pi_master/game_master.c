/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:  This module is the game master of this program, it manage all the game.
 *
 * Author:	Mathis Raemy
 * Date: 	03.12.2018
 */
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "game_master.h"
#include "view.h"
#include "timer.h"
#include "buttons.h"
#include "thread.h"
#include "msgq.h"

#define MaxTime			2500
#define MinTime			500
#define WHITE			0xffff
#define ORANGE			0xf480
#define GREEN			0x0ff0
#define timeCounter		DMTimer_4
#define timerVideo		DMTimer_5
#define NO_RESPONSE		DMTimer_3
#define MAX_POINT		3
#define MASK			(BUTTONS_2)
#define TOTALPLAYERS	2

uint32_t totalTime;
static uint32_t rdmTime;
static bool isP1_connected = false;
static bool isP2_connected = false;
static bool isConnectionLost = false;
static bool isP1_Press = false;
static bool isP2_Press = false;
static int player_score[TOTALPLAYERS];
static bool timeOver = false;													//le temps d'attente est ecoule.
static int lastRespondingPlayer = 0;

enum etat {
	W_CO, W_RDY, INIT_GAME, W_RDM, W_RELEASE,W_VIDEO, LOOSE, WIN, END_GAME
};

static enum etat etat_present;

void game_master_init_game(){
	view_init();
	timer_init(timeCounter);
	timer_init(timerVideo);
	timer_init(NO_RESPONSE);
	view_wait_connection();
	etat_present = W_CO;

}

void game_master_set_state_LOOSE(enum intc_vectors timer, void* param){
	(void)param;
	(void)timer;
	etat_present = LOOSE;
}
void game_master_set_timeOver(enum intc_vectors timer, void* param){
	(void)param;
	(void)timer;
	timeOver = true;

}


static void game_master_end_game(struct msgQstr* msgQID){
	timer_set_period(NO_RESPONSE, 0);
	timer_on_event(NO_RESPONSE, 0, 0);

	if(isConnectionLost){
		msgq_post(msgQID->msgQArray[1], (void*)CMD_WIN_GAME+lastRespondingPlayer*10);
	}else{
		int winner = 0;
		if (player_score[1] > player_score[0]){
			winner = 1;
		}
		msgq_post(msgQID->msgQArray[1], (void*)CMD_WIN_GAME+winner*10);
	}
	isP1_connected = false;
	isP2_connected = false;
	view_clear_display();
	etat_present = W_CO;
}

void game_master_not_responding(enum intc_vectors timer, void* p){
	(void)p;
	(void)timer;
	isConnectionLost = true;
	etat_present = END_GAME;

}

void game_master_collect_msgQ(void* msgQ){
	struct msgQstr* msgQID = (struct msgQstr*) msgQ;
	while(1){
		int cmd = (int)msgq_fetch(msgQID->msgQArray[0]);
		timer_reset_counter(NO_RESPONSE);
		if(cmd > 10){
			cmd -=10;
			lastRespondingPlayer = 1;
			if(cmd == CMD_PLAY_PRESS) isP2_Press = true;
			if(cmd == CMD_PLAY_RELEASE) isP2_Press = false;
			if(cmd == CMD_CONNECT_PRESS){
				isP2_connected = true;
			}

		}else{
			lastRespondingPlayer = 0;
			if(cmd == CMD_PLAY_PRESS) isP1_Press = true;
			if(cmd == CMD_PLAY_RELEASE) isP1_Press = false;
			if(cmd == CMD_CONNECT_PRESS ){
				isP1_connected = true;
			}
		}
		thread_yield();
	}
}


void game_master_gameProcess(void* p){
	(void) p;
	struct msgQstr* msgQID = (struct msgQstr*) p;
	while(1){
		if (etat_present == W_CO){						//attente de la connexion des deux joueurs
			for(int i = 0; i< TOTALPLAYERS; i++) player_score[i] = 0;
			isConnectionLost = false;
			view_wait_connection();
			if (isP1_connected && isP2_connected){
				timer_on_event(NO_RESPONSE, game_master_not_responding, 0);
				timer_set_period(NO_RESPONSE, 30000);
				etat_present = W_RDY;
				view_clear_display();
			}
		}
		else if (etat_present == W_RDY){
			view_loadIdleState();
			view_display_score(player_score);
			if (isP1_Press && isP2_Press){
				etat_present = INIT_GAME;
			}
		}
		else if (etat_present == INIT_GAME){
			timeOver = false;
			etat_present = W_RDM;
		}
		else if (etat_present == W_RDM){
			view_ChangePressColor(ORANGE);
			view_ChangeRdyColor(WHITE);
			if(rdmTime == 0){
				rdmTime = rand()%(MaxTime-MinTime)+MinTime;					//modulo used to be sure to never be bigger then MaxTime
				timer_set_period(timeCounter, rdmTime);
				timer_on_event(timeCounter, game_master_set_timeOver, 0);
			}

			if(timeOver){
				rdmTime = 0;
				etat_present = W_RELEASE;
				timer_set_period(timeCounter, 0);						//remove the listener on the timer
				timer_on_event(timeCounter, 0, 0);
			}
			if (!isP2_Press){
				if (++player_score[0] < MAX_POINT)msgq_post(msgQID->msgQArray[1], (void*)CMD_WIN_POINT);
				timer_set_period(timerVideo, 1000);
				timer_on_event(timerVideo, game_master_set_state_LOOSE, 0);
				etat_present = W_VIDEO;
			}
			else if (!isP1_Press){
				if (++player_score[1] < MAX_POINT)msgq_post(msgQID->msgQArray[1], (void*)CMD_WIN_POINT+10);
				timer_set_period(timerVideo, 1000);
				timer_on_event(timerVideo, game_master_set_state_LOOSE, 0);
				etat_present = W_VIDEO;
			}
		}
		else if (etat_present == W_RELEASE){							//time is over we wait for one player to release button
			view_ChangeRdyColor(GREEN);
			view_display_score(player_score);
			if (!(isP1_Press && isP2_Press)){
				etat_present = WIN;
			}
		}
		else if(etat_present == W_VIDEO){
			view_GameLoose();
		}
		else if (etat_present == LOOSE){
			timer_set_period(timerVideo, 0);
			timer_on_event(timerVideo, 0, 0);
			bool loose = false;
			for (int i = 0; i<TOTALPLAYERS;i++){
				if (player_score[i] >= MAX_POINT){
					loose = true;
					etat_present = END_GAME;
				}
			}
			if(!loose) etat_present = W_RDY;
			view_clear_display();

		}else if (etat_present == WIN){
			view_display_score(player_score);
			if(!isP1_Press){
				if(++player_score[0] >=MAX_POINT){
					etat_present = END_GAME;
				}else{
					msgq_post(msgQID->msgQArray[1], (void*)CMD_WIN_POINT);
					etat_present = W_RDY;
				}
			}
			else if(!isP2_Press){
				if(++player_score[1] >= MAX_POINT){
					etat_present = END_GAME;
				}else{
					msgq_post(msgQID->msgQArray[1], (void*)CMD_WIN_POINT+10);
					etat_present = W_RDY;
				}
			}
		}else if (etat_present == END_GAME){
			game_master_end_game(msgQID);
		}
		thread_yield();
	}
}

