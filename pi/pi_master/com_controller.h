/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 Laboratory
 *
 * Abstract: Switches Device Driver
 *
 * Purpose:	This module purpose a controller to use the Nrf module
 *
 * Modified:		Mathis Raemy
 * Modification: 	-
 * Date: 	18.05.2019
 */
#include "msgq.h"
#ifndef COM_CONTROLLER_H_
#define COM_CONTROLLER_H_

//initialisation of the communication
extern void com_controller_init(int targetID);

//thread to transfert the data
extern void thread_nrf_tx(void *p);

//thread to receave data
extern void thread_nrf_rx(void *param);



#endif /* COM_CONTROLLER_H_ */
