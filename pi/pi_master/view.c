/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of display.c
 *
 * Purpose:	This module implements basic services to draw forms, caract and thermo on the screen.
 *
 * Author:	Raemy Mathis
 * Date: 	03.12.2018
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "view.h"
#include "display.h"

#define sizeOfText	8
#define SCREEN_WIDTH	96
#define SCREEN_HEIGHT	96
#define GREEN			0x0ff0
#define RED				0xf800
#define WHITE			0xffff
#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static struct display_point StrReadyPosition;
static struct display_point StrWaitingPlayerPosition;
static struct display_point StrPressPosition;
static struct display_point StrReactionPosition;
static struct display_point CircleReadyPosition;
static struct display_point CirclePressPosition;
static struct display_point StrReadyPosition;
static struct display_point StrScore0Position;
static struct display_point StrScore1Position;

static char StrReady[] = "Ready";
static char StrPress[] = "Press";
static char StrWaitingPlayer[] = "Wait player";


void view_init(){
	display_init();
	display_change_rotation(180);
	StrWaitingPlayerPosition.x_coor = 0;
	StrWaitingPlayerPosition.y_coor = 1;
	StrReadyPosition.x_coor = 2;
	StrReadyPosition.y_coor = 2;
	StrPressPosition.x_coor = StrReadyPosition.x_coor;
	StrPressPosition.y_coor = StrReadyPosition.y_coor+1;
	StrReactionPosition.x_coor = 5;
	StrReactionPosition.y_coor = StrReadyPosition.y_coor;
	CircleReadyPosition.y_coor = StrReadyPosition.x_coor+(9*(ARRAY_SIZE(StrReady)+1))-4;
	CircleReadyPosition.x_coor = StrReadyPosition.y_coor*9;		//place a circle 8 character space after string
	CirclePressPosition.y_coor = StrPressPosition.x_coor+(9*(ARRAY_SIZE(StrReady)+1))-4;
	CirclePressPosition.x_coor = StrReadyPosition.y_coor*9+9;
	StrScore0Position.x_coor = 0;
	StrScore1Position.x_coor = 11;
	StrScore0Position.y_coor = 11;
	StrScore1Position.y_coor = 11;
}
void view_restartGame(){
	display_clear();
}

void view_wait_connection(){
	display_array_caract(StrWaitingPlayerPosition.x_coor, StrWaitingPlayerPosition.y_coor,StrWaitingPlayer, WHITE);
}


void view_loadIdleState(){
	display_array_caract(StrReadyPosition.x_coor, StrReadyPosition.y_coor,StrReady, WHITE);
	display_array_caract(StrPressPosition.x_coor, StrPressPosition.y_coor,StrPress, WHITE);
	display_circle(CirclePressPosition, 4, WHITE);
	display_circle(CircleReadyPosition, 4, WHITE);
}

void view_ChangeRdyColor(uint16_t color){
	display_array_caract(StrReadyPosition.x_coor, StrReadyPosition.y_coor,StrReady, color);
	display_circle(CircleReadyPosition, 4, color);
}

void view_ChangePressColor(uint16_t color){
	display_array_caract(StrPressPosition.x_coor, StrPressPosition.y_coor,StrPress, WHITE);
	display_circle(CirclePressPosition, 4, color);
}

void view_display_score(int player_score[]){
	display_char(StrScore0Position.x_coor,StrScore0Position.y_coor, player_score[0]+'0', RED);
	display_char(StrScore1Position.x_coor,StrScore1Position.y_coor, player_score[1]+'0', RED);
}

void view_clear_display(){
	display_clear();
}

void view_GameLoose(){
	display_video();
}


