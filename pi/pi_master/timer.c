/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Purpose:		This module give an interface to the embedded clock counter
 *
 * Author:	Tobias Moullet & Mathis Raemy
 * Date: 	5.11.18
 */


#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <am335x_clock.h>
#include "timer.h"

#define TIOCP_CFG_SOFTRESET (1<<0)
#define TCLR_ST 			(1<<0)
#define TCLR_AR 			(1<<1)
#define TCLR_CE 			(1<<6)
#define TCLR_TRG_OV 		(1<<10)
#define TIOP_CFG_SOFTRESET	(1<<0)

#define IRQENABLE_OVF_IT_FLAG (1<<1)
#define IRQSTATUS_OVF_IT_FLAG (1<<1)
#define BASE_FREQ            24000000




struct dmtimer {
	uint32_t TIDR; 					// adress 0x00 - 0x03
	uint32_t res[3];				// adress 0x04 - 0x0F
	uint32_t TIOCP_CFG;				// adress 0x10 - 0x13
	uint32_t res1[3];				// adress 0x14 - 0x1F
	uint32_t IRQ_EQI;				// adress 0x20 - 0x23
	uint32_t IRQSTATUS_RAW;			// adress 0x24 - 0x27
	uint32_t IRQSTATUS;				// adress 0x28 - 0x2B
	uint32_t IRQENABLE_SET;			// adress 0x2C - 0x2F
	uint32_t IRQENABLE_CLR;			// adress 0x30 - 0x33
	uint32_t IRQWAKEEN;				// adress 0x34 - 0x37
	uint32_t TCLR;					// adress 0x38 - 0x3B
	uint32_t TCRR;					// adress 0x3C - 0x3F
	uint32_t TLDR;					// adress 0x40 - 0x43
	uint32_t TTGR;					// adress 0x44 - 0x47
	uint32_t TWPS;					// adress 0x48 - 0x4B
	uint32_t TMAR;					// adress 0x4C - 0x4F
	uint32_t TCAR1;					// adress 0x50 - 0x53
	uint32_t TSICR;					// adress 0x54 - 0x57
	uint32_t TCAR2;					// adress 0x58 - 0x5B
};
static struct listener {
	dmtimer_service_routine_t routine;
	void *param;
} listeners[TIMER_COUNT];

static uint32_t periods_ms[TIMER_COUNT];
static uint64_t last_values[TIMER_COUNT];
static unsigned long totals[TIMER_COUNT];




static volatile struct dmtimer* timer_ctrl []={
	[DMTimer_2] = (struct dmtimer*) 0x48040000,
	[DMTimer_3] = (struct dmtimer*) 0x48042000,
	[DMTimer_4] = (struct dmtimer*) 0x48044000,

	[DMTimer_5] = (struct dmtimer*) 0x48046000,
	[DMTimer_6] = (struct dmtimer*) 0x48048000,
	[DMTimer_7] = (struct dmtimer*) 0x4804A000
};
static int timer_to_intc[] = { [DMTimer_2] = INTC_TIMER2, [DMTimer_3
		] = INTC_TIMER3, [DMTimer_4] = INTC_TIMER4, [DMTimer_5] = INTC_TIMER5,
		[DMTimer_6] = INTC_TIMER6, [DMTimer_7] = INTC_TIMER7 };

static int intc_to_timer[] = { [INTC_TIMER2] = DMTimer_2, [INTC_TIMER3
		] = DMTimer_3, [INTC_TIMER4] = DMTimer_4, [INTC_TIMER5] = DMTimer_5,
		[INTC_TIMER6] = DMTimer_6, [INTC_TIMER7] = DMTimer_7 };


static const enum am335x_clock_timer_modules timer2clock[] = {
		AM335X_CLOCK_TIMER2, AM335X_CLOCK_TIMER3,
		AM335X_CLOCK_TIMER4, AM335X_CLOCK_TIMER5,
		AM335X_CLOCK_TIMER6, AM335X_CLOCK_TIMER7,

};

void timer_init(enum dmtimer_set timer){

	am335x_clock_enable_timer_module(timer2clock[timer]);

	volatile struct dmtimer* ctrl = timer_ctrl[timer];
	ctrl->TIOCP_CFG= TIOP_CFG_SOFTRESET;
	while((ctrl->TIOCP_CFG & TIOP_CFG_SOFTRESET) != 0);
	ctrl->TLDR = 0;
	ctrl->TCRR = 0;
	ctrl->TTGR = 0;
	ctrl->TCLR = TCLR_AR | TCLR_ST;
}

static void timer_handler(enum intc_vectors vector_nr, void* param) {
	(void) param;
	enum dmtimer_set timer = intc_to_timer[vector_nr];
	struct listener* listener = &listeners[timer];
	volatile struct dmtimer * ctrl = timer_ctrl[timer];
	ctrl->IRQSTATUS = IRQSTATUS_OVF_IT_FLAG;

	if (listener->routine != 0) {
		listener->routine(timer, listener->param);
	} else {
		// spurious interrupt
		ctrl->IRQENABLE_CLR = IRQENABLE_OVF_IT_FLAG;
		ctrl->TCLR &= ~TCLR_ST;
	}

}


uint32_t timer_get_counter(enum dmtimer_set timer)
{
	volatile struct dmtimer* ctrl = timer_ctrl[timer];
	return ctrl->TCRR;
}

void timer_reset_counter(enum dmtimer_set timer) {
	totals[timer] = 0;
	last_values[timer] = 0;
	volatile struct dmtimer * ctrl = timer_ctrl[timer];
	ctrl->TIOCP_CFG = TIOCP_CFG_SOFTRESET;
	while ((ctrl->TIOCP_CFG & TIOCP_CFG_SOFTRESET) != 0)
		;

	uint32_t period = periods_ms[timer] * (BASE_FREQ / 1000);
	ctrl->TCRR = -period;
	ctrl->TLDR = -period;
	ctrl->TTGR = -period;
	ctrl->TCLR = TCLR_AR | TCLR_ST;
	ctrl->IRQENABLE_SET = IRQENABLE_OVF_IT_FLAG;
}



void timer_set_period(enum dmtimer_set timer, uint32_t period_ms) {
	periods_ms[timer] = period_ms;
	timer_reset_counter(timer);
}

int timer_on_event(enum dmtimer_set timer, dmtimer_service_routine_t routine,
		void* param) {
	struct listener* listener = &listeners[timer];
	volatile struct dmtimer * ctrl = timer_ctrl[timer];
	if (routine == 0) { 									//remove routine
		listener->routine = 0;
		listener->param = 0;
		return intc_on_event(timer_to_intc[timer], 0, 0); 	// remove intc
	}

	if (listener->routine == 0) {
		listener->routine = routine;
		listener->param = param;
		ctrl->IRQENABLE_SET = IRQENABLE_OVF_IT_FLAG;
		return intc_on_event(timer_to_intc[timer], timer_handler, 0); // attach intc
	}

	return -1;
}



uint32_t timer_get_frequency()
{
	return 24000000;
}


