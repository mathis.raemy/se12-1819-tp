/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 pi
 *
 * Abstract: File that manage everything we need to use interruptions
 *
 * Purpose:	PI
 *
 * Author: 	Mathis Raemy et Tobias Moullet
 * Date: 	27.03.2019
 */
#include "interrupt.h"
#include "exception.h"
#include "stdlib.h"
#include "stdio.h"

#define INT_NB_VECTORS 		5


static struct listener {
	interrupt_service_routine_t routine;
	void* param;
} listeners[INT_NB_VECTORS];


void interrupt_handler (enum interrupt_vectors vector_nr)
{
	struct listener* listener = &listeners[vector_nr];
	if (listener->routine != 0) {
		listener->routine (vector_nr, listener->param);
	} else {
		printf("No interrupt service routine hooked to the vector# %d. Freezing!\n", vector_nr);
		while(1);
	}
}
extern void interrupt_asm_init();

void interrupt_init() {
  interrupt_asm_init();
}

int interrupt_on_event (enum interrupt_vectors vector_nr,
								interrupt_service_routine_t routine,
								void* param){

	if (vector_nr >= INT_NB_VECTORS) return -1;

	struct listener* listener = &listeners[vector_nr];

	if (listener -> routine != 0) return -1;

	if (routine == 0){
		listener -> routine = 0;
		listener -> param = 0;
	}
if (listener->routine == 0){
		listener -> routine = routine;
		listener -> param = param;
	}
	return 0;
}

