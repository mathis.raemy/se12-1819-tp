#pragma once
#ifndef VIEW_H
#define VIEW_H

/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory

 *
 * Purpose:  This module is managing the view of the game
 *
 * Author:	Raemy Mathis
 * Date: 	03.12.2018
 */

#include <stdint.h>
#include <stdlib.h>

//init the view to start the game
extern void view_init();

//charge l'ecran d'attente que les deux joueurs soient selectionnes
extern void view_wait_connection();

//Load the view to wait the btn_2 pressed
extern void view_loadIdleState();

//Change the color of the text "Ready"
extern void view_ChangeRdyColor(uint16_t color);

//change the color of the text "Press"
extern void view_ChangePressColor(uint16_t color);

//display the time at the end of the game
extern void view_display_score(int player_score[]);

//display the Loose view
extern void view_GameLoose();

//clear the display to restart the game
extern void view_restartGame();

//clear the display
extern void view_clear_display();

#endif /* VIEW_H_ */
