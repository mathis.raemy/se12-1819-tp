#pragma once
#ifndef GAME_MASTER_H_
#define GAME_MASTER_H_
/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 *
 * Purpose:  This module is the interface of the game master of this program, it manage all the game.
 *
 * Author:	Mathis Raemy
 * Date: 	03.12.2018
 */
#include <stdint.h>
#include <stdbool.h>
#define CMD_CONNECT_PRESS	1				//valeur indiquant que le bouton de connexion S2 a été pressé
#define CMD_CONNECT_RELEASE	2				//valeur indiquant que le bouton de connexion S2 a été relaché
#define	CMD_PLAY_PRESS		3				//valeur indiquant que le bouton de jeux S1 a été pressé
#define CMD_PLAY_RELEASE	4 			//valeur indiquant que le bouton de jeux S1 a été relaché
#define CMD_WIN_POINT		5				//valeur indiquant a un client qu'il a gagne un point
#define CMD_WIN_GAME		6			//valeur indiquant a un client qu'il a gagne la partie


//initialisation of every component and initialisation of the game
extern void game_master_init_game();

//this function manage the state machine of the game
extern void game_master_gameProcess();

//this fonction increment the number of player who press the ready button
//if operation is false, it decrement the variable
extern void game_master_modify_player_press(bool operation);

//this fonction increment the number of connected player
extern void game_master_connect_player();

//set the last player that has generate an interruption
extern void game_master_player_id(int id);

//thread to collect the information from the nrf and uptate the variable for the state machine
//no critical section problem, because the kernel is cooperatif
extern void game_master_collect_msgQ(void* msgQ);

#endif /* GAME_MASTER_H_ */
