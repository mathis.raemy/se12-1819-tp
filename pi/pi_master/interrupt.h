/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 pi
 *
 * Abstract: Header of interrupt.c
 *
 * Purpose:	PI
 *
 * Author: 	Mathis Raemy et Tobias Moullet
 * Date: 	27.03.2019
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_


enum interrupt_vectors {
	INT_UNDEF,			///< undefined instruction
	INT_SVC,			///< supervisor call (software interrupt)
	INT_PREFETCH,		///<prefetch abort (instruction prefetch)
	INT_DATA,			///<data abort (data access)
	INT_IRQ,			///<hardware interrupt request
	INT_FIQ,			///<hardware fast interrupt request
	INT_NB_VECTORS
};


/**
* Prototype of the interrupt service routine (listener)
*
* @param vector interrupt vector number
* @param param parameter specified while attaching the isr
*/
typedef void (*interrupt_service_routine_t) (
						enum interrupt_vectors vector_nr,
						void* param);

/**
 *
* Method to initialize low level resources of the microprocessor.
* At least a 8KiB of memory will be allocated for each interrupt vector
*/
extern void interrupt_init();
/**
* Method to hook and unhook an interrupt service routine to a specified interrupt source
*
* @param vector interrupt vector number
* @param routine interrupt service routine to hook to the specified interrupt source
*
use 0 to unhook the method
* @param param parameter to be passed as argument while calling the the
*
specified interrupt service routine
* @return execution status, 0 if sussess, -1 if already attached
*/
extern int interrupt_on_event (enum interrupt_vectors vector_nr,
								interrupt_service_routine_t routine,
								void* param);
/**
* Method to enable interrupt requests
*/
extern void interrupt_enable();
/**
* Method to disable interrupt requests
*/
extern void interrupt_disable();

#endif /* INTERRUPT_H_ */
