/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of display.c
 *
 * Purpose:	This module implements basic services to draw forms, caract and thermo on the screen.
 *
 * Author:	Tobias Moullet & Mathis Raemy
 * Date: 	5.11.18
 */

#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <oled.c>
#include "font_8x8.h"
#include "display.h"
#include "frame.h"
#define SCREEN_WIDTH	96
#define SCREEN_HEIGHT	96
#define nbPixel			SCREEN_HEIGHT*SCREEN_WIDTH
#define nbFrame			5
#define BLACK			0
#define BLUE			0x001f
#define RED				0xf800
#define WHITE			0xffff


//************Init  Statics variables ****************
static struct frame* frameChain;
static struct pixel video[nbFrame][nbPixel] ;
static int rotation = 270;


void display_video(){
	oled_memory_size (0,95,0,95);
	for(int i = 0; i < nbFrame; i++){
		oled_send_image(video[i], nbPixel);
	}
}

void convertToPixelArray(){
	for(int f = 0; f<nbFrame; f++){
	int j=0;
	    for (int y=95;y>0;y--) {
	        for (int x=96;x>0;x--) {
	            video[f][nbPixel-(x+96*y-1)].lword = frameChain->pixel_data[j++];
	            video[f][nbPixel-(x+96*y-1)].hword = frameChain->pixel_data[j++];
	        }
	    }
	    frameChain = frameChain->next;
	}
}

static void memory_size(uint32_t x1, uint32_t x2, uint32_t y1, uint32_t y2)
{
    switch (rotation){
    case 0:
        oled_memory_size(x1, x2, y1, y2);
        break;
    case 270:
        oled_memory_size(y1, y2, (SCREEN_HEIGHT-1)-x2, (SCREEN_HEIGHT-1)-x1);
        break;
    case 180:
        oled_memory_size(
            (SCREEN_WIDTH-1)-x2, (SCREEN_WIDTH-1)-x1,
            (SCREEN_HEIGHT-1)-y2, (SCREEN_HEIGHT-1)-y1);
        break;
    case 90:
        oled_memory_size((SCREEN_WIDTH-1)-y2, (SCREEN_WIDTH-1)-y1, x1, x2);
        break;
    }
}


void display_init(){
	oled_init(OLED_V101);
	display_clear();
	frameChain = getFrame();
	convertToPixelArray();
}

void display_clear()
{
    memory_size(
        0, SCREEN_WIDTH - 1,
        0, SCREEN_WIDTH - 1
    );
    for (unsigned int y = 0; y < SCREEN_HEIGHT; y++) {
        for (unsigned int x = 0; x < SCREEN_WIDTH; x++) {
            oled_color(BLACK);
        }
    }
}

void display_circle(struct display_point center,int32_t ray, uint32_t color)
{
	int size =0;
	for (int i =-(ray); i <= ray; i++)
	{
		size = sqrt((ray*ray)-(i*i));
		oled_memory_size(center.x_coor-size, center.x_coor+size, center.y_coor+i, center.y_coor+i);
		for (int r=0;r<=(2*size);r++){
			oled_color(color);
		}
	}
}

void display_array_caract(int col, int row, const char* txt, int color)
{
    for (int i = 0; ((i < 12) && (txt[i] != 0)); i++) {
        display_char(col + i, row, txt[i], color);
    }
}


void display_char(int col, int row, char c, int color)
{
    struct pixel s[8*8] = {0};
    struct pixel c_pixel = {.hword = color >> 8, .lword=color & 0xff};
    unsigned const char* bitmap = fontdata_8x8[(int) c];
    for (unsigned y = 0; y < 8; y++) {
        for (unsigned x = 0; x < 8; x++) {
            if (bitmap[7 - y] & (1 << x)) {
                switch (rotation) {
                case   0: s[(7-x)   + y*8    ] = c_pixel; break;
                case  90: s[(7-x)*8 + (7-y)  ] = c_pixel; break;
                case 180: s[x       + (7-y)*8] = c_pixel; break;
                case 270: s[x*8     + y      ] = c_pixel; break;
                }
            }
        }
    }
    uint32_t x1 = col*8;
    uint32_t y1 = SCREEN_HEIGHT-row*8-7;
    memory_size(x1, x1+7, y1, y1+7);
    oled_send_image(s, 8*8);
}

void display_rect(
    uint32_t x0, uint32_t y0,
    uint32_t w, uint32_t h,
    uint32_t color)
{
    memory_size(x0, x0+w-1, y0, y0+h-1);
    for (unsigned int x = 0; x < w; x++) {
        for (unsigned int y = 0; y < h; y++) {
            oled_color(color);
        }
    }
}

void display_change_rotation(uint32_t rot)
{
    display_clear();
    rotation = 270-rot;
}


