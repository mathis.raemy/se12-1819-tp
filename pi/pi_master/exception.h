/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 pi
 *
 * Purpose:     Module to deal with ARM exception handing
 * Author:        Raemy Mathis et Tobias Moullet
 * Date:         27.03.2019
 */


#ifndef EXCEPTION_H_
#define EXCEPTION_H_

/**
* initialization method
*/
extern void exception_init();

#endif /* EXCEPTION_H_ */
