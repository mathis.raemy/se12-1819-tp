/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 2 pi
 *
 * Abstract: Implementation of the exception manager
 *
 * Purpose:	PI
 *
 * Author: 	Mathis Raemy et Tobias Moullet
 * Date: 	27.03.2019
 */
#include "interrupt.h"
#include "exception.h"
#include <stdio.h>
/*
 * this methode is called on exception defined on exception_init()
 */
static void exception_handler(enum interrupt_vectors vector, void* param ){
	printf("Exception %d with message : %s\n", vector, (char*) param);
	while(vector == INT_PREFETCH);
}

/*
 * this methode init all listener
 * it call an attachement on the Exception type specified with methode specified and param specified
 */
void exception_init(){
	interrupt_on_event(INT_UNDEF, 		exception_handler, "Undefined instruction");
	interrupt_on_event(INT_SVC, 		exception_handler, "Software interrupt");
	interrupt_on_event(INT_DATA, 		exception_handler, "Data Abort interrupt");
	interrupt_on_event(INT_PREFETCH, 	exception_handler, "Prefetch Abort interrupt");

}
