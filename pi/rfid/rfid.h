#pragma once
#ifndef RFID_H
#define RFID_H
/**
 * Copyright 2019 University of Applied Sciences Western Switzerland / Fribourg
 *
 * This program is mainly based on the sample code developped by
 * Mikroelektronika to get access to the orignal code go to:
 * https://libstock.mikroe.com/projects/view/739/rfid-click-example
 */

// ID length
#define RFID_ID_LEN 38

// Initialize the CPU interface and the device
extern void init_cpu_and_peripherals();

// Get CR95HF chip ID
extern const char* rfid_read_chip_id();

// Read the tag ID
extern void rfid_get_tag_id(char* id);

#endif
