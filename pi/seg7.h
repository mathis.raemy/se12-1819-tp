#pragma once
#ifndef SEG7_H_
#define SEG7_H_

/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:	HEIA-FR / Embedded Systems 1+2 Laboratory
 *
 * Abstract: 	Declaration of seg7.c
 *
 * Purpose:	This module implements basic services to drive the AM335x
 * 		pin multiplexer module.
 *
 * Author:	Mathis Raemy
 * Date: 	01.10.18
 */
#include <stdint.h>
#include <stdbool.h>

// Segments definition
#define SEGA		(1<<4)
#define SEGB		(1<<5)
#define SEGC		(1<<14)
#define SEGD		(1<<22)
#define SEGE		(1<<23)
#define SEGF		(1<<26)
#define SEGG		(1<<27)



/**
 * method used to init the gpio
*/
extern void seg7_init();
/**
 * method used to display a number on the two 7 segments.
 * @param number the number you want to display, between -99 and +99 (both inclusive)
*/
void seg7_display_number(int number, int id, bool dotL, bool dotR);
/**
 * method used to display some segments on the two 7 segments. Automatically handles the switch between them.
 * @param segments1 the segments to display on the first 7 segment. See defines above.
 * @param segments2 the segments to display on the second 7 segment. See defines above.
 * @param dot whether or not you want the dot to be on
*/
extern void seg7_display_segments(uint32_t segments1, uint32_t segments2,
                                  bool dot);
//used to reset all segments
extern void seg7_reset();

#endif

